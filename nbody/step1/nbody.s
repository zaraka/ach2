# mark_description "Intel(R) C++ Intel(R) 64 Compiler for applications running on Intel(R) 64, Version 16.0.1.150 Build 20151021";
# mark_description "";
# mark_description "-std=c++11 -lpapi -ansi-alias -O2 -Wall -xavx -qopenmp-simd -DN=1000 -DDT=0.001f -DSTEPS=1000 -S -fsource-as";
# mark_description "m -c";
	.file "nbody.cpp"
	.text
..TXTST0:
# -- Begin  _Z18particles_simulateRA1000_10t_particle
	.text
# mark_begin;
       .align    16,0x90
	.globl _Z18particles_simulateRA1000_10t_particle
# --- particles_simulate(t_particles &)
_Z18particles_simulateRA1000_10t_particle:
# parameter 1: %rdi
..B1.1:                         # Preds ..B1.0

### void particles_simulate(t_particles &p) {

	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
..___tag_value__Z18particles_simulateRA1000_10t_particle.1:
..L2:
                                                          #9.41
        pushq     %rbp                                          #9.41
	.cfi_def_cfa_offset 16
        movq      %rsp, %rbp                                    #9.41
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
        andq      $-32, %rsp                                    #9.41
        pushq     %r13                                          #9.41
        pushq     %r14                                          #9.41
        subq      $12016, %rsp                                  #9.41
	.cfi_escape 0x10, 0x0d, 0x0e, 0x38, 0x1c, 0x0d, 0xe0, 0xff, 0xff, 0xff, 0x1a, 0x0d, 0xf8, 0xff, 0xff, 0xff, 0x22
	.cfi_escape 0x10, 0x0e, 0x0e, 0x38, 0x1c, 0x0d, 0xe0, 0xff, 0xff, 0xff, 0x1a, 0x0d, 0xf0, 0xff, 0xff, 0xff, 0x22
        movq      %rdi, %r14                                    #9.41

###     int i, j, k;
### 
###     t_velocities velocities = {};

        xorl      %esi, %esi                                    #12.29
        lea       (%rsp), %rdi                                  #12.29
        movl      $12000, %edx                                  #12.29
        call      _intel_fast_memset                            #12.29
                                # LOE rbx r12 r14 r15
..B1.2:                         # Preds ..B1.1

### 
###     for (k = 0; k < STEPS; k++) {
###         //vynulovani mezisouctu
###         #pragma omp simd
###         for (i = 0; i < N; i++) {

        xorl      %edx, %edx                                    #17.14
        xorl      %eax, %eax                                    #17.14

###             velocities[i].x = 0.0f;

        vxorps    %xmm0, %xmm0, %xmm0                           #18.13
                                # LOE rax rbx r12 r14 r15 edx xmm0
..B1.3:                         # Preds ..B1.3 ..B1.2
        addl      $16, %edx                                     #17.14
        vmovss    %xmm0, (%rsp,%rax)                            #18.13
        vextractps $1, %xmm0, 12(%rsp,%rax)                     #18.13
        vextractps $2, %xmm0, 24(%rsp,%rax)                     #18.13
        vextractps $3, %xmm0, 36(%rsp,%rax)                     #18.13
        vmovss    %xmm0, 48(%rsp,%rax)                          #18.13
        vextractps $1, %xmm0, 60(%rsp,%rax)                     #18.13
        vextractps $2, %xmm0, 72(%rsp,%rax)                     #18.13
        vextractps $3, %xmm0, 84(%rsp,%rax)                     #18.13
        vmovss    %xmm0, 96(%rsp,%rax)                          #18.13
        vextractps $1, %xmm0, 108(%rsp,%rax)                    #18.13
        vextractps $2, %xmm0, 120(%rsp,%rax)                    #18.13
        vextractps $3, %xmm0, 132(%rsp,%rax)                    #18.13
        vmovss    %xmm0, 144(%rsp,%rax)                         #18.13
        vextractps $1, %xmm0, 156(%rsp,%rax)                    #18.13
        vextractps $2, %xmm0, 168(%rsp,%rax)                    #18.13
        vextractps $3, %xmm0, 180(%rsp,%rax)                    #18.13

###             velocities[i].y = 0.0f;

        vmovss    %xmm0, 4(%rsp,%rax)                           #19.13
        vextractps $1, %xmm0, 16(%rsp,%rax)                     #19.13
        vextractps $2, %xmm0, 28(%rsp,%rax)                     #19.13
        vextractps $3, %xmm0, 40(%rsp,%rax)                     #19.13
        vmovss    %xmm0, 52(%rsp,%rax)                          #19.13
        vextractps $1, %xmm0, 64(%rsp,%rax)                     #19.13
        vextractps $2, %xmm0, 76(%rsp,%rax)                     #19.13
        vextractps $3, %xmm0, 88(%rsp,%rax)                     #19.13
        vmovss    %xmm0, 100(%rsp,%rax)                         #19.13
        vextractps $1, %xmm0, 112(%rsp,%rax)                    #19.13
        vextractps $2, %xmm0, 124(%rsp,%rax)                    #19.13
        vextractps $3, %xmm0, 136(%rsp,%rax)                    #19.13
        vmovss    %xmm0, 148(%rsp,%rax)                         #19.13
        vextractps $1, %xmm0, 160(%rsp,%rax)                    #19.13
        vextractps $2, %xmm0, 172(%rsp,%rax)                    #19.13
        vextractps $3, %xmm0, 184(%rsp,%rax)                    #19.13

###             velocities[i].z = 0.0f;

        vmovss    %xmm0, 8(%rsp,%rax)                           #20.13
        vextractps $1, %xmm0, 20(%rsp,%rax)                     #20.13
        vextractps $2, %xmm0, 32(%rsp,%rax)                     #20.13
        vextractps $3, %xmm0, 44(%rsp,%rax)                     #20.13
        vmovss    %xmm0, 56(%rsp,%rax)                          #20.13
        vextractps $1, %xmm0, 68(%rsp,%rax)                     #20.13
        vextractps $2, %xmm0, 80(%rsp,%rax)                     #20.13
        vextractps $3, %xmm0, 92(%rsp,%rax)                     #20.13
        vmovss    %xmm0, 104(%rsp,%rax)                         #20.13
        vextractps $1, %xmm0, 116(%rsp,%rax)                    #20.13
        vextractps $2, %xmm0, 128(%rsp,%rax)                    #20.13
        vextractps $3, %xmm0, 140(%rsp,%rax)                    #20.13
        vmovss    %xmm0, 152(%rsp,%rax)                         #20.13
        vextractps $1, %xmm0, 164(%rsp,%rax)                    #20.13
        vextractps $2, %xmm0, 176(%rsp,%rax)                    #20.13
        vextractps $3, %xmm0, 188(%rsp,%rax)                    #20.13
        addq      $192, %rax                                    #17.14
        cmpl      $992, %edx                                    #17.14
        jb        ..B1.3        # Prob 82%                      #17.14
                                # LOE rax rbx r12 r14 r15 edx xmm0
..B1.4:                         # Preds ..B1.3

###         }
###         //vypocet nove rychlosti
###         #pragma omp simd collapse(2)
###         for (i = 0; i < N; i++) {

        xorl      %edx, %edx                                    #24.14

###             for (j = 0; j < N; j++) {
###                 calculate_gravitation_velocity(p[j], p[i], velocities[j]);

        movq      %r14, %rdi                                    #26.17
        xorl      %eax, %eax                                    #24.14
        idivl     %eax                                          #24.14
        movslq    %eax, %r13                                    #24.14
        lea       (%rsp), %rdx                                  #26.17
        vxorps    %xmm0, %xmm0, %xmm0                           #18.13
        vmovss    %xmm0, 11904(%rdx)                            #18.13
        vmovss    %xmm0, 11908(%rdx)                            #19.13
        vmovss    %xmm0, 11912(%rdx)                            #20.13
        lea       (,%r13,4), %r8                                #26.54
        shlq      $5, %r13                                      #26.54
        subq      %r8, %r13                                     #26.54
        addq      %r14, %r13                                    #26.54
        movq      %r13, %rsi                                    #26.17
        vmovss    %xmm0, 11916(%rdx)                            #18.13
        vmovss    %xmm0, 11920(%rdx)                            #19.13
        vmovss    %xmm0, 11924(%rdx)                            #20.13
        vmovss    %xmm0, 11928(%rdx)                            #18.13
        vmovss    %xmm0, 11932(%rdx)                            #19.13
        vmovss    %xmm0, 11936(%rdx)                            #20.13
        vmovss    %xmm0, 11940(%rdx)                            #18.13
        vmovss    %xmm0, 11944(%rdx)                            #19.13
        vmovss    %xmm0, 11948(%rdx)                            #20.13
        vmovss    %xmm0, 11952(%rdx)                            #18.13
        vmovss    %xmm0, 11956(%rdx)                            #19.13
        vmovss    %xmm0, 11960(%rdx)                            #20.13
        vmovss    %xmm0, 11964(%rdx)                            #18.13
        vmovss    %xmm0, 11968(%rdx)                            #19.13
        vmovss    %xmm0, 11972(%rdx)                            #20.13
        vmovss    %xmm0, 11976(%rdx)                            #18.13
        vmovss    %xmm0, 11980(%rdx)                            #19.13
        vmovss    %xmm0, 11984(%rdx)                            #20.13
        vmovss    %xmm0, 11988(%rdx)                            #18.13
        vmovss    %xmm0, 11992(%rdx)                            #19.13
        vmovss    %xmm0, 11996(%rdx)                            #20.13
..___tag_value__Z18particles_simulateRA1000_10t_particle.8:
#       calculate_gravitation_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z30calculate_gravitation_velocityRK10t_particleS1_R10t_velocity #26.17
..___tag_value__Z18particles_simulateRA1000_10t_particle.9:
                                # LOE rbx r12 r13 r14 r15
..B1.5:                         # Preds ..B1.4

###                 calculate_collision_velocity(p[j], p[i], velocities[j]);

        movq      %r14, %rdi                                    #27.17
        movq      %r13, %rsi                                    #27.17
        lea       (%rsp), %rdx                                  #27.17
..___tag_value__Z18particles_simulateRA1000_10t_particle.10:
#       calculate_collision_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z28calculate_collision_velocityRK10t_particleS1_R10t_velocity #27.17
..___tag_value__Z18particles_simulateRA1000_10t_particle.11:
                                # LOE rbx r12 r14 r15
..B1.6:                         # Preds ..B1.5

###             }
###         }
###         //ulozeni rychlosti a posun castic
###         #pragma omp simd
###         for (i = 0; i < N; i++) {
###             p[i].vel_x += velocities[i].x;

        vmovss    12(%r14), %xmm0                               #33.13

###             p[i].vel_y += velocities[i].y;

        vmovss    16(%r14), %xmm1                               #34.13

###             p[i].vel_z += velocities[i].z;

        vmovss    20(%r14), %xmm2                               #35.13

### 
###             p[i].pos_x += p[i].vel_x * DT;

        vmovss    .L_2il0floatpacket.12(%rip), %xmm9            #37.40
        vaddss    (%rsp), %xmm0, %xmm3                          #33.13
        vaddss    4(%rsp), %xmm1, %xmm6                         #34.13
        vaddss    8(%rsp), %xmm2, %xmm10                        #35.13
        vmulss    %xmm3, %xmm9, %xmm4                           #37.40

###             p[i].pos_y += p[i].vel_y * DT;

        vmulss    %xmm6, %xmm9, %xmm7                           #38.40

###             p[i].pos_z += p[i].vel_z * DT;

        vmulss    %xmm10, %xmm9, %xmm11                         #39.40
        vaddss    (%r14), %xmm4, %xmm5                          #37.13
        vaddss    4(%r14), %xmm7, %xmm8                         #38.13
        vaddss    8(%r14), %xmm11, %xmm12                       #39.13
        vmovss    %xmm3, 12(%r14)                               #33.13
        vmovss    %xmm6, 16(%r14)                               #34.13
        vmovss    %xmm10, 20(%r14)                              #35.13
        vmovss    %xmm5, (%r14)                                 #37.13
        vmovss    %xmm8, 4(%r14)                                #38.13
        vmovss    %xmm12, 8(%r14)                               #39.13

###         }
###     }
### }

        addq      $12016, %rsp                                  #42.1
	.cfi_restore 14
        popq      %r14                                          #42.1
	.cfi_restore 13
        popq      %r13                                          #42.1
        movq      %rbp, %rsp                                    #42.1
        popq      %rbp                                          #42.1
	.cfi_def_cfa 7, 8
	.cfi_restore 6
        ret                                                     #42.1
        .align    16,0x90
	.cfi_endproc
                                # LOE
# mark_end;
	.type	_Z18particles_simulateRA1000_10t_particle,@function
	.size	_Z18particles_simulateRA1000_10t_particle,.-_Z18particles_simulateRA1000_10t_particle
	.data
# -- End  _Z18particles_simulateRA1000_10t_particle
	.text
# -- Begin  _Z14particles_readP8_IO_FILERA1000_10t_particle
	.text
# mark_begin;
       .align    16,0x90
	.globl _Z14particles_readP8_IO_FILERA1000_10t_particle
# --- particles_read(FILE *, t_particles &)
_Z14particles_readP8_IO_FILERA1000_10t_particle:
# parameter 1: %rdi
# parameter 2: %rsi
..B2.1:                         # Preds ..B2.0

### void particles_read(FILE *fp, t_particles &p) {

	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
..___tag_value__Z14particles_readP8_IO_FILERA1000_10t_particle.17:
..L18:
                                                         #45.47
        pushq     %r12                                          #45.47
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
        pushq     %r13                                          #45.47
	.cfi_def_cfa_offset 24
	.cfi_offset 13, -24
        pushq     %r14                                          #45.47
	.cfi_def_cfa_offset 32
	.cfi_offset 14, -32

###     for (int i = 0; i < N; i++) {

        xorl      %eax, %eax                                    #46.16
        movl      %eax, %r12d                                   #46.16
        movq      %rsi, %r14                                    #46.16
        movq      %rdi, %r13                                    #46.16
                                # LOE rbx rbp r13 r14 r15 r12d
..B2.2:                         # Preds ..B2.3 ..B2.1

###         fscanf(fp, "%f %f %f %f %f %f %f \n",

        addq      $-32, %rsp                                    #47.9
	.cfi_def_cfa_offset 64
        lea       4(%r14), %rcx                                 #47.9
        movq      %r13, %rdi                                    #47.9
        lea       8(%r14), %r8                                  #47.9
        movl      $.L_2__STRING.0, %esi                         #47.9
        lea       12(%r14), %r9                                 #47.9
        movq      %r14, %rdx                                    #47.9
        xorl      %eax, %eax                                    #47.9
        lea       16(%r14), %r10                                #47.9
        movq      %r10, (%rsp)                                  #47.9
        lea       20(%r14), %r11                                #47.9
        movq      %r11, 8(%rsp)                                 #47.9
        lea       24(%r14), %r10                                #47.9
        movq      %r10, 16(%rsp)                                #47.9
#       fscanf(FILE *, const char *, ...)
        call      fscanf                                        #47.9
                                # LOE rbx rbp r13 r14 r15 r12d
..B2.7:                         # Preds ..B2.2
        addq      $32, %rsp                                     #47.9
	.cfi_def_cfa_offset 32
                                # LOE rbx rbp r13 r14 r15 r12d
..B2.3:                         # Preds ..B2.7
        incl      %r12d                                         #46.28
        addq      $28, %r14                                     #46.28
        cmpl      $1000, %r12d                                  #46.25
        jl        ..B2.2        # Prob 99%                      #46.25
                                # LOE rbx rbp r13 r14 r15 r12d
..B2.4:                         # Preds ..B2.3

###                &p[i].pos_x, &p[i].pos_y, &p[i].pos_z,
###                &p[i].vel_x, &p[i].vel_y, &p[i].vel_z,
###                &p[i].weight);
###     }
### }

	.cfi_restore 14
        popq      %r14                                          #52.1
	.cfi_def_cfa_offset 24
	.cfi_restore 13
        popq      %r13                                          #52.1
	.cfi_def_cfa_offset 16
	.cfi_restore 12
        popq      %r12                                          #52.1
	.cfi_def_cfa_offset 8
        ret                                                     #52.1
        .align    16,0x90
	.cfi_endproc
                                # LOE
# mark_end;
	.type	_Z14particles_readP8_IO_FILERA1000_10t_particle,@function
	.size	_Z14particles_readP8_IO_FILERA1000_10t_particle,.-_Z14particles_readP8_IO_FILERA1000_10t_particle
	.data
# -- End  _Z14particles_readP8_IO_FILERA1000_10t_particle
	.text
# -- Begin  _Z15particles_writeP8_IO_FILERA1000_10t_particle
	.text
# mark_begin;
       .align    16,0x90
	.globl _Z15particles_writeP8_IO_FILERA1000_10t_particle
# --- particles_write(FILE *, t_particles &)
_Z15particles_writeP8_IO_FILERA1000_10t_particle:
# parameter 1: %rdi
# parameter 2: %rsi
..B3.1:                         # Preds ..B3.0

### void particles_write(FILE *fp, t_particles &p) {

	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
..___tag_value__Z15particles_writeP8_IO_FILERA1000_10t_particle.34:
..L35:
                                                         #54.48
        pushq     %r12                                          #54.48
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
        pushq     %r13                                          #54.48
	.cfi_def_cfa_offset 24
	.cfi_offset 13, -24
        pushq     %r14                                          #54.48
	.cfi_def_cfa_offset 32
	.cfi_offset 14, -32
        pushq     %r15                                          #54.48
	.cfi_def_cfa_offset 40
	.cfi_offset 15, -40
        pushq     %rsi                                          #54.48
	.cfi_def_cfa_offset 48

###     for (int i = 0; i < N; i++) {

        xorl      %edx, %edx                                    #55.16
        xorl      %eax, %eax                                    #55.16
        movl      %edx, %r13d                                   #55.16
        movq      %rax, %r12                                    #55.16
        movq      %rsi, %r15                                    #55.16
        movq      %rdi, %r14                                    #55.16
                                # LOE rbx rbp r12 r14 r15 r13d
..B3.2:                         # Preds ..B3.3 ..B3.1

###         fprintf(fp, "%10.10f %10.10f %10.10f %10.10f %10.10f %10.10f %10.10f \n",

        vxorpd    %xmm0, %xmm0, %xmm0                           #56.9
        vxorpd    %xmm1, %xmm1, %xmm1                           #56.9
        vxorpd    %xmm2, %xmm2, %xmm2                           #56.9
        vxorpd    %xmm3, %xmm3, %xmm3                           #56.9
        vxorpd    %xmm4, %xmm4, %xmm4                           #56.9
        vxorpd    %xmm5, %xmm5, %xmm5                           #56.9
        vxorpd    %xmm6, %xmm6, %xmm6                           #56.9
        movq      %r14, %rdi                                    #56.9
        vcvtss2sd (%r12,%r15), %xmm0, %xmm0                     #56.9
        vcvtss2sd 4(%r12,%r15), %xmm1, %xmm1                    #56.9
        vcvtss2sd 8(%r12,%r15), %xmm2, %xmm2                    #56.9
        vcvtss2sd 12(%r12,%r15), %xmm3, %xmm3                   #56.9
        vcvtss2sd 16(%r12,%r15), %xmm4, %xmm4                   #56.9
        vcvtss2sd 20(%r12,%r15), %xmm5, %xmm5                   #56.9
        vcvtss2sd 24(%r12,%r15), %xmm6, %xmm6                   #56.9
        movl      $.L_2__STRING.1, %esi                         #56.9
        movl      $7, %eax                                      #56.9
#       fprintf(FILE *, const char *, ...)
        call      fprintf                                       #56.9
                                # LOE rbx rbp r12 r14 r15 r13d
..B3.3:                         # Preds ..B3.2
        incl      %r13d                                         #55.28
        addq      $28, %r12                                     #55.28
        cmpl      $1000, %r13d                                  #55.25
        jl        ..B3.2        # Prob 99%                      #55.25
                                # LOE rbx rbp r12 r14 r15 r13d
..B3.4:                         # Preds ..B3.3

###                 p[i].pos_x, p[i].pos_y, p[i].pos_z,
###                 p[i].vel_x, p[i].vel_y, p[i].vel_z,
###                 p[i].weight);
###     }
### }

        popq      %rcx                                          #61.1
	.cfi_def_cfa_offset 40
	.cfi_restore 15
        popq      %r15                                          #61.1
	.cfi_def_cfa_offset 32
	.cfi_restore 14
        popq      %r14                                          #61.1
	.cfi_def_cfa_offset 24
	.cfi_restore 13
        popq      %r13                                          #61.1
	.cfi_def_cfa_offset 16
	.cfi_restore 12
        popq      %r12                                          #61.1
	.cfi_def_cfa_offset 8
        ret                                                     #61.1
        .align    16,0x90
	.cfi_endproc
                                # LOE
# mark_end;
	.type	_Z15particles_writeP8_IO_FILERA1000_10t_particle,@function
	.size	_Z15particles_writeP8_IO_FILERA1000_10t_particle,.-_Z15particles_writeP8_IO_FILERA1000_10t_particle
	.data
# -- End  _Z15particles_writeP8_IO_FILERA1000_10t_particle
	.section .rodata, "a"
	.align 4
	.align 4
.L_2il0floatpacket.12:
	.long	0x3a83126f
	.type	.L_2il0floatpacket.12,@object
	.size	.L_2il0floatpacket.12,4
	.section .rodata.str1.4, "aMS",@progbits,1
	.align 4
	.align 4
.L_2__STRING.0:
	.long	622880293
	.long	1713709158
	.long	543565088
	.long	622880293
	.long	1713709158
	.word	2592
	.byte	0
	.type	.L_2__STRING.0,@object
	.size	.L_2__STRING.0,23
	.space 1, 0x00 	# pad
	.align 4
.L_2__STRING.1:
	.long	774910245
	.long	543567921
	.long	774910245
	.long	543567921
	.long	774910245
	.long	543567921
	.long	774910245
	.long	543567921
	.long	774910245
	.long	543567921
	.long	774910245
	.long	543567921
	.long	774910245
	.long	543567921
	.word	10
	.type	.L_2__STRING.1,@object
	.size	.L_2__STRING.1,58
	.data
	.section .note.GNU-stack, ""
// -- Begin DWARF2 SEGMENT .eh_frame
	.section .eh_frame,"a",@progbits
.eh_frame_seg:
	.align 8
# End

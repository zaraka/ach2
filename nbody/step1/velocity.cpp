/*
 * Architektura procesoru (ACH 2016)
 * Projekt c. 1 (nbody)
 * Login: xvanku00
 */

#include <cmath>
#include <cfloat>
#include "velocity.h"

/**
 * @breef   Funkce vypocte rychlost kterou teleso p1 ziska vlivem gravitace p2.
 * @details Viz zadani.pdf
 */
void calculate_gravitation_velocity(
        const t_particle &p1,
        const t_particle &p2,
        t_velocity &vel
) {
    float r, dx, dy, dz;
    float vx, vy, vz;

    dx = p2.pos_x - p1.pos_x;
    dy = p2.pos_y - p1.pos_y;
    dz = p2.pos_z - p1.pos_z;

    r = sqrt(dx * dx + dy * dy + dz * dz);

    /* MISTO PRO VAS KOD GRAVITACE */

    float r3 = r * r * r;
    float help = p2.weight * G * DT;

    vx = (help * dx) / r3;
    vy = (help * dy) / r3;
    vz = (help * dz) / r3;

    /* KONEC */

    vel.x += (r > COLLISION_DISTANCE) ? vx : 0.0f;
    vel.y += (r > COLLISION_DISTANCE) ? vy : 0.0f;
    vel.z += (r > COLLISION_DISTANCE) ? vz : 0.0f;
}

/**
 * @breef   Funkce vypocte rozdil mezi rychlostmi pred a po kolizi telesa p1 do telesa p2.
 */
void calculate_collision_velocity(
        const t_particle &p1,
        const t_particle &p2,
        t_velocity &vel
) {
    float r, dx, dy, dz;
    float vx, vy, vz;

    dx = p2.pos_x - p1.pos_x;
    dy = p2.pos_y - p1.pos_y;
    dz = p2.pos_z - p1.pos_z;

    r = sqrt(dx * dx + dy * dy + dz * dz);

    /* MISTO PRO VAS KOD KOLIZE */

    float m2 = p1.weight + p2.weight;
    float wx = (p1.weight * p1.vel_x - p2.weight * p1.vel_x + 2.0f * p2.weight * p2.vel_x) / m2;
    float wy = (p1.weight * p1.vel_y - p2.weight * p1.vel_y + 2.0f * p2.weight * p2.vel_y) / m2;
    float wz = (p1.weight * p1.vel_z - p2.weight * p1.vel_z + 2.0f * p2.weight * p2.vel_z) / m2;

    vx = wx - p1.vel_x;
    vy = wy - p1.vel_y;
    vz = wz - p1.vel_z;

    /* KONEC */

    // jedna se o rozdilne ale blizke prvky
    if (r > 0.0f && r < COLLISION_DISTANCE) {
        vel.x += vx;
        vel.y += vy;
        vel.z += vz;
    }
}

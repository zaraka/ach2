# mark_description "Intel(R) C++ Intel(R) 64 Compiler for applications running on Intel(R) 64, Version 16.0.1.150 Build 20151021";
# mark_description "";
# mark_description "-std=c++11 -lpapi -ansi-alias -O2 -Wall -xavx -qopenmp-simd -DN=1000 -DDT=0.001f -DSTEPS=1000 -S -fsource-as";
# mark_description "m -c";
	.file "nbody.cpp"
	.text
..TXTST0:
# -- Begin  _Z18particles_simulateP11t_particles
	.text
# mark_begin;
       .align    16,0x90
	.globl _Z18particles_simulateP11t_particles
# --- particles_simulate(t_particles *)
_Z18particles_simulateP11t_particles:
# parameter 1: %rdi
..B1.1:                         # Preds ..B1.0

### void particles_simulate(t_particles *p) {

	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
..___tag_value__Z18particles_simulateP11t_particles.1:
..L2:
                                                          #9.41
        pushq     %rbp                                          #9.41
	.cfi_def_cfa_offset 16
        movq      %rsp, %rbp                                    #9.41
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
        andq      $-64, %rsp                                    #9.41
        pushq     %r12                                          #9.41
        pushq     %r13                                          #9.41
        pushq     %r14                                          #9.41
        pushq     %r15                                          #9.41
        pushq     %rbx                                          #9.41
        subq      $13656, %rsp                                  #9.41
        movq      %rdi, %rdx                                    #9.41

###     int i, j, k;
### 
###     float *pos_x = p->pos_x;
###     float *pos_y = p->pos_y;
###     float *pos_z = p->pos_z;
###     float *vel_x = p->vel_x;
###     float *vel_y = p->vel_y;
###     float *vel_z = p->vel_z;
###     float *weight = p->weight;
### 
###     t_velocities velocities;
###     t_velocity vel_j;
### 
###     t_velocities *velocities_ptr = &velocities;
### 
###     float *velocities_x = velocities_ptr->x;
###     float *velocities_y = velocities_ptr->y;
###     float *velocities_z = velocities_ptr->z;
### 
###     t_particle particle_i, particle_j;
### 
###     for (k = 0; k < STEPS; k++) {
###         //vynulovani mezisouctu
###         #pragma omp simd
###         for (i = 0; i < N; i++) {
###             velocities_x[i] = 0.0f;
###             velocities_y[i] = 0.0f;
###             velocities_z[i] = 0.0f;
###         }
###         //vypocet nove rychlosti
###         for (i = 0; i < N; i++) {
###             #pragma omp simd private(particle_i, particle_j, vel_j) aligned(pos_x, pos_y, pos_z, vel_x, vel_y, vel_z, weight: 64)
###             for (j = 0; j < N; j++) {
###                 particle_i = { pos_x[i], pos_y[i], pos_z[i], vel_x[i], vel_y[i], vel_z[i], weight[i]};
###                 particle_j = { pos_x[j], pos_y[j], pos_z[j], vel_x[j], vel_y[j], vel_z[j], weight[j]};
###                 vel_j = { velocities_x[j], velocities_y[j], velocities_z[j] };
###                 calculate_gravitation_velocity(particle_j, particle_i, vel_j);

        vmovdqu   .L_2il0floatpacket.5(%rip), %xmm6             #46.48
        lea       12160(%rsp), %rdi                             #46.48
        vmovdqu   .L_2il0floatpacket.4(%rip), %xmm4             #46.48
        lea       12608(%rsp), %rsi                             #46.60
        vmovdqu   .L_2il0floatpacket.3(%rip), %xmm12            #46.48
        xorl      %eax, %eax                                    #31.10
        vmovdqu   .L_2il0floatpacket.6(%rip), %xmm14            #46.48
	.cfi_escape 0x10, 0x03, 0x0e, 0x38, 0x1c, 0x0d, 0xc0, 0xff, 0xff, 0xff, 0x1a, 0x0d, 0xd8, 0xff, 0xff, 0xff, 0x22
	.cfi_escape 0x10, 0x0c, 0x0e, 0x38, 0x1c, 0x0d, 0xc0, 0xff, 0xff, 0xff, 0x1a, 0x0d, 0xf8, 0xff, 0xff, 0xff, 0x22
	.cfi_escape 0x10, 0x0d, 0x0e, 0x38, 0x1c, 0x0d, 0xc0, 0xff, 0xff, 0xff, 0x1a, 0x0d, 0xf0, 0xff, 0xff, 0xff, 0x22
	.cfi_escape 0x10, 0x0e, 0x0e, 0x38, 0x1c, 0x0d, 0xc0, 0xff, 0xff, 0xff, 0x1a, 0x0d, 0xe8, 0xff, 0xff, 0xff, 0x22
	.cfi_escape 0x10, 0x0f, 0x0e, 0x38, 0x1c, 0x0d, 0xc0, 0xff, 0xff, 0xff, 0x1a, 0x0d, 0xe0, 0xff, 0xff, 0xff, 0x22

###                 calculate_collision_velocity(particle_j, particle_i, vel_j);
### 
###                 velocities_x[j] = vel_j.x;
###                 velocities_y[j] = vel_j.y;
###                 velocities_z[j] = vel_j.z;
###             }
###         }
###         //ulozeni rychlosti a posun castic
###         #pragma omp simd aligned(pos_x, pos_y, pos_z, vel_x, vel_y, vel_z, weight: 64)
###         for (i = 0; i < N; i++) {
###             vel_x[i] += velocities_x[i];
###             vel_y[i] += velocities_y[i];
###             vel_z[i] += velocities_z[i];
### 
###             pos_x[i] += vel_x[i] * DT;

        movq      %rdx, %r12                                    #61.36
        vmovd     %rdi, %xmm5                                   #46.48
        movq      %rsi, %rbx                                    #61.36
        vpunpcklqdq %xmm5, %xmm5, %xmm15                        #46.48
        movq      %rdi, %r14                                    #61.36
        vpaddq    %xmm6, %xmm15, %xmm7                          #46.48
        vpaddq    %xmm4, %xmm15, %xmm0                          #46.48
        vpaddq    %xmm12, %xmm15, %xmm5                         #46.48
        vpaddq    %xmm14, %xmm15, %xmm9                         #46.48
        vmovdqu   %xmm7, 16(%rsp)                               #46.48
        lea       13056(%rsp), %r11                             #46.72
        vmovdqu   .L_2il0floatpacket.7(%rip), %xmm7             #46.48
        vpaddq    %xmm7, %xmm15, %xmm13                         #46.48
        vmovdqu   %xmm0, (%rsp)                                 #46.48
        vmovd     %rsi, %xmm0                                   #46.60
        vmovdqu   %xmm13, 48(%rsp)                              #46.48
        vpunpcklqdq %xmm0, %xmm0, %xmm13                        #46.60
        vpaddq    %xmm6, %xmm13, %xmm6                          #46.60
        vpaddq    %xmm14, %xmm13, %xmm14                        #46.60
        vmovdqu   .L_2il0floatpacket.0(%rip), %xmm10            #46.48
        vmovdqu   .L_2il0floatpacket.1(%rip), %xmm11            #46.48
        vmovdqu   .L_2il0floatpacket.2(%rip), %xmm1             #46.48
        vmovdqu   %xmm6, 96(%rsp)                               #46.60
        vpaddq    %xmm4, %xmm13, %xmm6                          #46.60
        vpaddq    %xmm12, %xmm13, %xmm4                         #46.60
        vpaddq    %xmm10, %xmm15, %xmm3                         #46.48
        vpaddq    %xmm11, %xmm15, %xmm8                         #46.48
        vpaddq    %xmm1, %xmm15, %xmm2                          #46.48
        vpaddq    %xmm11, %xmm13, %xmm0                         #46.60
        vpaddq    %xmm1, %xmm13, %xmm1                          #46.60
        vmovd     %xmm3, %r8                                    #61.36
        vmovd     %r11, %xmm12                                  #46.72
        vpunpcklqdq %xmm12, %xmm12, %xmm15                      #46.72
        vmovdqu   %xmm9, 32(%rsp)                               #46.48
        vpaddq    %xmm7, %xmm13, %xmm9                          #46.60
        vpaddq    %xmm10, %xmm13, %xmm7                         #46.60
        vpaddq    .L_2il0floatpacket.9(%rip), %xmm15, %xmm10    #46.72
        vpaddq    .L_2il0floatpacket.10(%rip), %xmm15, %xmm11   #46.72
        vpaddq    .L_2il0floatpacket.11(%rip), %xmm15, %xmm12   #46.72
        vpaddq    .L_2il0floatpacket.12(%rip), %xmm15, %xmm13   #46.72
        vmovd     %xmm7, %r9                                    #61.36
        vmovdqu   %xmm9, 64(%rsp)                               #46.60
        vpshufd   $14, %xmm3, %xmm3                             #61.36
        vmovdqu   %xmm14, 80(%rsp)                              #46.60
        vpaddq    .L_2il0floatpacket.8(%rip), %xmm15, %xmm9     #46.72
        vpaddq    .L_2il0floatpacket.13(%rip), %xmm15, %xmm14   #46.72
        vmovd     %xmm9, %r10                                   #61.36
        vmovq     %xmm0, 984(%rsi)                              #61.36
        vpshufd   $14, %xmm0, %xmm0                             #61.36
        vmovq     %xmm3, 656(%rsi)                              #61.36
        vpshufd   $14, %xmm10, %xmm3                            #61.36
        vmovq     %xmm0, 680(%rsi)                              #61.36
        vpshufd   $14, %xmm11, %xmm0                            #61.36
        vmovdqu   %xmm14, 112(%rsp)                             #46.72
        vmovq     %xmm2, 712(%rsi)                              #61.36
        vpshufd   $14, %xmm2, %xmm2                             #61.36
        vpaddq    .L_2il0floatpacket.14(%rip), %xmm15, %xmm14   #46.72
        vpaddq    .L_2il0floatpacket.15(%rip), %xmm15, %xmm15   #46.72
        vpshufd   $14, %xmm9, %xmm9                             #61.36
        vmovq     %xmm3, 672(%rsi)                              #61.36
        vmovq     %xmm0, 720(%rsi)                              #61.36
        vpshufd   $14, %xmm12, %xmm0                            #61.36
        vmovdqu   (%rsp), %xmm3                                 #61.36
        vmovq     %xmm8, 664(%rsi)                              #61.36
        vpshufd   $14, %xmm8, %xmm8                             #61.36
        vmovq     %xmm1, 704(%rsi)                              #61.36
        vpshufd   $14, %xmm1, %xmm1                             #61.36
        vmovq     %xmm2, 736(%rsi)                              #61.36
        vpshufd   $14, %xmm5, %xmm2                             #61.36
        vmovd     %xmm9, %rcx                                   #61.36
        vmovq     %xmm0, 768(%rsi)                              #61.36
        vmovq     %xmm3, 808(%rsi)                              #61.36
        vmovq     %xmm10, 968(%rsi)                             #61.36
        vmovq     %xmm8, 688(%rsi)                              #61.36
        vmovq     %xmm1, 728(%rsi)                              #61.36
        vmovq     %xmm12, 744(%rsi)                             #61.36
        vmovq     %xmm4, 752(%rsi)                              #61.36
        vmovq     %xmm5, 760(%rsi)                              #61.36
        vpshufd   $14, %xmm4, %xmm1                             #61.36
        vmovq     %xmm2, 784(%rsi)                              #61.36
        vpshufd   $14, %xmm3, %xmm4                             #61.36
        vmovdqu   32(%rsp), %xmm0                               #61.36
        vmovdqu   48(%rsp), %xmm3                               #61.36
        vmovdqu   112(%rsp), %xmm5                              #61.36
        vmovdqu   96(%rsp), %xmm8                               #61.36
        vmovdqu   16(%rsp), %xmm10                              #61.36
        vmovdqu   80(%rsp), %xmm12                              #61.36
        vmovdqu   64(%rsp), %xmm2                               #61.36
        vpshufd   $14, %xmm7, %xmm7                             #61.36
        vmovq     %xmm15, 936(%rsi)                             #61.36
        vpshufd   $14, %xmm15, %xmm15                           #61.36
        vmovq     %xmm1, 776(%rsi)                              #61.36
        vmovq     %xmm0, 904(%rsi)                              #61.36
        vpshufd   $14, %xmm0, %xmm1                             #61.36
        vpshufd   $14, %xmm3, %xmm0                             #61.36
        vmovq     %xmm7, 976(%rsi)                              #61.36
        vmovq     %xmm11, 696(%rsi)                             #61.36
        vmovq     %xmm13, 792(%rsi)                             #61.36
        vmovq     %xmm6, 800(%rsi)                              #61.36
        vpshufd   $14, %xmm13, %xmm13                           #61.36
        vpshufd   $14, %xmm6, %xmm6                             #61.36
        vpshufd   $14, %xmm5, %xmm7                             #61.36
        vpshufd   $14, %xmm8, %xmm9                             #61.36
        vpshufd   $14, %xmm10, %xmm11                           #61.36
        vmovq     %xmm14, 888(%rsi)                             #61.36
        vmovq     %xmm12, 896(%rsi)                             #61.36
        vpshufd   $14, %xmm14, %xmm14                           #61.36
        vpshufd   $14, %xmm12, %xmm12                           #61.36
        vmovq     %xmm15, 640(%rsi)                             #61.36
        vpshufd   $14, %xmm2, %xmm15                            #61.36
        vmovq     %xmm0, 960(%rsi)                              #61.36
        vmovq     %xmm13, 816(%rsi)                             #61.36
        vmovq     %xmm6, 824(%rsi)                              #61.36
        vmovq     %xmm4, 832(%rsi)                              #61.36
        vmovq     %xmm5, 840(%rsi)                              #61.36
        vmovq     %xmm8, 848(%rsi)                              #61.36
        vmovq     %xmm10, 856(%rsi)                             #61.36
        vmovq     %xmm7, 864(%rsi)                              #61.36
        vmovq     %xmm9, 872(%rsi)                              #61.36
        vmovq     %xmm11, 880(%rsi)                             #61.36
        vmovq     %xmm14, 912(%rsi)                             #61.36
        vmovq     %xmm12, 920(%rsi)                             #61.36
        vmovq     %xmm1, 928(%rsi)                              #61.36
        vmovq     %xmm2, 944(%rsi)                              #61.36
        vmovq     %xmm3, 952(%rsi)                              #61.36
        vmovq     %xmm15, 648(%rsi)                             #61.36
        movq      %rcx, 992(%rsi)                               #61.36
        movq      %r8, 1000(%rsi)                               #61.36
        movq      %r9, 1008(%rsi)                               #61.36
        movq      %r10, 1016(%rsi)                              #61.36
        vxorps    %ymm0, %ymm0, %ymm0                           #61.36
                                # LOE rbx r12 r14 eax ymm0
..B1.2:                         # Preds ..B1.46 ..B1.1
        xorl      %ecx, %ecx                                    #34.14
        .align    16,0x90
                                # LOE rcx rbx r12 r14 eax ymm0
..B1.3:                         # Preds ..B1.3 ..B1.2
        vmovups   %ymm0, 64(%rsp,%rcx,4)                        #35.13
        vmovups   %ymm0, 4096(%rsp,%rcx,4)                      #36.13
        vmovups   %ymm0, 8128(%rsp,%rcx,4)                      #37.13
        vmovups   %ymm0, 96(%rsp,%rcx,4)                        #35.13
        vmovups   %ymm0, 4128(%rsp,%rcx,4)                      #36.13
        vmovups   %ymm0, 8160(%rsp,%rcx,4)                      #37.13
        vmovups   %ymm0, 128(%rsp,%rcx,4)                       #35.13
        vmovups   %ymm0, 4160(%rsp,%rcx,4)                      #36.13
        vmovups   %ymm0, 8192(%rsp,%rcx,4)                      #37.13
        vmovups   %ymm0, 160(%rsp,%rcx,4)                       #35.13
        vmovups   %ymm0, 4192(%rsp,%rcx,4)                      #36.13
        vmovups   %ymm0, 8224(%rsp,%rcx,4)                      #37.13
        addq      $32, %rcx                                     #34.14
        cmpq      $992, %rcx                                    #34.14
        jb        ..B1.3        # Prob 82%                      #34.14
                                # LOE rcx rbx r12 r14 eax ymm0
..B1.4:                         # Preds ..B1.3
        vmovups   %ymm0, 4032(%rsp)                             #35.13
        xorl      %r13d, %r13d                                  #40.14
        vmovups   %ymm0, 8064(%rsp)                             #36.13
        vmovups   %ymm0, 12096(%rsp)                            #37.13
        movl      %eax, (%rsp)                                  #40.14
                                # LOE rbx r12 r13 r14
..B1.5:                         # Preds ..B1.43 ..B1.4
        movq      13256(%rsp), %rbx                             #42.18
        xorl      %r15d, %r15d                                  #42.18
        movq      13248(%rsp), %r14                             #42.18
                                # LOE rbx r12 r13 r14 r15
..B1.6:                         # Preds ..B1.38 ..B1.5
        vbroadcastss (%r12,%r13,4), %xmm12                      #43.32
        vbroadcastss 4032(%r12,%r13,4), %xmm13                  #43.42
        vbroadcastss 8064(%r12,%r13,4), %xmm14                  #43.52
        vbroadcastss 12096(%r12,%r13,4), %xmm15                 #43.62
        vbroadcastss 16128(%r12,%r13,4), %xmm10                 #43.72
        vbroadcastss 20160(%r12,%r13,4), %xmm9                  #43.82
        vbroadcastss 24192(%r12,%r13,4), %xmm8                  #43.92
        vmovss    %xmm12, 12608(%rsp)                           #43.17
        vmovss    %xmm12, 12720(%rsp)                           #43.17
        vmovss    %xmm12, 12832(%rsp)                           #43.17
        vextractps $1, %xmm12, 12636(%rsp)                      #43.17
        vextractps $2, %xmm12, 12664(%rsp)                      #43.17
        vextractps $3, %xmm12, 12692(%rsp)                      #43.17
        vextractps $1, %xmm12, 12748(%rsp)                      #43.17
        vextractps $2, %xmm12, 12776(%rsp)                      #43.17
        vextractps $3, %xmm12, 12804(%rsp)                      #43.17
        vextractps $1, %xmm12, 12860(%rsp)                      #43.17
        vextractps $2, %xmm12, 12888(%rsp)                      #43.17
        vextractps $3, %xmm12, 12916(%rsp)                      #43.17
        vmovss    %xmm12, 12944(%rsp)                           #43.17
        vextractps $1, %xmm12, 12972(%rsp)                      #43.17
        vextractps $2, %xmm12, 13000(%rsp)                      #43.17
        vextractps $3, %xmm12, 13028(%rsp)                      #43.17
        vmovss    %xmm13, 12612(%rsp)                           #43.17
        vextractps $1, %xmm13, 12640(%rsp)                      #43.17
        vextractps $2, %xmm13, 12668(%rsp)                      #43.17
        vextractps $3, %xmm13, 12696(%rsp)                      #43.17
        vmovss    %xmm13, 12724(%rsp)                           #43.17
        vextractps $1, %xmm13, 12752(%rsp)                      #43.17
        vextractps $2, %xmm13, 12780(%rsp)                      #43.17
        vextractps $3, %xmm13, 12808(%rsp)                      #43.17
        vmovss    %xmm13, 12836(%rsp)                           #43.17
        vextractps $1, %xmm13, 12864(%rsp)                      #43.17
        vextractps $2, %xmm13, 12892(%rsp)                      #43.17
        vextractps $3, %xmm13, 12920(%rsp)                      #43.17
        vmovss    %xmm13, 12948(%rsp)                           #43.17
        vextractps $1, %xmm13, 12976(%rsp)                      #43.17
        vextractps $2, %xmm13, 13004(%rsp)                      #43.17
        vextractps $3, %xmm13, 13032(%rsp)                      #43.17
        vmovss    %xmm14, 12616(%rsp)                           #43.17
        vextractps $1, %xmm14, 12644(%rsp)                      #43.17
        vextractps $2, %xmm14, 12672(%rsp)                      #43.17
        vextractps $3, %xmm14, 12700(%rsp)                      #43.17
        vmovss    %xmm14, 12728(%rsp)                           #43.17
        vextractps $1, %xmm14, 12756(%rsp)                      #43.17
        vextractps $2, %xmm14, 12784(%rsp)                      #43.17
        vextractps $3, %xmm14, 12812(%rsp)                      #43.17
        vmovss    %xmm14, 12840(%rsp)                           #43.17
        vextractps $1, %xmm14, 12868(%rsp)                      #43.17
        vextractps $2, %xmm14, 12896(%rsp)                      #43.17
        vextractps $3, %xmm14, 12924(%rsp)                      #43.17
        vmovss    %xmm14, 12952(%rsp)                           #43.17
        vextractps $1, %xmm14, 12980(%rsp)                      #43.17
        vextractps $2, %xmm14, 13008(%rsp)                      #43.17
        vextractps $3, %xmm14, 13036(%rsp)                      #43.17
        vmovss    %xmm15, 12620(%rsp)                           #43.17
        vextractps $1, %xmm15, 12648(%rsp)                      #43.17
        vextractps $2, %xmm15, 12676(%rsp)                      #43.17
        vextractps $3, %xmm15, 12704(%rsp)                      #43.17
        vmovss    %xmm15, 12732(%rsp)                           #43.17
        vextractps $1, %xmm15, 12760(%rsp)                      #43.17
        vextractps $2, %xmm15, 12788(%rsp)                      #43.17
        vextractps $3, %xmm15, 12816(%rsp)                      #43.17
        vmovss    %xmm15, 12844(%rsp)                           #43.17
        vextractps $1, %xmm15, 12872(%rsp)                      #43.17
        vextractps $2, %xmm15, 12900(%rsp)                      #43.17
        vextractps $3, %xmm15, 12928(%rsp)                      #43.17
        vmovss    %xmm15, 12956(%rsp)                           #43.17
        vextractps $1, %xmm15, 12984(%rsp)                      #43.17
        vextractps $2, %xmm15, 13012(%rsp)                      #43.17
        vextractps $3, %xmm15, 13040(%rsp)                      #43.17
        vmovss    %xmm10, 12624(%rsp)                           #43.17
        vextractps $1, %xmm10, 12652(%rsp)                      #43.17
        vextractps $2, %xmm10, 12680(%rsp)                      #43.17
        vextractps $3, %xmm10, 12708(%rsp)                      #43.17
        vmovss    %xmm10, 12736(%rsp)                           #43.17
        vextractps $1, %xmm10, 12764(%rsp)                      #43.17
        vextractps $2, %xmm10, 12792(%rsp)                      #43.17
        vextractps $3, %xmm10, 12820(%rsp)                      #43.17
        vmovss    %xmm10, 12848(%rsp)                           #43.17
        vextractps $1, %xmm10, 12876(%rsp)                      #43.17
        vextractps $2, %xmm10, 12904(%rsp)                      #43.17
        vextractps $3, %xmm10, 12932(%rsp)                      #43.17
        vmovss    %xmm10, 12960(%rsp)                           #43.17
        vextractps $1, %xmm10, 12988(%rsp)                      #43.17
        vextractps $2, %xmm10, 13016(%rsp)                      #43.17
        vextractps $3, %xmm10, 13044(%rsp)                      #43.17
        vmovss    %xmm9, 12628(%rsp)                            #43.17
        vextractps $1, %xmm9, 12656(%rsp)                       #43.17
        vextractps $2, %xmm9, 12684(%rsp)                       #43.17
        vextractps $3, %xmm9, 12712(%rsp)                       #43.17
        vmovss    %xmm9, 12740(%rsp)                            #43.17
        vextractps $1, %xmm9, 12768(%rsp)                       #43.17
        vextractps $2, %xmm9, 12796(%rsp)                       #43.17
        vextractps $3, %xmm9, 12824(%rsp)                       #43.17
        vmovss    %xmm9, 12852(%rsp)                            #43.17
        vextractps $1, %xmm9, 12880(%rsp)                       #43.17
        vextractps $2, %xmm9, 12908(%rsp)                       #43.17
        vextractps $3, %xmm9, 12936(%rsp)                       #43.17
        vmovss    %xmm9, 12964(%rsp)                            #43.17
        vextractps $1, %xmm9, 12992(%rsp)                       #43.17
        vextractps $2, %xmm9, 13020(%rsp)                       #43.17
        vextractps $3, %xmm9, 13048(%rsp)                       #43.17
        vmovss    %xmm8, 12632(%rsp)                            #43.17
        vextractps $1, %xmm8, 12660(%rsp)                       #43.17
        vextractps $2, %xmm8, 12688(%rsp)                       #43.17
        vextractps $3, %xmm8, 12716(%rsp)                       #43.17
        vmovss    %xmm8, 12744(%rsp)                            #43.17
        vextractps $1, %xmm8, 12772(%rsp)                       #43.17
        vextractps $2, %xmm8, 12800(%rsp)                       #43.17
        vextractps $3, %xmm8, 12828(%rsp)                       #43.17
        vmovss    %xmm8, 12856(%rsp)                            #43.17
        vextractps $1, %xmm8, 12884(%rsp)                       #43.17
        vextractps $2, %xmm8, 12912(%rsp)                       #43.17
        vextractps $3, %xmm8, 12940(%rsp)                       #43.17
        vmovss    %xmm8, 12968(%rsp)                            #43.17
        vextractps $1, %xmm8, 12996(%rsp)                       #43.17
        vextractps $2, %xmm8, 13024(%rsp)                       #43.17
        vextractps $3, %xmm8, 13052(%rsp)                       #43.17
        vmovups   (%r12,%r15,4), %ymm12                         #44.32
        vmovups   4032(%r12,%r15,4), %ymm11                     #44.42
        vmovups   4064(%r12,%r15,4), %ymm0                      #44.42
        vmovups   8064(%r12,%r15,4), %ymm1                      #44.52
        vmovups   8096(%r12,%r15,4), %ymm2                      #44.52
        vmovups   32(%r12,%r15,4), %ymm14                       #44.32
        vmovups   12096(%r12,%r15,4), %ymm3                     #44.62
        vmovups   12128(%r12,%r15,4), %ymm4                     #44.62
        vmovups   16128(%r12,%r15,4), %ymm5                     #44.72
        vmovups   16160(%r12,%r15,4), %ymm6                     #44.72
        vmovups   20160(%r12,%r15,4), %ymm7                     #44.82
        vmovups   20192(%r12,%r15,4), %ymm8                     #44.82
        vmovups   24192(%r12,%r15,4), %ymm9                     #44.92
        vmovups   24224(%r12,%r15,4), %ymm10                    #44.92
        movq      13608(%rsp), %rdi                             #46.17
        movq      13616(%rsp), %rsi                             #46.17
        movq      13624(%rsp), %rdx                             #46.17
        vextractf128 $1, %ymm12, %xmm13                         #44.17
        vmovss    %xmm12, 12160(%rsp)                           #44.17
        vmovss    %xmm11, 12164(%rsp)                           #44.17
        vextractps $1, %xmm12, 12188(%rsp)                      #44.17
        vextractps $2, %xmm12, 12216(%rsp)                      #44.17
        vextractps $3, %xmm12, 12244(%rsp)                      #44.17
        vextractf128 $1, %ymm11, %xmm12                         #44.17
        vmovss    %xmm0, 12388(%rsp)                            #44.17
        vmovss    %xmm1, 12168(%rsp)                            #44.17
        vextractps $1, %xmm11, 12192(%rsp)                      #44.17
        vextractps $2, %xmm11, 12220(%rsp)                      #44.17
        vextractps $3, %xmm11, 12248(%rsp)                      #44.17
        vextractf128 $1, %ymm0, %xmm11                          #44.17
        vmovss    %xmm13, 12272(%rsp)                           #44.17
        vmovss    %xmm12, 12276(%rsp)                           #44.17
        vextractps $1, %xmm0, 12416(%rsp)                       #44.17
        vextractps $2, %xmm0, 12444(%rsp)                       #44.17
        vextractps $3, %xmm0, 12472(%rsp)                       #44.17
        vextractf128 $1, %ymm1, %xmm0                           #44.17
        vmovss    %xmm11, 12500(%rsp)                           #44.17
        vmovss    %xmm0, 12280(%rsp)                            #44.17
        vextractps $1, %xmm0, 12308(%rsp)                       #44.17
        vextractps $2, %xmm0, 12336(%rsp)                       #44.17
        vextractps $3, %xmm0, 12364(%rsp)                       #44.17
        vextractps $1, %xmm1, 12196(%rsp)                       #44.17
        vextractps $2, %xmm1, 12224(%rsp)                       #44.17
        vextractps $3, %xmm1, 12252(%rsp)                       #44.17
        vextractps $1, %xmm13, 12300(%rsp)                      #44.17
        vextractps $2, %xmm13, 12328(%rsp)                      #44.17
        vextractps $3, %xmm13, 12356(%rsp)                      #44.17
        vextractps $1, %xmm12, 12304(%rsp)                      #44.17
        vextractps $2, %xmm12, 12332(%rsp)                      #44.17
        vextractps $3, %xmm12, 12360(%rsp)                      #44.17
        vextractps $1, %xmm11, 12528(%rsp)                      #44.17
        vextractps $2, %xmm11, 12556(%rsp)                      #44.17
        vextractps $3, %xmm11, 12584(%rsp)                      #44.17
        vextractf128 $1, %ymm2, %xmm0                           #44.17
        vmovss    %xmm2, 12392(%rsp)                            #44.17
        vmovss    %xmm0, 12504(%rsp)                            #44.17
        vextractps $1, %xmm0, 12532(%rsp)                       #44.17
        vextractps $2, %xmm0, 12560(%rsp)                       #44.17
        vextractps $3, %xmm0, 12588(%rsp)                       #44.17
        vextractps $1, %xmm2, 12420(%rsp)                       #44.17
        vextractps $2, %xmm2, 12448(%rsp)                       #44.17
        vextractps $3, %xmm2, 12476(%rsp)                       #44.17
        vextractf128 $1, %ymm3, %xmm0                           #44.17
        vmovss    %xmm3, 12172(%rsp)                            #44.17
        vmovss    %xmm0, 12284(%rsp)                            #44.17
        vextractps $1, %xmm0, 12312(%rsp)                       #44.17
        vextractps $2, %xmm0, 12340(%rsp)                       #44.17
        vextractps $3, %xmm0, 12368(%rsp)                       #44.17
        vextractps $1, %xmm3, 12200(%rsp)                       #44.17
        vextractps $2, %xmm3, 12228(%rsp)                       #44.17
        vextractps $3, %xmm3, 12256(%rsp)                       #44.17
        vextractf128 $1, %ymm4, %xmm0                           #44.17
        vmovss    %xmm4, 12396(%rsp)                            #44.17
        vmovss    %xmm0, 12508(%rsp)                            #44.17
        vextractps $1, %xmm0, 12536(%rsp)                       #44.17
        vextractps $2, %xmm0, 12564(%rsp)                       #44.17
        vextractps $3, %xmm0, 12592(%rsp)                       #44.17
        vextractps $1, %xmm4, 12424(%rsp)                       #44.17
        vextractps $2, %xmm4, 12452(%rsp)                       #44.17
        vextractps $3, %xmm4, 12480(%rsp)                       #44.17
        vextractf128 $1, %ymm5, %xmm0                           #44.17
        vmovss    %xmm5, 12176(%rsp)                            #44.17
        vmovss    %xmm0, 12288(%rsp)                            #44.17
        vextractps $1, %xmm0, 12316(%rsp)                       #44.17
        vextractps $2, %xmm0, 12344(%rsp)                       #44.17
        vextractps $3, %xmm0, 12372(%rsp)                       #44.17
        vextractps $1, %xmm5, 12204(%rsp)                       #44.17
        vextractps $2, %xmm5, 12232(%rsp)                       #44.17
        vextractps $3, %xmm5, 12260(%rsp)                       #44.17
        vextractf128 $1, %ymm6, %xmm0                           #44.17
        vmovss    %xmm6, 12400(%rsp)                            #44.17
        vmovss    %xmm0, 12512(%rsp)                            #44.17
        vextractps $1, %xmm0, 12540(%rsp)                       #44.17
        vextractps $2, %xmm0, 12568(%rsp)                       #44.17
        vextractps $3, %xmm0, 12596(%rsp)                       #44.17
        vextractps $1, %xmm6, 12428(%rsp)                       #44.17
        vextractps $2, %xmm6, 12456(%rsp)                       #44.17
        vextractps $3, %xmm6, 12484(%rsp)                       #44.17
        vextractf128 $1, %ymm14, %xmm15                         #44.17
        vextractf128 $1, %ymm7, %xmm0                           #44.17
        vextractf128 $1, %ymm8, %xmm1                           #44.17
        vextractf128 $1, %ymm9, %xmm2                           #44.17
        vextractf128 $1, %ymm10, %xmm3                          #44.17
        vmovss    %xmm14, 12384(%rsp)                           #44.17
        vmovss    %xmm15, 12496(%rsp)                           #44.17
        vextractps $1, %xmm14, 12412(%rsp)                      #44.17
        vextractps $2, %xmm14, 12440(%rsp)                      #44.17
        vextractps $3, %xmm14, 12468(%rsp)                      #44.17
        vextractps $1, %xmm15, 12524(%rsp)                      #44.17
        vextractps $2, %xmm15, 12552(%rsp)                      #44.17
        vextractps $3, %xmm15, 12580(%rsp)                      #44.17
        vmovss    %xmm7, 12180(%rsp)                            #44.17
        vextractps $1, %xmm7, 12208(%rsp)                       #44.17
        vextractps $2, %xmm7, 12236(%rsp)                       #44.17
        vextractps $3, %xmm7, 12264(%rsp)                       #44.17
        vmovss    %xmm0, 12292(%rsp)                            #44.17
        vextractps $1, %xmm0, 12320(%rsp)                       #44.17
        vextractps $2, %xmm0, 12348(%rsp)                       #44.17
        vextractps $3, %xmm0, 12376(%rsp)                       #44.17
        vmovss    %xmm8, 12404(%rsp)                            #44.17
        vextractps $1, %xmm8, 12432(%rsp)                       #44.17
        vextractps $2, %xmm8, 12460(%rsp)                       #44.17
        vextractps $3, %xmm8, 12488(%rsp)                       #44.17
        vmovss    %xmm1, 12516(%rsp)                            #44.17
        vextractps $1, %xmm1, 12544(%rsp)                       #44.17
        vextractps $2, %xmm1, 12572(%rsp)                       #44.17
        vextractps $3, %xmm1, 12600(%rsp)                       #44.17
        vmovss    %xmm9, 12184(%rsp)                            #44.17
        vextractps $1, %xmm9, 12212(%rsp)                       #44.17
        vextractps $2, %xmm9, 12240(%rsp)                       #44.17
        vextractps $3, %xmm9, 12268(%rsp)                       #44.17
        vmovss    %xmm2, 12296(%rsp)                            #44.17
        vextractps $1, %xmm2, 12324(%rsp)                       #44.17
        vextractps $2, %xmm2, 12352(%rsp)                       #44.17
        vextractps $3, %xmm2, 12380(%rsp)                       #44.17
        vmovss    %xmm10, 12408(%rsp)                           #44.17
        vextractps $1, %xmm10, 12436(%rsp)                      #44.17
        vextractps $2, %xmm10, 12464(%rsp)                      #44.17
        vextractps $3, %xmm10, 12492(%rsp)                      #44.17
        vmovss    %xmm3, 12520(%rsp)                            #44.17
        vextractps $1, %xmm3, 12548(%rsp)                       #44.17
        vextractps $2, %xmm3, 12576(%rsp)                       #44.17
        vextractps $3, %xmm3, 12604(%rsp)                       #44.17
        vmovups   64(%rsp,%r15,4), %ymm4                        #45.27
        vmovups   96(%rsp,%r15,4), %ymm6                        #45.27
        vmovups   4096(%rsp,%r15,4), %ymm11                     #45.44
        vmovups   4128(%rsp,%r15,4), %ymm13                     #45.44
        vmovups   8128(%rsp,%r15,4), %ymm15                     #45.61
        vmovups   8160(%rsp,%r15,4), %ymm0                      #45.61
        vextractf128 $1, %ymm4, %xmm5                           #45.17
        vmovss    %xmm4, 13056(%rsp)                            #45.17
        vmovss    %xmm5, 13104(%rsp)                            #45.17
        vextractps $1, %xmm4, 13068(%rsp)                       #45.17
        vextractps $2, %xmm4, 13080(%rsp)                       #45.17
        vextractps $3, %xmm4, 13092(%rsp)                       #45.17
        vextractf128 $1, %ymm6, %xmm7                           #45.17
        vextractf128 $1, %ymm11, %xmm12                         #45.17
        vextractf128 $1, %ymm13, %xmm14                         #45.17
        vextractf128 $1, %ymm15, %xmm4                          #45.17
        vextractf128 $1, %ymm0, %xmm1                           #45.17
        vmovss    %xmm6, 13152(%rsp)                            #45.17
        vmovss    %xmm7, 13200(%rsp)                            #45.17
        vextractps $1, %xmm5, 13116(%rsp)                       #45.17
        vextractps $2, %xmm5, 13128(%rsp)                       #45.17
        vextractps $3, %xmm5, 13140(%rsp)                       #45.17
        vextractps $1, %xmm6, 13164(%rsp)                       #45.17
        vextractps $2, %xmm6, 13176(%rsp)                       #45.17
        vextractps $3, %xmm6, 13188(%rsp)                       #45.17
        vextractps $1, %xmm7, 13212(%rsp)                       #45.17
        vextractps $2, %xmm7, 13224(%rsp)                       #45.17
        vextractps $3, %xmm7, 13236(%rsp)                       #45.17
        vmovss    %xmm11, 13060(%rsp)                           #45.17
        vextractps $1, %xmm11, 13072(%rsp)                      #45.17
        vextractps $2, %xmm11, 13084(%rsp)                      #45.17
        vextractps $3, %xmm11, 13096(%rsp)                      #45.17
        vmovss    %xmm12, 13108(%rsp)                           #45.17
        vextractps $1, %xmm12, 13120(%rsp)                      #45.17
        vextractps $2, %xmm12, 13132(%rsp)                      #45.17
        vextractps $3, %xmm12, 13144(%rsp)                      #45.17
        vmovss    %xmm13, 13156(%rsp)                           #45.17
        vextractps $1, %xmm13, 13168(%rsp)                      #45.17
        vextractps $2, %xmm13, 13180(%rsp)                      #45.17
        vextractps $3, %xmm13, 13192(%rsp)                      #45.17
        vmovss    %xmm14, 13204(%rsp)                           #45.17
        vextractps $1, %xmm14, 13216(%rsp)                      #45.17
        vextractps $2, %xmm14, 13228(%rsp)                      #45.17
        vextractps $3, %xmm14, 13240(%rsp)                      #45.17
        vmovss    %xmm15, 13064(%rsp)                           #45.17
        vextractps $1, %xmm15, 13076(%rsp)                      #45.17
        vextractps $2, %xmm15, 13088(%rsp)                      #45.17
        vextractps $3, %xmm15, 13100(%rsp)                      #45.17
        vmovss    %xmm4, 13112(%rsp)                            #45.17
        vextractps $1, %xmm4, 13124(%rsp)                       #45.17
        vextractps $2, %xmm4, 13136(%rsp)                       #45.17
        vextractps $3, %xmm4, 13148(%rsp)                       #45.17
        vmovss    %xmm0, 13160(%rsp)                            #45.17
        vextractps $1, %xmm0, 13172(%rsp)                       #45.17
        vextractps $2, %xmm0, 13184(%rsp)                       #45.17
        vextractps $3, %xmm0, 13196(%rsp)                       #45.17
        vmovss    %xmm1, 13208(%rsp)                            #45.17
        vextractps $1, %xmm1, 13220(%rsp)                       #45.17
        vextractps $2, %xmm1, 13232(%rsp)                       #45.17
        vextractps $3, %xmm1, 13244(%rsp)                       #45.17
        vzeroupper                                              #46.17
..___tag_value__Z18particles_simulateP11t_particles.11:
#       calculate_gravitation_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z30calculate_gravitation_velocityRK10t_particleS1_R10t_velocity #46.17
..___tag_value__Z18particles_simulateP11t_particles.12:
                                # LOE rbx r12 r13 r14 r15
..B1.7:                         # Preds ..B1.6
        movq      13264(%rsp), %rdi                             #46.17
        movq      13584(%rsp), %rsi                             #46.17
        movq      13600(%rsp), %rdx                             #46.17
..___tag_value__Z18particles_simulateP11t_particles.13:
#       calculate_gravitation_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z30calculate_gravitation_velocityRK10t_particleS1_R10t_velocity #46.17
..___tag_value__Z18particles_simulateP11t_particles.14:
                                # LOE rbx r12 r13 r14 r15
..B1.8:                         # Preds ..B1.7
        movq      13272(%rsp), %rdi                             #46.17
        movq      13592(%rsp), %rsi                             #46.17
        movq      13576(%rsp), %rdx                             #46.17
..___tag_value__Z18particles_simulateP11t_particles.15:
#       calculate_gravitation_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z30calculate_gravitation_velocityRK10t_particleS1_R10t_velocity #46.17
..___tag_value__Z18particles_simulateP11t_particles.16:
                                # LOE rbx r12 r13 r14 r15
..B1.9:                         # Preds ..B1.8
        movq      13296(%rsp), %rdi                             #46.17
        movq      13288(%rsp), %rsi                             #46.17
        movq      13280(%rsp), %rdx                             #46.17
..___tag_value__Z18particles_simulateP11t_particles.17:
#       calculate_gravitation_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z30calculate_gravitation_velocityRK10t_particleS1_R10t_velocity #46.17
..___tag_value__Z18particles_simulateP11t_particles.18:
                                # LOE rbx r12 r13 r14 r15
..B1.10:                        # Preds ..B1.9
        movq      13320(%rsp), %rdi                             #46.17
        movq      13312(%rsp), %rsi                             #46.17
        movq      13304(%rsp), %rdx                             #46.17
..___tag_value__Z18particles_simulateP11t_particles.19:
#       calculate_gravitation_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z30calculate_gravitation_velocityRK10t_particleS1_R10t_velocity #46.17
..___tag_value__Z18particles_simulateP11t_particles.20:
                                # LOE rbx r12 r13 r14 r15
..B1.11:                        # Preds ..B1.10
        movq      13344(%rsp), %rdi                             #46.17
        movq      13336(%rsp), %rsi                             #46.17
        movq      13328(%rsp), %rdx                             #46.17
..___tag_value__Z18particles_simulateP11t_particles.21:
#       calculate_gravitation_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z30calculate_gravitation_velocityRK10t_particleS1_R10t_velocity #46.17
..___tag_value__Z18particles_simulateP11t_particles.22:
                                # LOE rbx r12 r13 r14 r15
..B1.12:                        # Preds ..B1.11
        movq      13368(%rsp), %rdi                             #46.17
        movq      13360(%rsp), %rsi                             #46.17
        movq      13352(%rsp), %rdx                             #46.17
..___tag_value__Z18particles_simulateP11t_particles.23:
#       calculate_gravitation_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z30calculate_gravitation_velocityRK10t_particleS1_R10t_velocity #46.17
..___tag_value__Z18particles_simulateP11t_particles.24:
                                # LOE rbx r12 r13 r14 r15
..B1.13:                        # Preds ..B1.12
        movq      13392(%rsp), %rdi                             #46.17
        movq      13384(%rsp), %rsi                             #46.17
        movq      13376(%rsp), %rdx                             #46.17
..___tag_value__Z18particles_simulateP11t_particles.25:
#       calculate_gravitation_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z30calculate_gravitation_velocityRK10t_particleS1_R10t_velocity #46.17
..___tag_value__Z18particles_simulateP11t_particles.26:
                                # LOE rbx r12 r13 r14 r15
..B1.14:                        # Preds ..B1.13
        movq      13416(%rsp), %rdi                             #46.17
        movq      13408(%rsp), %rsi                             #46.17
        movq      13400(%rsp), %rdx                             #46.17
..___tag_value__Z18particles_simulateP11t_particles.27:
#       calculate_gravitation_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z30calculate_gravitation_velocityRK10t_particleS1_R10t_velocity #46.17
..___tag_value__Z18particles_simulateP11t_particles.28:
                                # LOE rbx r12 r13 r14 r15
..B1.15:                        # Preds ..B1.14
        movq      13440(%rsp), %rdi                             #46.17
        movq      13432(%rsp), %rsi                             #46.17
        movq      13424(%rsp), %rdx                             #46.17
..___tag_value__Z18particles_simulateP11t_particles.29:
#       calculate_gravitation_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z30calculate_gravitation_velocityRK10t_particleS1_R10t_velocity #46.17
..___tag_value__Z18particles_simulateP11t_particles.30:
                                # LOE rbx r12 r13 r14 r15
..B1.16:                        # Preds ..B1.15
        movq      13464(%rsp), %rdi                             #46.17
        movq      13456(%rsp), %rsi                             #46.17
        movq      13448(%rsp), %rdx                             #46.17
..___tag_value__Z18particles_simulateP11t_particles.31:
#       calculate_gravitation_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z30calculate_gravitation_velocityRK10t_particleS1_R10t_velocity #46.17
..___tag_value__Z18particles_simulateP11t_particles.32:
                                # LOE rbx r12 r13 r14 r15
..B1.17:                        # Preds ..B1.16
        movq      13488(%rsp), %rdi                             #46.17
        movq      13480(%rsp), %rsi                             #46.17
        movq      13472(%rsp), %rdx                             #46.17
..___tag_value__Z18particles_simulateP11t_particles.33:
#       calculate_gravitation_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z30calculate_gravitation_velocityRK10t_particleS1_R10t_velocity #46.17
..___tag_value__Z18particles_simulateP11t_particles.34:
                                # LOE rbx r12 r13 r14 r15
..B1.18:                        # Preds ..B1.17
        movq      13512(%rsp), %rdi                             #46.17
        movq      13504(%rsp), %rsi                             #46.17
        movq      13496(%rsp), %rdx                             #46.17
..___tag_value__Z18particles_simulateP11t_particles.35:
#       calculate_gravitation_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z30calculate_gravitation_velocityRK10t_particleS1_R10t_velocity #46.17
..___tag_value__Z18particles_simulateP11t_particles.36:
                                # LOE rbx r12 r13 r14 r15
..B1.19:                        # Preds ..B1.18
        movq      13536(%rsp), %rdi                             #46.17
        movq      13528(%rsp), %rsi                             #46.17
        movq      13520(%rsp), %rdx                             #46.17
..___tag_value__Z18particles_simulateP11t_particles.37:
#       calculate_gravitation_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z30calculate_gravitation_velocityRK10t_particleS1_R10t_velocity #46.17
..___tag_value__Z18particles_simulateP11t_particles.38:
                                # LOE rbx r12 r13 r14 r15
..B1.20:                        # Preds ..B1.19
        movq      13560(%rsp), %rdi                             #46.17
        movq      13552(%rsp), %rsi                             #46.17
        movq      13544(%rsp), %rdx                             #46.17
..___tag_value__Z18particles_simulateP11t_particles.39:
#       calculate_gravitation_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z30calculate_gravitation_velocityRK10t_particleS1_R10t_velocity #46.17
..___tag_value__Z18particles_simulateP11t_particles.40:
                                # LOE rbx r12 r13 r14 r15
..B1.21:                        # Preds ..B1.20
        movq      %rbx, %rsi                                    #46.17
        movq      %r14, %rdx                                    #46.17
        movq      13568(%rsp), %rdi                             #46.17
..___tag_value__Z18particles_simulateP11t_particles.41:
#       calculate_gravitation_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z30calculate_gravitation_velocityRK10t_particleS1_R10t_velocity #46.17
..___tag_value__Z18particles_simulateP11t_particles.42:
                                # LOE rbx r12 r13 r14 r15
..B1.22:                        # Preds ..B1.21
        movq      13608(%rsp), %rdi                             #47.17
        movq      13616(%rsp), %rsi                             #47.17
        movq      13624(%rsp), %rdx                             #47.17
..___tag_value__Z18particles_simulateP11t_particles.43:
#       calculate_collision_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z28calculate_collision_velocityRK10t_particleS1_R10t_velocity #47.17
..___tag_value__Z18particles_simulateP11t_particles.44:
                                # LOE rbx r12 r13 r14 r15
..B1.23:                        # Preds ..B1.22
        movq      13264(%rsp), %rdi                             #47.17
        movq      13584(%rsp), %rsi                             #47.17
        movq      13600(%rsp), %rdx                             #47.17
..___tag_value__Z18particles_simulateP11t_particles.45:
#       calculate_collision_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z28calculate_collision_velocityRK10t_particleS1_R10t_velocity #47.17
..___tag_value__Z18particles_simulateP11t_particles.46:
                                # LOE rbx r12 r13 r14 r15
..B1.24:                        # Preds ..B1.23
        movq      13272(%rsp), %rdi                             #47.17
        movq      13592(%rsp), %rsi                             #47.17
        movq      13576(%rsp), %rdx                             #47.17
..___tag_value__Z18particles_simulateP11t_particles.47:
#       calculate_collision_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z28calculate_collision_velocityRK10t_particleS1_R10t_velocity #47.17
..___tag_value__Z18particles_simulateP11t_particles.48:
                                # LOE rbx r12 r13 r14 r15
..B1.25:                        # Preds ..B1.24
        movq      13296(%rsp), %rdi                             #47.17
        movq      13288(%rsp), %rsi                             #47.17
        movq      13280(%rsp), %rdx                             #47.17
..___tag_value__Z18particles_simulateP11t_particles.49:
#       calculate_collision_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z28calculate_collision_velocityRK10t_particleS1_R10t_velocity #47.17
..___tag_value__Z18particles_simulateP11t_particles.50:
                                # LOE rbx r12 r13 r14 r15
..B1.26:                        # Preds ..B1.25
        movq      13320(%rsp), %rdi                             #47.17
        movq      13312(%rsp), %rsi                             #47.17
        movq      13304(%rsp), %rdx                             #47.17
..___tag_value__Z18particles_simulateP11t_particles.51:
#       calculate_collision_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z28calculate_collision_velocityRK10t_particleS1_R10t_velocity #47.17
..___tag_value__Z18particles_simulateP11t_particles.52:
                                # LOE rbx r12 r13 r14 r15
..B1.27:                        # Preds ..B1.26
        movq      13344(%rsp), %rdi                             #47.17
        movq      13336(%rsp), %rsi                             #47.17
        movq      13328(%rsp), %rdx                             #47.17
..___tag_value__Z18particles_simulateP11t_particles.53:
#       calculate_collision_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z28calculate_collision_velocityRK10t_particleS1_R10t_velocity #47.17
..___tag_value__Z18particles_simulateP11t_particles.54:
                                # LOE rbx r12 r13 r14 r15
..B1.28:                        # Preds ..B1.27
        movq      13368(%rsp), %rdi                             #47.17
        movq      13360(%rsp), %rsi                             #47.17
        movq      13352(%rsp), %rdx                             #47.17
..___tag_value__Z18particles_simulateP11t_particles.55:
#       calculate_collision_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z28calculate_collision_velocityRK10t_particleS1_R10t_velocity #47.17
..___tag_value__Z18particles_simulateP11t_particles.56:
                                # LOE rbx r12 r13 r14 r15
..B1.29:                        # Preds ..B1.28
        movq      13392(%rsp), %rdi                             #47.17
        movq      13384(%rsp), %rsi                             #47.17
        movq      13376(%rsp), %rdx                             #47.17
..___tag_value__Z18particles_simulateP11t_particles.57:
#       calculate_collision_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z28calculate_collision_velocityRK10t_particleS1_R10t_velocity #47.17
..___tag_value__Z18particles_simulateP11t_particles.58:
                                # LOE rbx r12 r13 r14 r15
..B1.30:                        # Preds ..B1.29
        movq      13416(%rsp), %rdi                             #47.17
        movq      13408(%rsp), %rsi                             #47.17
        movq      13400(%rsp), %rdx                             #47.17
..___tag_value__Z18particles_simulateP11t_particles.59:
#       calculate_collision_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z28calculate_collision_velocityRK10t_particleS1_R10t_velocity #47.17
..___tag_value__Z18particles_simulateP11t_particles.60:
                                # LOE rbx r12 r13 r14 r15
..B1.31:                        # Preds ..B1.30
        movq      13440(%rsp), %rdi                             #47.17
        movq      13432(%rsp), %rsi                             #47.17
        movq      13424(%rsp), %rdx                             #47.17
..___tag_value__Z18particles_simulateP11t_particles.61:
#       calculate_collision_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z28calculate_collision_velocityRK10t_particleS1_R10t_velocity #47.17
..___tag_value__Z18particles_simulateP11t_particles.62:
                                # LOE rbx r12 r13 r14 r15
..B1.32:                        # Preds ..B1.31
        movq      13464(%rsp), %rdi                             #47.17
        movq      13456(%rsp), %rsi                             #47.17
        movq      13448(%rsp), %rdx                             #47.17
..___tag_value__Z18particles_simulateP11t_particles.63:
#       calculate_collision_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z28calculate_collision_velocityRK10t_particleS1_R10t_velocity #47.17
..___tag_value__Z18particles_simulateP11t_particles.64:
                                # LOE rbx r12 r13 r14 r15
..B1.33:                        # Preds ..B1.32
        movq      13488(%rsp), %rdi                             #47.17
        movq      13480(%rsp), %rsi                             #47.17
        movq      13472(%rsp), %rdx                             #47.17
..___tag_value__Z18particles_simulateP11t_particles.65:
#       calculate_collision_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z28calculate_collision_velocityRK10t_particleS1_R10t_velocity #47.17
..___tag_value__Z18particles_simulateP11t_particles.66:
                                # LOE rbx r12 r13 r14 r15
..B1.34:                        # Preds ..B1.33
        movq      13512(%rsp), %rdi                             #47.17
        movq      13504(%rsp), %rsi                             #47.17
        movq      13496(%rsp), %rdx                             #47.17
..___tag_value__Z18particles_simulateP11t_particles.67:
#       calculate_collision_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z28calculate_collision_velocityRK10t_particleS1_R10t_velocity #47.17
..___tag_value__Z18particles_simulateP11t_particles.68:
                                # LOE rbx r12 r13 r14 r15
..B1.35:                        # Preds ..B1.34
        movq      13536(%rsp), %rdi                             #47.17
        movq      13528(%rsp), %rsi                             #47.17
        movq      13520(%rsp), %rdx                             #47.17
..___tag_value__Z18particles_simulateP11t_particles.69:
#       calculate_collision_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z28calculate_collision_velocityRK10t_particleS1_R10t_velocity #47.17
..___tag_value__Z18particles_simulateP11t_particles.70:
                                # LOE rbx r12 r13 r14 r15
..B1.36:                        # Preds ..B1.35
        movq      13560(%rsp), %rdi                             #47.17
        movq      13552(%rsp), %rsi                             #47.17
        movq      13544(%rsp), %rdx                             #47.17
..___tag_value__Z18particles_simulateP11t_particles.71:
#       calculate_collision_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z28calculate_collision_velocityRK10t_particleS1_R10t_velocity #47.17
..___tag_value__Z18particles_simulateP11t_particles.72:
                                # LOE rbx r12 r13 r14 r15
..B1.37:                        # Preds ..B1.36
        movq      %rbx, %rsi                                    #47.17
        movq      %r14, %rdx                                    #47.17
        movq      13568(%rsp), %rdi                             #47.17
..___tag_value__Z18particles_simulateP11t_particles.73:
#       calculate_collision_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z28calculate_collision_velocityRK10t_particleS1_R10t_velocity #47.17
..___tag_value__Z18particles_simulateP11t_particles.74:
                                # LOE rbx r12 r13 r14 r15
..B1.38:                        # Preds ..B1.37
        vmovss    13056(%rsp), %xmm0                            #49.35
        vmovss    13152(%rsp), %xmm8                            #49.35
        vmovss    13104(%rsp), %xmm3                            #49.35
        vmovss    13200(%rsp), %xmm11                           #49.35
        vinsertps $16, 13164(%rsp), %xmm8, %xmm9                #49.35
        vinsertps $16, 13068(%rsp), %xmm0, %xmm1                #49.35
        vinsertps $80, 13116(%rsp), %xmm3, %xmm4                #49.35
        vinsertps $80, 13212(%rsp), %xmm11, %xmm12              #49.35
        vinsertps $32, 13176(%rsp), %xmm9, %xmm10               #49.35
        vinsertps $32, 13080(%rsp), %xmm1, %xmm2                #49.35
        vinsertps $96, 13128(%rsp), %xmm4, %xmm5                #49.35
        vinsertps $96, 13224(%rsp), %xmm12, %xmm13              #49.35
        vinsertps $48, 13188(%rsp), %xmm10, %xmm14              #49.35
        vinsertps $48, 13092(%rsp), %xmm2, %xmm6                #49.35
        vinsertps $112, 13140(%rsp), %xmm5, %xmm7               #49.35
        vinsertps $112, 13236(%rsp), %xmm13, %xmm15             #49.35
        vinsertf128 $1, %xmm7, %ymm6, %ymm0                     #49.35
        vinsertf128 $1, %xmm15, %ymm14, %ymm1                   #49.35
        vmovups   %ymm0, 64(%rsp,%r15,4)                        #49.17
        vmovups   %ymm1, 96(%rsp,%r15,4)                        #49.17
        vmovss    13060(%rsp), %xmm2                            #50.35
        vmovss    13156(%rsp), %xmm10                           #50.35
        vmovss    13108(%rsp), %xmm5                            #50.35
        vmovss    13204(%rsp), %xmm13                           #50.35
        vinsertps $16, 13168(%rsp), %xmm10, %xmm11              #50.35
        vinsertps $16, 13072(%rsp), %xmm2, %xmm3                #50.35
        vinsertps $80, 13120(%rsp), %xmm5, %xmm6                #50.35
        vinsertps $80, 13216(%rsp), %xmm13, %xmm14              #50.35
        vinsertps $32, 13180(%rsp), %xmm11, %xmm12              #50.35
        vinsertps $32, 13084(%rsp), %xmm3, %xmm4                #50.35
        vinsertps $96, 13132(%rsp), %xmm6, %xmm7                #50.35
        vinsertps $96, 13228(%rsp), %xmm14, %xmm15              #50.35
        vinsertps $48, 13192(%rsp), %xmm12, %xmm10              #50.35
        vinsertps $48, 13096(%rsp), %xmm4, %xmm8                #50.35
        vinsertps $112, 13144(%rsp), %xmm7, %xmm9               #50.35
        vinsertps $112, 13240(%rsp), %xmm15, %xmm11             #50.35
        vinsertf128 $1, %xmm9, %ymm8, %ymm0                     #50.35
        vinsertf128 $1, %xmm11, %ymm10, %ymm1                   #50.35
        vmovups   %ymm0, 4096(%rsp,%r15,4)                      #50.17
        vmovups   %ymm1, 4128(%rsp,%r15,4)                      #50.17
        vmovss    13064(%rsp), %xmm2                            #51.35
        vmovss    13160(%rsp), %xmm12                           #51.35
        vmovss    13112(%rsp), %xmm5                            #51.35
        vmovss    13208(%rsp), %xmm15                           #51.35
        vinsertps $16, 13172(%rsp), %xmm12, %xmm13              #51.35
        vinsertps $16, 13076(%rsp), %xmm2, %xmm3                #51.35
        vinsertps $80, 13124(%rsp), %xmm5, %xmm6                #51.35
        vinsertps $80, 13220(%rsp), %xmm15, %xmm15              #51.35
        vinsertps $32, 13184(%rsp), %xmm13, %xmm14              #51.35
        vinsertps $32, 13088(%rsp), %xmm3, %xmm4                #51.35
        vinsertps $96, 13136(%rsp), %xmm6, %xmm7                #51.35
        vinsertps $96, 13232(%rsp), %xmm15, %xmm12              #51.35
        vinsertps $48, 13196(%rsp), %xmm14, %xmm13              #51.35
        vinsertps $48, 13100(%rsp), %xmm4, %xmm8                #51.35
        vinsertps $112, 13148(%rsp), %xmm7, %xmm9               #51.35
        vinsertps $112, 13244(%rsp), %xmm12, %xmm14             #51.35
        vinsertf128 $1, %xmm9, %ymm8, %ymm0                     #51.35
        vinsertf128 $1, %xmm14, %ymm13, %ymm1                   #51.35
        vmovups   %ymm0, 8128(%rsp,%r15,4)                      #51.17
        vmovups   %ymm1, 8160(%rsp,%r15,4)                      #51.17
        addq      $16, %r15                                     #42.18
        cmpq      $992, %r15                                    #42.18
        jb        ..B1.6        # Prob 82%                      #42.18
                                # LOE rbx r12 r13 r14 r15
..B1.39:                        # Preds ..B1.38
        xorl      %r15d, %r15d                                  #42.18
        lea       12608(%rsp), %rbx                             #
        lea       12160(%rsp), %r14                             #
                                # LOE rbx r12 r13 r14 r15
..B1.40:                        # Preds ..B1.42 ..B1.39
        movl      (%r12,%r13,4), %eax                           #43.32
        lea       13056(%rsp), %rdx                             #46.17
        movl      4032(%r12,%r13,4), %ecx                       #43.42
        movl      8064(%r12,%r13,4), %r8d                       #43.52
        movl      %eax, -448(%rdx)                              #42.18
        movl      %ecx, -444(%rdx)                              #42.18
        movl      %r8d, -440(%rdx)                              #42.18
        movl      12096(%r12,%r13,4), %r9d                      #43.62
        movl      16128(%r12,%r13,4), %r10d                     #43.72
        movl      20160(%r12,%r13,4), %r11d                     #43.82
        movl      3968(%r12,%r15,4), %eax                       #44.32
        movl      8000(%r12,%r15,4), %ecx                       #44.42
        movl      12032(%r12,%r15,4), %r8d                      #44.52
        movl      24192(%r12,%r13,4), %esi                      #43.92
        movl      28160(%r12,%r15,4), %edi                      #44.92
        movl      %r9d, -436(%rdx)                              #42.18
        movl      %r10d, -432(%rdx)                             #42.18
        movl      %r11d, -428(%rdx)                             #42.18
        movl      %eax, -896(%rdx)                              #42.18
        movl      %ecx, -892(%rdx)                              #42.18
        movl      %r8d, -888(%rdx)                              #42.18
        movl      %esi, -424(%rdx)                              #42.18
        movq      %rbx, %rsi                                    #46.17
        movl      16064(%r12,%r15,4), %r9d                      #44.62
        movl      20096(%r12,%r15,4), %r10d                     #44.72
        movl      24128(%r12,%r15,4), %r11d                     #44.82
        movl      %edi, -872(%rdx)                              #42.18
        movq      %r14, %rdi                                    #46.17
        movl      4032(%rsp,%r15,4), %eax                       #45.27
        movl      8064(%rsp,%r15,4), %ecx                       #45.44
        movl      12096(%rsp,%r15,4), %r8d                      #45.61
        movl      %r9d, -884(%rdx)                              #42.18
        movl      %r10d, -880(%rdx)                             #42.18
        movl      %r11d, -876(%rdx)                             #42.18
        movl      %eax, (%rdx)                                  #42.18
        movl      %ecx, 4(%rdx)                                 #42.18
        movl      %r8d, 8(%rdx)                                 #42.18
        vzeroupper                                              #46.17
..___tag_value__Z18particles_simulateP11t_particles.75:
#       calculate_gravitation_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z30calculate_gravitation_velocityRK10t_particleS1_R10t_velocity #46.17
..___tag_value__Z18particles_simulateP11t_particles.76:
                                # LOE rbx r12 r13 r14 r15
..B1.41:                        # Preds ..B1.40
        movq      %r14, %rdi                                    #47.17
        movq      %rbx, %rsi                                    #47.17
        lea       13056(%rsp), %rdx                             #47.17
..___tag_value__Z18particles_simulateP11t_particles.77:
#       calculate_collision_velocity(const t_particle &, const t_particle &, t_velocity &)
        call      _Z28calculate_collision_velocityRK10t_particleS1_R10t_velocity #47.17
..___tag_value__Z18particles_simulateP11t_particles.78:
                                # LOE rbx r12 r13 r14 r15
..B1.42:                        # Preds ..B1.41
        movl      13056(%rsp), %eax                             #42.18
        movl      13060(%rsp), %ecx                             #42.18
        movl      13064(%rsp), %r8d                             #42.18
        movl      %eax, 4032(%rsp,%r15,4)                       #49.17
        movl      %ecx, 8064(%rsp,%r15,4)                       #50.17
        movl      %r8d, 12096(%rsp,%r15,4)                      #51.17
        incq      %r15                                          #42.18
        cmpq      $8, %r15                                      #42.18
        jb        ..B1.40       # Prob 82%                      #42.18
                                # LOE rbx r12 r13 r14 r15
..B1.43:                        # Preds ..B1.42
        incq      %r13                                          #40.28
        cmpq      $1000, %r13                                   #40.25
        jl        ..B1.5        # Prob 99%                      #40.25
                                # LOE rbx r12 r13 r14
..B1.44:                        # Preds ..B1.43
        movl      (%rsp), %eax                                  #
        xorl      %ecx, %ecx                                    #56.14
        vxorps    %ymm0, %ymm0, %ymm0                           #
        vmovups   .L_2il0floatpacket.16(%rip), %ymm13           #56.14
                                # LOE rcx rbx r12 r14 eax ymm0 ymm13
..B1.45:                        # Preds ..B1.45 ..B1.44
        vmovups   12096(%r12,%rcx,4), %ymm1                     #57.13
        vmovups   16128(%r12,%rcx,4), %ymm2                     #58.13
        vmovups   20160(%r12,%rcx,4), %ymm3                     #59.13
        vaddps    64(%rsp,%rcx,4), %ymm1, %ymm4                 #57.13
        vaddps    4096(%rsp,%rcx,4), %ymm2, %ymm7               #58.13
        vaddps    8128(%rsp,%rcx,4), %ymm3, %ymm10              #59.13
        vmulps    %ymm4, %ymm13, %ymm5                          #61.36

###             pos_y[i] += vel_y[i] * DT;

        vmulps    %ymm7, %ymm13, %ymm8                          #62.36

###             pos_z[i] += vel_z[i] * DT;

        vmulps    %ymm10, %ymm13, %ymm11                        #63.36
        vmovups   %ymm4, 12096(%r12,%rcx,4)                     #57.13
        vmovups   %ymm7, 16128(%r12,%rcx,4)                     #58.13
        vmovups   %ymm10, 20160(%r12,%rcx,4)                    #59.13
        vaddps    (%r12,%rcx,4), %ymm5, %ymm6                   #61.13
        vaddps    4032(%r12,%rcx,4), %ymm8, %ymm9               #62.13
        vaddps    8064(%r12,%rcx,4), %ymm11, %ymm12             #63.13
        vmovups   %ymm6, (%r12,%rcx,4)                          #61.13
        vmovups   %ymm9, 4032(%r12,%rcx,4)                      #62.13
        vmovups   %ymm12, 8064(%r12,%rcx,4)                     #63.13
        addq      $8, %rcx                                      #56.14
        cmpq      $1000, %rcx                                   #56.14
        jb        ..B1.45       # Prob 82%                      #56.14
                                # LOE rcx rbx r12 r14 eax ymm0 ymm13
..B1.46:                        # Preds ..B1.45
        incl      %eax                                          #31.28
        cmpl      $1000, %eax                                   #31.21
        jl        ..B1.2        # Prob 99%                      #31.21
                                # LOE rbx r12 r14 eax ymm0
..B1.47:                        # Preds ..B1.46

###         }
###     }
### }

        vzeroupper                                              #66.1
        addq      $13656, %rsp                                  #66.1
	.cfi_restore 3
        popq      %rbx                                          #66.1
	.cfi_restore 15
        popq      %r15                                          #66.1
	.cfi_restore 14
        popq      %r14                                          #66.1
	.cfi_restore 13
        popq      %r13                                          #66.1
	.cfi_restore 12
        popq      %r12                                          #66.1
        movq      %rbp, %rsp                                    #66.1
        popq      %rbp                                          #66.1
	.cfi_def_cfa 7, 8
	.cfi_restore 6
        ret                                                     #66.1
        .align    16,0x90
	.cfi_endproc
                                # LOE
# mark_end;
	.type	_Z18particles_simulateP11t_particles,@function
	.size	_Z18particles_simulateP11t_particles,.-_Z18particles_simulateP11t_particles
	.data
# -- End  _Z18particles_simulateP11t_particles
	.text
# -- Begin  _Z14particles_readP8_IO_FILEP11t_particles
	.text
# mark_begin;
       .align    16,0x90
	.globl _Z14particles_readP8_IO_FILEP11t_particles
# --- particles_read(FILE *, t_particles *)
_Z14particles_readP8_IO_FILEP11t_particles:
# parameter 1: %rdi
# parameter 2: %rsi
..B2.1:                         # Preds ..B2.0

### void particles_read(FILE *fp, t_particles* p) {

	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
..___tag_value__Z14particles_readP8_IO_FILEP11t_particles.87:
..L88:
                                                         #69.47
        pushq     %r12                                          #69.47
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
        pushq     %r13                                          #69.47
	.cfi_def_cfa_offset 24
	.cfi_offset 13, -24
        pushq     %r14                                          #69.47
	.cfi_def_cfa_offset 32
	.cfi_offset 14, -32

###     for (int i = 0; i < N; i++) {

        xorl      %eax, %eax                                    #70.16
        movq      %rax, %r12                                    #70.16
        movq      %rsi, %r13                                    #70.16
        movq      %rdi, %r14                                    #70.16
                                # LOE rbx rbp r12 r13 r14 r15
..B2.2:                         # Preds ..B2.3 ..B2.1

###         fscanf(fp, "%f %f %f %f %f %f %f \n",

        addq      $-32, %rsp                                    #71.9
	.cfi_def_cfa_offset 64

###                &p->pos_x[i], &p->pos_y[i], &p->pos_z[i],
###                &p->vel_x[i], &p->vel_y[i], &p->vel_z[i],
###                &p->weight[i]);

        lea       (%r13,%r12,4), %rdx                           #74.17
        movq      %r14, %rdi                                    #71.9
        lea       4032(%rdx), %rcx                              #71.9
        movl      $.L_2__STRING.0, %esi                         #71.9
        lea       8064(%rdx), %r8                               #71.9
        xorl      %eax, %eax                                    #71.9
        lea       12096(%rdx), %r9                              #71.9
        lea       16128(%rdx), %r10                             #71.9
        movq      %r10, (%rsp)                                  #71.9
        lea       20160(%rdx), %r11                             #71.9
        movq      %r11, 8(%rsp)                                 #71.9
        lea       24192(%rdx), %r10                             #71.9
        movq      %r10, 16(%rsp)                                #71.9
#       fscanf(FILE *, const char *, ...)
        call      fscanf                                        #71.9
                                # LOE rbx rbp r12 r13 r14 r15
..B2.7:                         # Preds ..B2.2
        addq      $32, %rsp                                     #71.9
	.cfi_def_cfa_offset 32
                                # LOE rbx rbp r12 r13 r14 r15
..B2.3:                         # Preds ..B2.7
        incq      %r12                                          #70.28
        cmpq      $1000, %r12                                   #70.25
        jl        ..B2.2        # Prob 99%                      #70.25
                                # LOE rbx rbp r12 r13 r14 r15
..B2.4:                         # Preds ..B2.3

###     }
### }

	.cfi_restore 14
        popq      %r14                                          #76.1
	.cfi_def_cfa_offset 24
	.cfi_restore 13
        popq      %r13                                          #76.1
	.cfi_def_cfa_offset 16
	.cfi_restore 12
        popq      %r12                                          #76.1
	.cfi_def_cfa_offset 8
        ret                                                     #76.1
        .align    16,0x90
	.cfi_endproc
                                # LOE
# mark_end;
	.type	_Z14particles_readP8_IO_FILEP11t_particles,@function
	.size	_Z14particles_readP8_IO_FILEP11t_particles,.-_Z14particles_readP8_IO_FILEP11t_particles
	.data
# -- End  _Z14particles_readP8_IO_FILEP11t_particles
	.text
# -- Begin  _Z15particles_writeP8_IO_FILEP11t_particles
	.text
# mark_begin;
       .align    16,0x90
	.globl _Z15particles_writeP8_IO_FILEP11t_particles
# --- particles_write(FILE *, t_particles *)
_Z15particles_writeP8_IO_FILEP11t_particles:
# parameter 1: %rdi
# parameter 2: %rsi
..B3.1:                         # Preds ..B3.0

### void particles_write(FILE *fp, t_particles* p) {

	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
..___tag_value__Z15particles_writeP8_IO_FILEP11t_particles.104:
..L105:
                                                        #78.48
        pushq     %r12                                          #78.48
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
        pushq     %r13                                          #78.48
	.cfi_def_cfa_offset 24
	.cfi_offset 13, -24
        pushq     %r14                                          #78.48
	.cfi_def_cfa_offset 32
	.cfi_offset 14, -32

###     for (int i = 0; i < N; i++) {

        xorl      %eax, %eax                                    #79.16
        movq      %rax, %r12                                    #79.16
        movq      %rsi, %r14                                    #79.16
        movq      %rdi, %r13                                    #79.16
                                # LOE rbx rbp r12 r13 r14 r15
..B3.2:                         # Preds ..B3.3 ..B3.1

###         fprintf(fp, "%10.10f %10.10f %10.10f %10.10f %10.10f %10.10f %10.10f \n",

        vxorpd    %xmm0, %xmm0, %xmm0                           #80.9
        vxorpd    %xmm1, %xmm1, %xmm1                           #80.9
        vxorpd    %xmm2, %xmm2, %xmm2                           #80.9
        vxorpd    %xmm3, %xmm3, %xmm3                           #80.9
        vxorpd    %xmm4, %xmm4, %xmm4                           #80.9
        vxorpd    %xmm5, %xmm5, %xmm5                           #80.9
        vxorpd    %xmm6, %xmm6, %xmm6                           #80.9
        movq      %r13, %rdi                                    #80.9
        vcvtss2sd (%r14,%r12,4), %xmm0, %xmm0                   #80.9
        vcvtss2sd 4032(%r14,%r12,4), %xmm1, %xmm1               #80.9
        vcvtss2sd 8064(%r14,%r12,4), %xmm2, %xmm2               #80.9
        vcvtss2sd 12096(%r14,%r12,4), %xmm3, %xmm3              #80.9
        vcvtss2sd 16128(%r14,%r12,4), %xmm4, %xmm4              #80.9
        vcvtss2sd 20160(%r14,%r12,4), %xmm5, %xmm5              #80.9
        vcvtss2sd 24192(%r14,%r12,4), %xmm6, %xmm6              #80.9
        movl      $.L_2__STRING.1, %esi                         #80.9
        movl      $7, %eax                                      #80.9
#       fprintf(FILE *, const char *, ...)
        call      fprintf                                       #80.9
                                # LOE rbx rbp r12 r13 r14 r15
..B3.3:                         # Preds ..B3.2
        incq      %r12                                          #79.28
        cmpq      $1000, %r12                                   #79.25
        jl        ..B3.2        # Prob 99%                      #79.25
                                # LOE rbx rbp r12 r13 r14 r15
..B3.4:                         # Preds ..B3.3

###                 p->pos_x[i], p->pos_y[i], p->pos_z[i],
###                 p->vel_x[i], p->vel_y[i], p->vel_z[i],
###                 p->weight[i]);
###     }
### }

	.cfi_restore 14
        popq      %r14                                          #85.1
	.cfi_def_cfa_offset 24
	.cfi_restore 13
        popq      %r13                                          #85.1
	.cfi_def_cfa_offset 16
	.cfi_restore 12
        popq      %r12                                          #85.1
	.cfi_def_cfa_offset 8
        ret                                                     #85.1
        .align    16,0x90
	.cfi_endproc
                                # LOE
# mark_end;
	.type	_Z15particles_writeP8_IO_FILEP11t_particles,@function
	.size	_Z15particles_writeP8_IO_FILEP11t_particles,.-_Z15particles_writeP8_IO_FILEP11t_particles
	.data
# -- End  _Z15particles_writeP8_IO_FILEP11t_particles
	.section .rodata, "a"
	.align 32
	.align 32
.L_2il0floatpacket.16:
	.long	0x3a83126f,0x3a83126f,0x3a83126f,0x3a83126f,0x3a83126f,0x3a83126f,0x3a83126f,0x3a83126f
	.type	.L_2il0floatpacket.16,@object
	.size	.L_2il0floatpacket.16,32
	.align 16
.L_2il0floatpacket.0:
	.long	0x00000000,0x00000000,0x0000001c,0x00000000
	.type	.L_2il0floatpacket.0,@object
	.size	.L_2il0floatpacket.0,16
	.align 16
.L_2il0floatpacket.1:
	.long	0x00000038,0x00000000,0x00000054,0x00000000
	.type	.L_2il0floatpacket.1,@object
	.size	.L_2il0floatpacket.1,16
	.align 16
.L_2il0floatpacket.2:
	.long	0x00000070,0x00000000,0x0000008c,0x00000000
	.type	.L_2il0floatpacket.2,@object
	.size	.L_2il0floatpacket.2,16
	.align 16
.L_2il0floatpacket.3:
	.long	0x000000a8,0x00000000,0x000000c4,0x00000000
	.type	.L_2il0floatpacket.3,@object
	.size	.L_2il0floatpacket.3,16
	.align 16
.L_2il0floatpacket.4:
	.long	0x000000e0,0x00000000,0x000000fc,0x00000000
	.type	.L_2il0floatpacket.4,@object
	.size	.L_2il0floatpacket.4,16
	.align 16
.L_2il0floatpacket.5:
	.long	0x00000118,0x00000000,0x00000134,0x00000000
	.type	.L_2il0floatpacket.5,@object
	.size	.L_2il0floatpacket.5,16
	.align 16
.L_2il0floatpacket.6:
	.long	0x00000150,0x00000000,0x0000016c,0x00000000
	.type	.L_2il0floatpacket.6,@object
	.size	.L_2il0floatpacket.6,16
	.align 16
.L_2il0floatpacket.7:
	.long	0x00000188,0x00000000,0x000001a4,0x00000000
	.type	.L_2il0floatpacket.7,@object
	.size	.L_2il0floatpacket.7,16
	.align 16
.L_2il0floatpacket.8:
	.long	0x00000000,0x00000000,0x0000000c,0x00000000
	.type	.L_2il0floatpacket.8,@object
	.size	.L_2il0floatpacket.8,16
	.align 16
.L_2il0floatpacket.9:
	.long	0x00000018,0x00000000,0x00000024,0x00000000
	.type	.L_2il0floatpacket.9,@object
	.size	.L_2il0floatpacket.9,16
	.align 16
.L_2il0floatpacket.10:
	.long	0x00000030,0x00000000,0x0000003c,0x00000000
	.type	.L_2il0floatpacket.10,@object
	.size	.L_2il0floatpacket.10,16
	.align 16
.L_2il0floatpacket.11:
	.long	0x00000048,0x00000000,0x00000054,0x00000000
	.type	.L_2il0floatpacket.11,@object
	.size	.L_2il0floatpacket.11,16
	.align 16
.L_2il0floatpacket.12:
	.long	0x00000060,0x00000000,0x0000006c,0x00000000
	.type	.L_2il0floatpacket.12,@object
	.size	.L_2il0floatpacket.12,16
	.align 16
.L_2il0floatpacket.13:
	.long	0x00000078,0x00000000,0x00000084,0x00000000
	.type	.L_2il0floatpacket.13,@object
	.size	.L_2il0floatpacket.13,16
	.align 16
.L_2il0floatpacket.14:
	.long	0x00000090,0x00000000,0x0000009c,0x00000000
	.type	.L_2il0floatpacket.14,@object
	.size	.L_2il0floatpacket.14,16
	.align 16
.L_2il0floatpacket.15:
	.long	0x000000a8,0x00000000,0x000000b4,0x00000000
	.type	.L_2il0floatpacket.15,@object
	.size	.L_2il0floatpacket.15,16
	.section .rodata.str1.4, "aMS",@progbits,1
	.align 4
	.align 4
.L_2__STRING.0:
	.long	622880293
	.long	1713709158
	.long	543565088
	.long	622880293
	.long	1713709158
	.word	2592
	.byte	0
	.type	.L_2__STRING.0,@object
	.size	.L_2__STRING.0,23
	.space 1, 0x00 	# pad
	.align 4
.L_2__STRING.1:
	.long	774910245
	.long	543567921
	.long	774910245
	.long	543567921
	.long	774910245
	.long	543567921
	.long	774910245
	.long	543567921
	.long	774910245
	.long	543567921
	.long	774910245
	.long	543567921
	.long	774910245
	.long	543567921
	.word	10
	.type	.L_2__STRING.1,@object
	.size	.L_2__STRING.1,58
	.data
	.section .note.GNU-stack, ""
// -- Begin DWARF2 SEGMENT .eh_frame
	.section .eh_frame,"a",@progbits
.eh_frame_seg:
	.align 8
# End

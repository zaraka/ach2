#!/bin/sh

#Step 0 make (no openMP)
#parameters N DT Steps
MakeSerial () {
	icpc -std=c++11 -lpapi -ansi-alias -O2 -Wall -DN=$1 -DDT=$2 -DSTEPS=$3 -c ../velocity.cpp
	icpc -std=c++11 -lpapi -ansi-alias -O2 -Wall -DN=$1 -DDT=$2 -DSTEPS=$3 -c ../nbody.cpp
	icpc -std=c++11 -lpapi -ansi-alias -O2 -Wall -DN=$1 -DDT=$2 -DSTEPS=$3 velocity.o nbody.o ../main.cpp -o nbody
}

#Step 0 make (no openMP)
#parameters N DT Steps
MakeVector () {
	icpc -std=c++11 -lpapi -ansi-alias -O2 -Wall -xavx -qopenmp-simd -DN=$1 -DDT=$2 -DSTEPS=$3 -c ../velocity.cpp
	icpc -std=c++11 -lpapi -ansi-alias -O2 -Wall -xavx -qopenmp-simd -DN=$1 -DDT=$2 -DSTEPS=$3 -c ../nbody.cpp
	icpc -std=c++11 -lpapi -ansi-alias -O2 -Wall -xavx -qopenmp-simd -DN=$1 -DDT=$2 -DSTEPS=$3 velocity.o nbody.o ../main.cpp -o nbody
}

#clean files
rm -rf ~test-outputs
mkdir ~test-outputs

#Test: Two particles on circle
echo "Two particles on circular trajectory..."
MakeSerial 2 0.00001f 543847
./nbody ../../test-data/circle.dat ~test-outputs/circle.out >> /dev/null
./test-difference.py ~test-outputs/circle.out ../../test-data/circle-ref.dat


#Test:
echo "Points on line with colision... without vectorization..."
MakeSerial 32 0.001f 10000
./nbody ../../test-data/two-lines.dat ~test-outputs/two-lines.out >> /dev/null
./test-difference.py ~test-outputs/two-lines.out ../../test-data/two-lines-ref.dat

#Test:
echo "Points on globe with uniform placement ...with vectorization..."
MakeVector 32 0.001f 10000
./nbody ../../test-data/two-lines.dat ~test-outputs/two-lines-v.out >> /dev/null
./test-difference.py ~test-outputs/two-lines-v.out ../../test-data/two-lines-ref.dat


#Test:
echo "Symetry globe test...without vectorization..."
MakeSerial 932 0.1f 1
./nbody ../../test-data/thompson_points_932.dat ~test-outputs/thompson.out >> /dev/null
./test-thompson.py ../../test-data/thompson_points_932.dat ~test-outputs/thompson.out


#Test:
echo "Symetry globe test...with vectorization..."
MakeVector 932 0.1f 1
./nbody ../../test-data/thompson_points_932.dat ~test-outputs/thompson-v.out >> /dev/null
./test-thompson.py ../../test-data/thompson_points_932.dat ~test-outputs/thompson-v.out


#Test:
echo "Stability globe test...without vectorization..."
MakeSerial 932 0.00001f 15000
./nbody ../../test-data/thompson_points_932.dat ~test-outputs/thompson.out >> /dev/null
./test-thompson.py ../../test-data/thompson_points_932.dat ~test-outputs/thompson.out


#Test:
echo "Stability globe test...with vectorization..."
MakeVector 932 0.00001f 15000
./nbody ../../test-data/thompson_points_932.dat ~test-outputs/thompson-v.out >> /dev/null
./test-thompson.py ../../test-data/thompson_points_932.dat ~test-outputs/thompson-v.out

rm *.o

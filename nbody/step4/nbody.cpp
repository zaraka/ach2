/*
 * Architektura procesoru (ACH 2016)
 * Projekt c. 1 (nbody)
 * Login: xvanku00
 */

#include "nbody.h"

#include <cmath>
#include <cfloat>
#include <iostream>

void particles_simulate(t_particles *p) {
    int i, j, k;

    t_velocities velocities;

    float *pos_x = p->pos_x;
    float *pos_y = p->pos_y;
    float *pos_z = p->pos_z;
    float *vel_x = p->vel_x;
    float *vel_y = p->vel_y;
    float *vel_z = p->vel_z;
    float *weight = p->weight;

    for (k = 0; k < STEPS; k++) {
        //vynulovani mezisouctu
        #pragma omp simd
        for (i = 0; i < N; i++) {
            velocities.x[i] = 0.0f;
            velocities.y[i] = 0.0f;
            velocities.z[i] = 0.0f;
        }
        //vypocet nove rychlosti
        for (i = 0; i < N; i++) {

            float acc_vel_x = 0.0f;
            float acc_vel_y = 0.0f;
            float acc_vel_z = 0.0f;

            //float * ptr_pos_x = p->pos_x;

            //
            
            #pragma omp simd reduction(-:acc_vel_x) reduction(-:acc_vel_y) reduction(-:acc_vel_z) aligned(pos_x, pos_y, pos_z, vel_x, vel_y, vel_z, weight: 64)
            for (j = i + 1; j < N; j++) {

                float r, dx, dy, dz;
                float vx1, vy1, vz1, vx2, vy2, vz2;
                float vxc1, vyc1, vzc1, vxc2, vyc2, vzc2;

                dx = pos_x[i] - pos_x[j];
                dy = pos_y[i] - pos_y[j];
                dz = pos_z[i] - pos_z[j];

                r = sqrt(pow(dx, 2.0) + pow(dy, 2.0) + pow(dz, 2.0));

                const float r3 = pow(r, 3.0);
                const float gdt = G * DT;
                const float help1 = weight[i] * gdt;

                vx1 = (r > COLLISION_DISTANCE) ? (help1 * dx) / r3 : 0.0f;
                vy1 = (r > COLLISION_DISTANCE) ? (help1 * dy) / r3 : 0.0f;
                vz1 = (r > COLLISION_DISTANCE) ? (help1 * dz) / r3 : 0.0f;

                const float help2 = weight[j] * gdt;

                vx2 = (r > COLLISION_DISTANCE) ? (help2 * dx) / r3 : 0.0f;
                vy2 = (r > COLLISION_DISTANCE) ? (help2 * dy) / r3 : 0.0f;
                vz2 = (r > COLLISION_DISTANCE) ? (help2 * dz) / r3 : 0.0f;

                const float m2 = weight[j] + weight[i];

                const float j_weight_2_1 = 2.0f * weight[i];
                const float j_weight_2_2 = 2.0f * weight[j];
                
                float wx1 = (weight[j] * vel_x[j] - weight[i] * vel_x[j] + j_weight_2_1 * vel_x[i]) / m2;
                float wy1 = (weight[j] * vel_y[j] - weight[i] * vel_y[j] + j_weight_2_1 * vel_y[i]) / m2;
                float wz1 = (weight[j] * vel_z[j] - weight[i] * vel_z[j] + j_weight_2_1 * vel_z[i]) / m2;

                float wx2 = (weight[i] * vel_x[i] - weight[j] * vel_x[i] + j_weight_2_2 * vel_x[j]) / m2;
                float wy2 = (weight[i] * vel_y[i] - weight[j] * vel_y[i] + j_weight_2_2 * vel_y[j]) / m2;
                float wz2 = (weight[i] * vel_z[i] - weight[j] * vel_z[i] + j_weight_2_2 * vel_z[j]) / m2;

                vxc1 += (r > 0.0f && r < COLLISION_DISTANCE) ? wx1 - vel_x[j] : 0.0f;
                vyc1 += (r > 0.0f && r < COLLISION_DISTANCE) ? wy1 - vel_y[j] : 0.0f;
                vzc1 += (r > 0.0f && r < COLLISION_DISTANCE) ? wz1 - vel_z[j] : 0.0f;

                vxc2 += (r > 0.0f && r < COLLISION_DISTANCE) ? wx2 - vel_x[i] : 0.0f;
                vyc2 += (r > 0.0f && r < COLLISION_DISTANCE) ? wy2 - vel_y[i] : 0.0f;
                vzc2 += (r > 0.0f && r < COLLISION_DISTANCE) ? wz2 - vel_z[i] : 0.0f;

                velocities.x[j] += vx1 + vxc1;
                velocities.y[j] += vy1 + vyc1;
                velocities.z[j] += vz1 + vzc1;

                acc_vel_x -= vx2 - vxc2;
                acc_vel_y -= vy2 - vyc2;
                acc_vel_z -= vz2 - vzc2;
            }

            velocities.x[i] += acc_vel_x;
            velocities.y[i] += acc_vel_y;
            velocities.z[i] += acc_vel_z;

        }
        //ulozeni rychlosti a posun castic
        #pragma omp simd aligned(pos_x, pos_y, pos_z, vel_x, vel_y, vel_z, weight: 64)
        for (i = 0; i < N; i++) {
            vel_x[i] += velocities.x[i];
            vel_y[i] += velocities.y[i];
            vel_z[i] += velocities.z[i];

            pos_x[i] += vel_x[i] * DT;
            pos_y[i] += vel_y[i] * DT;
            pos_z[i] += vel_z[i] * DT;
        }
    }
}


void particles_read(FILE *fp, t_particles* p) {
    for (int i = 0; i < N; i++) {
        fscanf(fp, "%f %f %f %f %f %f %f \n",
               &p->pos_x[i], &p->pos_y[i], &p->pos_z[i],
               &p->vel_x[i], &p->vel_y[i], &p->vel_z[i],
               &p->weight[i]);
    }
}

void particles_write(FILE *fp, t_particles* p) {
    for (int i = 0; i < N; i++) {
        fprintf(fp, "%10.10f %10.10f %10.10f %10.10f %10.10f %10.10f %10.10f \n",
                p->pos_x[i], p->pos_y[i], p->pos_z[i],
                p->vel_x[i], p->vel_y[i], p->vel_z[i],
                p->weight[i]);
    }
}

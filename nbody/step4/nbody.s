# mark_description "Intel(R) C++ Intel(R) 64 Compiler for applications running on Intel(R) 64, Version 16.0.1.150 Build 20151021";
# mark_description "";
# mark_description "-std=c++11 -lpapi -ansi-alias -O2 -Wall -xavx -qopenmp-simd -DN=1000 -DDT=0.001f -DSTEPS=1000 -S -fsource-as";
# mark_description "m -c";
	.file "nbody.cpp"
	.text
..TXTST0:
# -- Begin  _Z18particles_simulateP11t_particles
	.text
# mark_begin;
       .align    16,0x90
	.globl _Z18particles_simulateP11t_particles
# --- particles_simulate(t_particles *)
_Z18particles_simulateP11t_particles:
# parameter 1: %rdi
..B1.1:                         # Preds ..B1.0

### void particles_simulate(t_particles *p) {

	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
..___tag_value__Z18particles_simulateP11t_particles.1:
..L2:
                                                          #13.41
        pushq     %rbp                                          #13.41
	.cfi_def_cfa_offset 16
        movq      %rsp, %rbp                                    #13.41
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
        andq      $-64, %rsp                                    #13.41
        pushq     %r12                                          #13.41
        pushq     %r13                                          #13.41
        pushq     %r14                                          #13.41
        pushq     %r15                                          #13.41
        pushq     %rbx                                          #13.41
        subq      $13016, %rsp                                  #13.41

###     int i, j, k;
### 
###     t_velocities velocities;
### 
###     float *pos_x = p->pos_x;
###     float *pos_y = p->pos_y;
###     float *pos_z = p->pos_z;
###     float *vel_x = p->vel_x;
###     float *vel_y = p->vel_y;
###     float *vel_z = p->vel_z;
###     float *weight = p->weight;
### 
###     for (k = 0; k < STEPS; k++) {

        xorl      %esi, %esi                                    #26.5

###         //vynulovani mezisouctu
###         #pragma omp simd
###         for (i = 0; i < N; i++) {
###             velocities.x[i] = 0.0f;

        vxorps    %ymm1, %ymm1, %ymm1                           #30.31

###             velocities.y[i] = 0.0f;
###             velocities.z[i] = 0.0f;
###         }
###         //vypocet nove rychlosti
###         for (i = 0; i < N; i++) {
### 
###             float acc_vel_x = 0.0f;

        vxorps    %xmm8, %xmm8, %xmm8                           #37.29

###             float acc_vel_y = 0.0f;
###             float acc_vel_z = 0.0f;
### 
###             //float * ptr_pos_x = p->pos_x;
### 
###             //
###             
###             #pragma omp simd reduction(-:acc_vel_x) reduction(-:acc_vel_y) reduction(-:acc_vel_z) aligned(pos_x, pos_y, pos_z, vel_x, vel_y, vel_z, weight: 64)
###             for (j = i + 1; j < N; j++) {
### 
###                 float r, dx, dy, dz;
###                 float vx1, vy1, vz1, vx2, vy2, vz2;
###                 float vxc1, vyc1, vzc1, vxc2, vyc2, vzc2;
### 
###                 dx = pos_x[i] - pos_x[j];
###                 dy = pos_y[i] - pos_y[j];
###                 dz = pos_z[i] - pos_z[j];
### 
###                 r = sqrt(pow(dx, 2.0) + pow(dy, 2.0) + pow(dz, 2.0));
### 
###                 const float r3 = pow(r, 3.0);
###                 const float gdt = G * DT;
###                 const float help1 = weight[i] * gdt;
### 
###                 vx1 = (r > COLLISION_DISTANCE) ? (help1 * dx) / r3 : 0.0f;
###                 vy1 = (r > COLLISION_DISTANCE) ? (help1 * dy) / r3 : 0.0f;
###                 vz1 = (r > COLLISION_DISTANCE) ? (help1 * dz) / r3 : 0.0f;
### 
###                 const float help2 = weight[j] * gdt;
### 
###                 vx2 = (r > COLLISION_DISTANCE) ? (help2 * dx) / r3 : 0.0f;
###                 vy2 = (r > COLLISION_DISTANCE) ? (help2 * dy) / r3 : 0.0f;
###                 vz2 = (r > COLLISION_DISTANCE) ? (help2 * dz) / r3 : 0.0f;
### 
###                 const float m2 = weight[j] + weight[i];
### 
###                 const float j_weight_2_1 = 2.0f * weight[i];
###                 const float j_weight_2_2 = 2.0f * weight[j];
###                 
###                 float wx1 = (weight[j] * vel_x[j] - weight[i] * vel_x[j] + j_weight_2_1 * vel_x[i]) / m2;
###                 float wy1 = (weight[j] * vel_y[j] - weight[i] * vel_y[j] + j_weight_2_1 * vel_y[i]) / m2;
###                 float wz1 = (weight[j] * vel_z[j] - weight[i] * vel_z[j] + j_weight_2_1 * vel_z[i]) / m2;
### 
###                 float wx2 = (weight[i] * vel_x[i] - weight[j] * vel_x[i] + j_weight_2_2 * vel_x[j]) / m2;
###                 float wy2 = (weight[i] * vel_y[i] - weight[j] * vel_y[i] + j_weight_2_2 * vel_y[j]) / m2;
###                 float wz2 = (weight[i] * vel_z[i] - weight[j] * vel_z[i] + j_weight_2_2 * vel_z[j]) / m2;
### 
###                 vxc1 += (r > 0.0f && r < COLLISION_DISTANCE) ? wx1 - vel_x[j] : 0.0f;
###                 vyc1 += (r > 0.0f && r < COLLISION_DISTANCE) ? wy1 - vel_y[j] : 0.0f;
###                 vzc1 += (r > 0.0f && r < COLLISION_DISTANCE) ? wz1 - vel_z[j] : 0.0f;
### 
###                 vxc2 += (r > 0.0f && r < COLLISION_DISTANCE) ? wx2 - vel_x[i] : 0.0f;
###                 vyc2 += (r > 0.0f && r < COLLISION_DISTANCE) ? wy2 - vel_y[i] : 0.0f;
###                 vzc2 += (r > 0.0f && r < COLLISION_DISTANCE) ? wz2 - vel_z[i] : 0.0f;
### 
###                 velocities.x[j] += vx1 + vxc1;
###                 velocities.y[j] += vy1 + vyc1;
###                 velocities.z[j] += vz1 + vzc1;
### 
###                 acc_vel_x -= vx2 - vxc2;
###                 acc_vel_y -= vy2 - vyc2;
###                 acc_vel_z -= vz2 - vzc2;
###             }
### 
###             velocities.x[i] += acc_vel_x;
###             velocities.y[i] += acc_vel_y;
###             velocities.z[i] += acc_vel_z;
### 
###         }
###         //ulozeni rychlosti a posun castic
###         #pragma omp simd aligned(pos_x, pos_y, pos_z, vel_x, vel_y, vel_z, weight: 64)
###         for (i = 0; i < N; i++) {
###             vel_x[i] += velocities.x[i];
###             vel_y[i] += velocities.y[i];
###             vel_z[i] += velocities.z[i];
### 
###             pos_x[i] += vel_x[i] * DT;

        vmovups   .L_2il0floatpacket.0(%rip), %ymm0             #114.36
        vmovss    .L_2il0floatpacket.1(%rip), %xmm7             #60.49
        vmovss    .L_2il0floatpacket.4(%rip), %xmm4             #74.44
        vmovss    .L_2il0floatpacket.6(%rip), %xmm2             #62.28
        vmovups   .L_2il0floatpacket.2(%rip), %ymm6             #62.28
        vmovups   .L_2il0floatpacket.3(%rip), %ymm5             #66.49
        vmovups   .L_2il0floatpacket.5(%rip), %ymm3             #75.44
	.cfi_escape 0x10, 0x03, 0x0e, 0x38, 0x1c, 0x0d, 0xc0, 0xff, 0xff, 0xff, 0x1a, 0x0d, 0xd8, 0xff, 0xff, 0xff, 0x22
	.cfi_escape 0x10, 0x0c, 0x0e, 0x38, 0x1c, 0x0d, 0xc0, 0xff, 0xff, 0xff, 0x1a, 0x0d, 0xf8, 0xff, 0xff, 0xff, 0x22
	.cfi_escape 0x10, 0x0d, 0x0e, 0x38, 0x1c, 0x0d, 0xc0, 0xff, 0xff, 0xff, 0x1a, 0x0d, 0xf0, 0xff, 0xff, 0xff, 0x22
	.cfi_escape 0x10, 0x0e, 0x0e, 0x38, 0x1c, 0x0d, 0xc0, 0xff, 0xff, 0xff, 0x1a, 0x0d, 0xe8, 0xff, 0xff, 0xff, 0x22
	.cfi_escape 0x10, 0x0f, 0x0e, 0x38, 0x1c, 0x0d, 0xc0, 0xff, 0xff, 0xff, 0x1a, 0x0d, 0xe0, 0xff, 0xff, 0xff, 0x22
                                # LOE rdi esi ymm0 ymm1
..B1.2:                         # Preds ..B1.24 ..B1.1
        xorl      %eax, %eax                                    #29.14
        .align    16,0x90
                                # LOE rax rdi esi ymm0 ymm1
..B1.3:                         # Preds ..B1.3 ..B1.2
        vmovups   %ymm1, 64(%rsp,%rax,4)                        #30.13
        vmovups   %ymm1, 4096(%rsp,%rax,4)                      #31.13
        vmovups   %ymm1, 8128(%rsp,%rax,4)                      #32.13
        vmovups   %ymm1, 96(%rsp,%rax,4)                        #30.13
        vmovups   %ymm1, 4128(%rsp,%rax,4)                      #31.13
        vmovups   %ymm1, 8160(%rsp,%rax,4)                      #32.13
        vmovups   %ymm1, 128(%rsp,%rax,4)                       #30.13
        vmovups   %ymm1, 4160(%rsp,%rax,4)                      #31.13
        vmovups   %ymm1, 8192(%rsp,%rax,4)                      #32.13
        vmovups   %ymm1, 160(%rsp,%rax,4)                       #30.13
        vmovups   %ymm1, 4192(%rsp,%rax,4)                      #31.13
        vmovups   %ymm1, 8224(%rsp,%rax,4)                      #32.13
        addq      $32, %rax                                     #29.14
        cmpq      $992, %rax                                    #29.14
        jb        ..B1.3        # Prob 99%                      #29.14
                                # LOE rax rdi esi ymm0 ymm1
..B1.4:                         # Preds ..B1.3
        xorl      %edx, %edx                                    #35.9
        xorl      %ecx, %ecx                                    #35.9
        vmovups   %ymm1, 4032(%rsp)                             #30.13
        xorl      %eax, %eax                                    #35.9
        vmovups   %ymm1, 8064(%rsp)                             #31.13
        vmovups   %ymm1, 12096(%rsp)                            #32.13
        movl      %esi, 8(%rsp)                                 #35.9
        movq      %rdi, 24(%rsp)                                #35.9
                                # LOE rax rdx ecx
..B1.5:                         # Preds ..B1.21 ..B1.4
        incl      %ecx                                          #14.5
        vxorps    %xmm12, %xmm12, %xmm12                        #38.29
        vxorps    %xmm9, %xmm9, %xmm9                           #37.29
        vmovaps   %xmm12, %xmm13                                #39.29
        cmpl      $1000, %ecx                                   #46.41
        jge       ..B1.21       # Prob 50%                      #46.41
                                # LOE rax rdx ecx xmm9 xmm12 xmm13
..B1.6:                         # Preds ..B1.5
        lea       999(%rax), %rsi                               #46.13
        cmpq      $8, %rsi                                      #46.13
        jl        ..B1.26       # Prob 10%                      #46.13
                                # LOE rax rdx rsi ecx xmm9 xmm12 xmm13
..B1.7:                         # Preds ..B1.6
        movq      24(%rsp), %r14                                #79.42
        movl      %esi, %ebx                                    #46.13
        andl      $-8, %ebx                                     #46.13
        xorl      %r8d, %r8d                                    #46.13
        vxorps    %ymm1, %ymm1, %ymm1                           #37.29
        movslq    %ebx, %rbx                                    #46.13
        lea       24192(%r14,%rdx,4), %r12                      #60.37
        vmovss    (%r12), %xmm2                                 #60.37
        lea       12096(%r14,%rdx,4), %r11                      #77.42
        vmulss    (%r11), %xmm2, %xmm3                          #81.42
        lea       20160(%r14,%rdx,4), %r9                       #79.42
        vmulss    (%r9), %xmm2, %xmm7                           #83.42
        vshufps   $0, %xmm3, %xmm3, %xmm15                      #81.42
        lea       16128(%r14,%rdx,4), %r10                      #78.42
        vmulss    (%r10), %xmm2, %xmm5                          #82.42
        lea       8064(%r14,%rdx,4), %r13                       #54.22
        vmulss    .L_2il0floatpacket.1(%rip), %xmm2, %xmm3      #60.49
        vmovss    (%r9), %xmm13                                 #79.91
        lea       4032(%r14,%rdx,4), %rdi                       #53.22
        vmovss    (%r10), %xmm11                                #78.91
        lea       (%r14,%rdx,4), %r15                           #52.22
        vshufps   $0, %xmm7, %xmm7, %xmm6                       #83.42
        movq      %rdx, %r14                                    #46.13
        vshufps   $0, %xmm2, %xmm2, %xmm7                       #72.46
        vmulss    .L_2il0floatpacket.4(%rip), %xmm2, %xmm2      #74.51
        vshufps   $0, %xmm13, %xmm13, %xmm12                    #79.91
        vshufps   $0, %xmm11, %xmm11, %xmm10                    #78.91
        vshufps   $0, %xmm5, %xmm5, %xmm4                       #82.42
        vmovss    (%r11), %xmm9                                 #77.91
        vshufps   $0, %xmm9, %xmm9, %xmm8                       #77.91
        vbroadcastss (%rdi), %xmm5                              #53.22
        vmovaps   %ymm1, %ymm0                                  #38.29
        vmovaps   %ymm1, %ymm14                                 #39.29
        vinsertf128 $1, %xmm12, %ymm12, %ymm13                  #79.91
        vinsertf128 $1, %xmm10, %ymm10, %ymm12                  #78.91
        vinsertf128 $1, %xmm6, %ymm6, %ymm10                    #83.42
        vinsertf128 $1, %xmm4, %ymm4, %ymm9                     #82.42
        vbroadcastss (%r13), %xmm6                              #54.22
        vbroadcastss (%r15), %xmm4                              #52.22
        vmovups   %ymm9, 12352(%rsp)                            #46.13
        vmovups   %ymm10, 12416(%rsp)                           #46.13
        vmovups   %ymm12, 12480(%rsp)                           #46.13
        vmovups   %ymm13, 12512(%rsp)                           #46.13
        vinsertf128 $1, %xmm8, %ymm8, %ymm11                    #77.91
        vinsertf128 $1, %xmm15, %ymm15, %ymm8                   #81.42
        vshufps   $0, %xmm2, %xmm2, %xmm15                      #74.51
        vmovups   %ymm8, 12192(%rsp)                            #81.42
        vmovups   %ymm11, 12544(%rsp)                           #46.13
        vinsertf128 $1, %xmm15, %ymm15, %ymm15                  #74.51
        vmulps    %ymm15, %ymm11, %ymm2                         #77.91
        vmovups   %ymm2, 12288(%rsp)                            #46.13
        vinsertf128 $1, %xmm7, %ymm7, %ymm8                     #72.46
        vinsertf128 $1, %xmm6, %ymm6, %ymm7                     #54.22
        vinsertf128 $1, %xmm5, %ymm5, %ymm6                     #53.22
        vinsertf128 $1, %xmm4, %ymm4, %ymm5                     #52.22
        vshufps   $0, %xmm3, %xmm3, %xmm4                       #60.49
        vmulps    %ymm12, %ymm15, %ymm3                         #78.91
        vmulps    %ymm13, %ymm15, %ymm15                        #79.91
        vmovups   %ymm5, 12320(%rsp)                            #46.13
        vmovups   %ymm6, 12448(%rsp)                            #46.13
        vmovups   %ymm7, 12384(%rsp)                            #46.13
        vmovups   %ymm3, 12256(%rsp)                            #46.13
        vmovups   %ymm15, 12224(%rsp)                           #46.13
        vmovups   %ymm8, 12608(%rsp)                            #46.13
        vinsertf128 $1, %xmm4, %ymm4, %ymm4                     #60.49
        vmovups   %ymm4, 12576(%rsp)                            #46.13
                                # LOE rax rdx rbx rsi rdi r8 r9 r10 r11 r12 r13 r14 r15 ecx ymm0 ymm1 ymm14
..B1.8:                         # Preds ..B1.8 ..B1.7
        vmovups   %ymm1, 12640(%rsp)                            #
        vmovups   12320(%rsp), %ymm4                            #52.33
        vmovups   12448(%rsp), %ymm1                            #53.33
        vmovups   %ymm14, 12704(%rsp)                           #
        vmovups   4(%r15,%r8,4), %xmm6                          #52.33
        vmovups   4(%rdi,%r8,4), %xmm14                         #53.33
        vmovups   12384(%rsp), %ymm5                            #54.33
        vmovups   4(%r13,%r8,4), %xmm15                         #54.33
        vmovups   %ymm0, 12672(%rsp)                            #
        vinsertf128 $1, 20(%r15,%r8,4), %ymm6, %ymm2            #52.33
        vinsertf128 $1, 20(%rdi,%r8,4), %ymm14, %ymm0           #53.33
        vsubps    %ymm2, %ymm4, %ymm8                           #52.33
        vsubps    %ymm0, %ymm1, %ymm7                           #53.33
        vinsertf128 $1, 20(%r13,%r8,4), %ymm15, %ymm3           #54.33
        vsubps    %ymm3, %ymm5, %ymm10                          #54.33
        vextractf128 $1, %ymm8, %xmm13                          #56.30
        vextractf128 $1, %ymm7, %xmm11                          #56.45
        vcvtps2pd %xmm8, %ymm9                                  #56.30
        vcvtps2pd %xmm7, %ymm4                                  #56.45
        vcvtps2pd %xmm13, %ymm6                                 #56.30
        vcvtps2pd %xmm11, %ymm2                                 #56.45
        vmulpd    %ymm9, %ymm9, %ymm14                          #56.26
        vmulpd    %ymm4, %ymm4, %ymm1                           #56.41
        vmulpd    %ymm6, %ymm6, %ymm0                           #56.26
        vmulpd    %ymm2, %ymm2, %ymm15                          #56.41
        vaddpd    %ymm1, %ymm14, %ymm13                         #56.41
        vextractf128 $1, %ymm10, %xmm12                         #56.60
        vcvtps2pd %xmm10, %ymm5                                 #56.60
        vcvtps2pd %xmm12, %ymm3                                 #56.60
        vaddpd    %ymm15, %ymm0, %ymm12                         #56.41
        vmulpd    %ymm5, %ymm5, %ymm11                          #56.56
        vmulpd    %ymm3, %ymm3, %ymm9                           #56.56
        vaddpd    %ymm11, %ymm13, %ymm6                         #56.56
        vaddpd    %ymm9, %ymm12, %ymm2                          #56.56
        vsqrtpd   %ymm6, %ymm4                                  #56.21
        vsqrtpd   %ymm2, %ymm14                                 #56.21
        vcvtpd2ps %ymm4, %xmm1                                  #56.21
        vcvtpd2ps %ymm14, %xmm0                                 #56.21
        vcvtps2pd %xmm0, %ymm3                                  #58.38
        vmulpd    %ymm3, %ymm3, %ymm13                          #58.34
        vmulpd    %ymm13, %ymm3, %ymm12                         #58.34
        vcvtpd2ps %ymm12, %xmm6                                 #58.34
        vmovups   12576(%rsp), %ymm12                           #62.59
        vmulps    %ymm12, %ymm8, %ymm3                          #62.59
        vcvtps2pd %xmm1, %ymm15                                 #58.38
        vmulpd    %ymm15, %ymm15, %ymm5                         #58.34
        vmulpd    %ymm5, %ymm15, %ymm11                         #58.34
        vcvtpd2ps %ymm11, %xmm9                                 #58.34
        vinsertf128 $1, %xmm0, %ymm1, %ymm2                     #56.21
        vmovups   .L_2il0floatpacket.2(%rip), %ymm0             #62.28
        vinsertf128 $1, %xmm6, %ymm9, %ymm4                     #58.34
        vrcpps    %ymm4, %ymm1                                  #62.65
        vcmpgtps  %ymm0, %ymm2, %ymm9                           #62.28
        vmulps    %ymm4, %ymm1, %ymm14                          #62.65
        vmulps    %ymm7, %ymm12, %ymm4                          #63.59
        vmulps    %ymm10, %ymm12, %ymm12                        #64.59
        vaddps    %ymm1, %ymm1, %ymm15                          #62.65
        vmulps    %ymm1, %ymm14, %ymm5                          #62.65
        vsubps    %ymm5, %ymm15, %ymm11                         #62.65
        vmovups   4(%r12,%r8,4), %xmm15                         #66.37
        vmulps    %ymm11, %ymm4, %ymm14                         #63.65
        vmulps    %ymm11, %ymm3, %ymm13                         #62.65
        vandps    %ymm9, %ymm14, %ymm1                          #63.65
        vmulps    %ymm11, %ymm12, %ymm14                        #64.65
        vmovups   %ymm1, 12768(%rsp)                            #63.65
        vandps    %ymm9, %ymm14, %ymm1                          #64.65
        vmovups   %ymm1, 12800(%rsp)                            #64.65
        vandps    %ymm9, %ymm13, %ymm6                          #62.65
        vmovups   12608(%rsp), %ymm13                           #72.46
        vmovups   %ymm6, 12736(%rsp)                            #62.65
        vinsertf128 $1, 20(%r12,%r8,4), %ymm15, %ymm1           #66.37
        vmulps    .L_2il0floatpacket.3(%rip), %ymm1, %ymm5      #66.49
        vmulps    %ymm5, %ymm8, %ymm8                           #68.59
        vmulps    %ymm5, %ymm7, %ymm7                           #69.59
        vmulps    %ymm5, %ymm10, %ymm10                         #70.59
        vmovups   4(%r11,%r8,4), %xmm5                          #77.42
        vmulps    %ymm11, %ymm8, %ymm14                         #68.65
        vandps    %ymm9, %ymm14, %ymm15                         #68.65
        vmulps    %ymm11, %ymm7, %ymm14                         #69.65
        vmovups   %ymm15, 12832(%rsp)                           #68.65
        vandps    %ymm9, %ymm14, %ymm15                         #69.65
        vmulps    %ymm11, %ymm10, %ymm14                        #70.65
        vmovups   %ymm15, 12864(%rsp)                           #69.65
        vaddps    %ymm1, %ymm13, %ymm10                         #72.46
        vrcpps    %ymm10, %ymm8                                 #77.103
        vmulps    %ymm10, %ymm8, %ymm7                          #77.103
        vaddps    %ymm8, %ymm8, %ymm6                           #77.103
        vmulps    %ymm8, %ymm7, %ymm4                           #77.103
        vmovups   4(%r9,%r8,4), %xmm8                           #79.42
        vandps    %ymm9, %ymm14, %ymm15                         #70.65
        vmovups   %ymm15, 12896(%rsp)                           #70.65
        vmulps    .L_2il0floatpacket.5(%rip), %ymm1, %ymm14     #75.51
        vsubps    %ymm4, %ymm6, %ymm15                          #77.103
        .byte     102                                           #
        .byte     144                                           #
        vinsertf128 $1, 20(%r11,%r8,4), %ymm5, %ymm3            #77.42
        vmulps    %ymm3, %ymm1, %ymm11                          #77.42
        vmulps    %ymm3, %ymm13, %ymm12                         #77.65
        vsubps    %ymm12, %ymm11, %ymm9                         #77.65
        vmovups   4(%r10,%r8,4), %xmm11                         #78.42
        vaddps    12288(%rsp), %ymm9, %ymm5                     #77.91
        vmulps    %ymm15, %ymm5, %ymm4                          #77.103
        vinsertf128 $1, 20(%r10,%r8,4), %ymm11, %ymm5           #78.42
        vmulps    %ymm5, %ymm1, %ymm12                          #78.42
        vmulps    %ymm5, %ymm13, %ymm9                          #78.65
        vsubps    %ymm9, %ymm12, %ymm10                         #78.65
        vmovups   12544(%rsp), %ymm9                            #81.65
        vmovups   12192(%rsp), %ymm12                           #81.65
        vaddps    12256(%rsp), %ymm10, %ymm7                    #78.91
        vmulps    %ymm1, %ymm9, %ymm10                          #81.65
        vmulps    %ymm15, %ymm7, %ymm6                          #78.103
        vinsertf128 $1, 20(%r9,%r8,4), %ymm8, %ymm7             #79.42
        addq      $8, %r8                                       #46.13
        vmulps    %ymm7, %ymm1, %ymm11                          #79.42
        vmulps    %ymm7, %ymm13, %ymm13                         #79.65
        vsubps    %ymm13, %ymm11, %ymm13                        #79.65
        vaddps    12224(%rsp), %ymm13, %ymm11                   #79.91
        vsubps    %ymm10, %ymm12, %ymm13                        #81.65
        vmulps    %ymm15, %ymm11, %ymm8                         #79.103
        vmulps    %ymm14, %ymm3, %ymm11                         #81.91
        vaddps    %ymm11, %ymm13, %ymm12                        #81.91
        vmovups   12480(%rsp), %ymm11                           #82.65
        vmovups   12352(%rsp), %ymm13                           #82.65
        vmulps    %ymm15, %ymm12, %ymm10                        #81.103
        vmulps    %ymm1, %ymm11, %ymm12                         #82.65
        vsubps    %ymm12, %ymm13, %ymm13                        #82.65
        vmulps    %ymm5, %ymm14, %ymm12                         #82.91
        vmulps    %ymm7, %ymm14, %ymm14                         #83.91
        vaddps    %ymm12, %ymm13, %ymm13                        #82.91
        vmulps    %ymm15, %ymm13, %ymm12                        #82.103
        vmulps    12512(%rsp), %ymm1, %ymm13                    #83.65
        vmovups   12416(%rsp), %ymm1                            #83.65
        vsubps    %ymm13, %ymm1, %ymm1                          #83.65
        vaddps    %ymm14, %ymm1, %ymm14                         #83.91
        vsubps    %ymm3, %ymm4, %ymm1                           #85.70
        vmulps    %ymm15, %ymm14, %ymm13                        #83.103
        vxorps    %ymm15, %ymm15, %ymm15                        #85.30
        vcmpgtps  %ymm15, %ymm2, %ymm14                         #85.30
        vcmpltps  %ymm0, %ymm2, %ymm2                           #85.42
        vsubps    %ymm5, %ymm6, %ymm0                           #86.70
        vsubps    %ymm9, %ymm10, %ymm15                         #89.70
        vmovups   68(%rsp,%r14,4), %xmm6                        #93.17
        vmovups   12832(%rsp), %ymm10                           #97.36
        vsubps    12512(%rsp), %ymm13, %ymm4                    #91.70
        vandps    %ymm2, %ymm14, %ymm14                         #85.42
        vsubps    %ymm11, %ymm12, %ymm2                         #90.70
        vmovups   12864(%rsp), %ymm12                           #98.36
        vandps    %ymm14, %ymm1, %ymm3                          #85.70
        vsubps    %ymm7, %ymm8, %ymm1                           #87.70
        vaddps    12736(%rsp), %ymm3, %ymm3                     #85.17
        vandps    %ymm14, %ymm0, %ymm5                          #86.70
        vandps    %ymm14, %ymm1, %ymm0                          #87.70
        vandps    %ymm14, %ymm15, %ymm1                         #89.70
        vandps    %ymm14, %ymm2, %ymm15                         #90.70
        vsubps    %ymm15, %ymm12, %ymm13                        #98.36
        vaddps    12800(%rsp), %ymm0, %ymm0                     #87.17
        vaddps    12768(%rsp), %ymm5, %ymm5                     #86.17
        vsubps    %ymm1, %ymm10, %ymm11                         #97.36
        vmovups   12896(%rsp), %ymm15                           #99.36
        vmovups   12640(%rsp), %ymm1                            #97.17
        vandps    %ymm14, %ymm4, %ymm14                         #91.70
        vsubps    %ymm11, %ymm1, %ymm1                          #97.17
        vinsertf128 $1, 84(%rsp,%r14,4), %ymm6, %ymm7           #93.17
        vaddps    %ymm3, %ymm7, %ymm2                           #93.17
        vmovups   8132(%rsp,%r14,4), %xmm7                      #95.17
        vmovups   4100(%rsp,%r14,4), %xmm3                      #94.17
        vinsertf128 $1, 8148(%rsp,%r14,4), %ymm7, %ymm8         #95.17
        vaddps    %ymm0, %ymm8, %ymm9                           #95.17
        vmovups   12672(%rsp), %ymm0                            #98.17
        vsubps    %ymm13, %ymm0, %ymm0                          #98.17
        vmovups   %xmm2, 68(%rsp,%r14,4)                        #93.17
        vextractf128 $1, %ymm2, 84(%rsp,%r14,4)                 #93.17
        vsubps    %ymm14, %ymm15, %ymm2                         #99.36
        vmovups   12704(%rsp), %ymm14                           #99.17
        vinsertf128 $1, 4116(%rsp,%r14,4), %ymm3, %ymm4         #94.17
        vaddps    %ymm5, %ymm4, %ymm6                           #94.17
        vsubps    %ymm2, %ymm14, %ymm14                         #99.17
        vmovups   %xmm6, 4100(%rsp,%r14,4)                      #94.17
        vmovups   %xmm9, 8132(%rsp,%r14,4)                      #95.17
        vextractf128 $1, %ymm6, 4116(%rsp,%r14,4)               #94.17
        vextractf128 $1, %ymm9, 8148(%rsp,%r14,4)               #95.17
        addq      $8, %r14                                      #46.13
        cmpq      %rbx, %r8                                     #46.13
        jb        ..B1.8        # Prob 99%                      #46.13
                                # LOE rax rdx rbx rsi rdi r8 r9 r10 r11 r12 r13 r14 r15 ecx ymm0 ymm1 ymm14
..B1.9:                         # Preds ..B1.8
        vextractf128 $1, %ymm14, %xmm13                         #39.29
        vextractf128 $1, %ymm0, %xmm5                           #38.29
        vextractf128 $1, %ymm1, %xmm10                          #37.29
        vaddps    %xmm13, %xmm14, %xmm12                        #39.29
        vaddps    %xmm5, %xmm0, %xmm6                           #38.29
        vaddps    %xmm10, %xmm1, %xmm11                         #37.29
        vmovhlps  %xmm12, %xmm12, %xmm2                         #39.29
        vmovhlps  %xmm6, %xmm6, %xmm7                           #38.29
        vaddps    %xmm2, %xmm12, %xmm3                          #39.29
        vaddps    %xmm7, %xmm6, %xmm8                           #38.29
        vmovhlps  %xmm11, %xmm11, %xmm15                        #37.29
        vshufps   $245, %xmm3, %xmm3, %xmm4                     #39.29
        vaddps    %xmm15, %xmm11, %xmm0                         #37.29
        vaddss    %xmm4, %xmm3, %xmm13                          #39.29
        vshufps   $245, %xmm8, %xmm8, %xmm9                     #38.29
        vshufps   $245, %xmm0, %xmm0, %xmm1                     #37.29
        vaddss    %xmm9, %xmm8, %xmm12                          #38.29
        vaddss    %xmm1, %xmm0, %xmm9                           #37.29
                                # LOE rax rdx rbx rsi ecx xmm9 xmm12 xmm13
..B1.10:                        # Preds ..B1.9 ..B1.26
        cmpq      %rsi, %rbx                                    #46.13
        jae       ..B1.21       # Prob 9%                       #46.13
                                # LOE rax rdx rbx rsi ecx xmm9 xmm12 xmm13
..B1.11:                        # Preds ..B1.10
        movq      24(%rsp), %rdi                                #79.42
        lea       8128(%rsp,%rdx,4), %r8                        #95.17
        lea       4096(%rsp,%rdx,4), %r9                        #94.17
        movl      %ecx, 16(%rsp)                                #77.91
        lea       64(%rsp,%rdx,4), %r10                         #93.17
        movq      %r8, %rcx                                     #77.91
        lea       24192(%rdi,%rdx,4), %r14                      #60.37
        vmovss    (%r14), %xmm7                                 #60.37
        lea       20160(%rdi,%rdx,4), %r11                      #79.42
        vmulss    .L_2il0floatpacket.4(%rip), %xmm7, %xmm0      #74.51
        lea       16128(%rdi,%rdx,4), %r12                      #78.42
        vmulss    .L_2il0floatpacket.1(%rip), %xmm7, %xmm3      #60.49
        vmovss    (%r11), %xmm11                                #79.91
        lea       12096(%rdi,%rdx,4), %r13                      #77.42
        vmovss    (%r12), %xmm10                                #78.91
        lea       8064(%rdi,%rdx,4), %r15                       #54.22
        vmovss    (%r13), %xmm8                                 #77.91
        lea       4032(%rdi,%rdx,4), %r8                        #53.22
        vmulss    %xmm0, %xmm11, %xmm2                          #79.91
        lea       (%rdi,%rdx,4), %rdi                           #52.22
        vmulss    %xmm10, %xmm0, %xmm1                          #78.91
        vmulss    %xmm8, %xmm0, %xmm0                           #77.91
        vmovss    (%r15), %xmm6                                 #54.22
        vmovss    (%r8), %xmm5                                  #53.22
        vmovss    (%rdi), %xmm4                                 #52.22
        vmovss    %xmm0, 12160(%rsp)                            #77.91
        vmovss    %xmm1, 56(%rsp)                               #77.91
        vmovss    %xmm2, 48(%rsp)                               #77.91
        vmovss    %xmm3, 12176(%rsp)                            #77.91
        vmovss    %xmm4, 12936(%rsp)                            #77.91
        vmovss    %xmm5, 12928(%rsp)                            #77.91
        vmovss    %xmm6, 12184(%rsp)                            #77.91
        vmovss    %xmm7, 12944(%rsp)                            #77.91
        vmovss    %xmm8, 40(%rsp)                               #77.91
        vmovss    %xmm10, 12168(%rsp)                           #77.91
        vmovss    %xmm11, 32(%rsp)                              #77.91
                                # LOE rax rdx rcx rbx rsi rdi r8 r9 r10 r11 r12 r13 r14 r15 xmm9 xmm12 xmm13
..B1.12:                        # Preds ..B1.19 ..B1.11
        vmovss    12936(%rsp), %xmm11                           #52.33
        vmovss    12928(%rsp), %xmm10                           #53.33
        vmovss    12184(%rsp), %xmm15                           #54.33
        vsubss    4(%rdi,%rbx,4), %xmm11, %xmm11                #52.33
        vsubss    4(%r8,%rbx,4), %xmm10, %xmm10                 #53.33
        vsubss    4(%r15,%rbx,4), %xmm15, %xmm15                #54.33
        vcvtss2sd %xmm11, %xmm11, %xmm14                        #56.30
        vcvtss2sd %xmm10, %xmm10, %xmm0                         #56.45
        vcvtss2sd %xmm15, %xmm15, %xmm3                         #56.60
        vmulsd    %xmm14, %xmm14, %xmm1                         #56.26
        vmulsd    %xmm0, %xmm0, %xmm2                           #56.41
        vmulsd    %xmm3, %xmm3, %xmm5                           #56.56
        vaddsd    %xmm2, %xmm1, %xmm4                           #56.41
        vaddsd    %xmm5, %xmm4, %xmm14                          #56.56
        vsqrtsd   %xmm14, %xmm14, %xmm14                        #56.21
        vcvtsd2ss %xmm14, %xmm14, %xmm14                        #56.17
        vcvtss2sd %xmm14, %xmm14, %xmm7                         #58.38
        vcomiss   .L_2il0floatpacket.6(%rip), %xmm14            #62.28
        vmulsd    %xmm7, %xmm7, %xmm6                           #58.34
        vmulsd    %xmm7, %xmm6, %xmm3                           #58.34
        vcvtsd2ss %xmm3, %xmm3, %xmm3                           #58.34
        jbe       ..B1.14       # Prob 50%                      #62.28
                                # LOE rax rdx rcx rbx rsi rdi r8 r9 r10 r11 r12 r13 r14 r15 xmm3 xmm9 xmm10 xmm11 xmm12 xmm13 xmm14 xmm15
..B1.13:                        # Preds ..B1.12
        vmovss    12176(%rsp), %xmm0                            #62.59
        vmulss    %xmm0, %xmm11, %xmm8                          #62.59
        vmulss    %xmm0, %xmm10, %xmm7                          #63.59
        vmulss    %xmm0, %xmm15, %xmm0                          #64.59
        vdivss    %xmm3, %xmm8, %xmm8                           #62.65
        vdivss    %xmm3, %xmm0, %xmm6                           #64.65
        vmovss    4(%r14,%rbx,4), %xmm0                         #66.37
        vmulss    .L_2il0floatpacket.1(%rip), %xmm0, %xmm1      #66.49
        vdivss    %xmm3, %xmm7, %xmm7                           #63.65
        vmulss    %xmm1, %xmm11, %xmm11                         #68.59
        vmulss    %xmm10, %xmm1, %xmm10                         #69.59
        vmulss    %xmm15, %xmm1, %xmm15                         #70.59
        vdivss    %xmm3, %xmm11, %xmm5                          #68.65
        vdivss    %xmm3, %xmm10, %xmm4                          #69.65
        vdivss    %xmm3, %xmm15, %xmm2                          #70.65
        jmp       ..B1.15       # Prob 100%                     #70.65
                                # LOE rax rdx rcx rbx rsi rdi r8 r9 r10 r11 r12 r13 r14 r15 xmm0 xmm2 xmm4 xmm5 xmm6 xmm7 xmm8 xmm9 xmm12 xmm13 xmm14
..B1.14:                        # Preds ..B1.12
        vmovss    4(%r14,%rbx,4), %xmm0                         #66.37
        vxorps    %xmm7, %xmm7, %xmm7                           #63.17
        vxorps    %xmm8, %xmm8, %xmm8                           #62.17
        vmovaps   %xmm7, %xmm6                                  #64.17
        vmovaps   %xmm7, %xmm5                                  #68.17
        vmovaps   %xmm7, %xmm4                                  #69.17
        vmovaps   %xmm7, %xmm2                                  #70.17
                                # LOE rax rdx rcx rbx rsi rdi r8 r9 r10 r11 r12 r13 r14 r15 xmm0 xmm2 xmm4 xmm5 xmm6 xmm7 xmm8 xmm9 xmm12 xmm13 xmm14
..B1.15:                        # Preds ..B1.13 ..B1.14
        vmovss    4(%r11,%rbx,4), %xmm15                        #79.42
        vmovss    %xmm15, 12976(%rsp)                           #79.42
        vxorps    %xmm15, %xmm15, %xmm15                        #85.30
        vmovss    12944(%rsp), %xmm1                            #72.46
        vmulss    .L_2il0floatpacket.4(%rip), %xmm0, %xmm3      #75.51
        vaddss    %xmm0, %xmm1, %xmm10                          #72.46
        vsubss    %xmm1, %xmm0, %xmm1                           #77.91
        vcomiss   %xmm15, %xmm14                                #85.30
        vmovss    4(%r13,%rbx,4), %xmm11                        #77.42
        vmovss    4(%r12,%rbx,4), %xmm0                         #78.42
        jbe       ..B1.18       # Prob 50%                      #85.30
                                # LOE rax rdx rcx rbx rsi rdi r8 r9 r10 r11 r12 r13 r14 r15 xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6 xmm7 xmm8 xmm9 xmm10 xmm11 xmm12 xmm13 xmm14
..B1.16:                        # Preds ..B1.15
        vmovss    .L_2il0floatpacket.6(%rip), %xmm15            #85.42
        vcomiss   %xmm14, %xmm15                                #85.42
        jbe       ..B1.18       # Prob 50%                      #85.42
                                # LOE rax rdx rcx rbx rsi rdi r8 r9 r10 r11 r12 r13 r14 r15 xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6 xmm7 xmm8 xmm9 xmm10 xmm11 xmm12 xmm13
..B1.17:                        # Preds ..B1.16
        vmulss    %xmm1, %xmm11, %xmm15                         #77.91
        vmovss    %xmm2, (%rsp)                                 #
        vmovss    40(%rsp), %xmm2                               #81.91
        vaddss    12160(%rsp), %xmm15, %xmm14                   #77.91
        vdivss    %xmm10, %xmm14, %xmm15                        #77.103
        vsubss    %xmm11, %xmm15, %xmm14                        #85.70
        vmulss    %xmm1, %xmm0, %xmm15                          #78.91
        vmulss    %xmm11, %xmm3, %xmm11                         #81.91
        vaddss    56(%rsp), %xmm15, %xmm15                      #78.91
        vdivss    %xmm10, %xmm15, %xmm15                        #78.103
        vmovss    %xmm14, 12952(%rsp)                           #85.70
        vsubss    %xmm0, %xmm15, %xmm15                         #86.70
        vmulss    %xmm0, %xmm3, %xmm0                           #82.91
        vmovss    12976(%rsp), %xmm14                           #79.91
        vmovss    %xmm15, 12960(%rsp)                           #86.70
        vmulss    %xmm1, %xmm14, %xmm15                         #79.91
        vmulss    %xmm3, %xmm14, %xmm3                          #83.91
        vaddss    48(%rsp), %xmm15, %xmm15                      #79.91
        vdivss    %xmm10, %xmm15, %xmm15                        #79.103
        vsubss    %xmm14, %xmm15, %xmm15                        #87.70
        vmovss    %xmm15, 12968(%rsp)                           #87.70
        vmulss    %xmm1, %xmm2, %xmm15                          #81.91
        vxorps    .L_2il0floatpacket.7(%rip), %xmm15, %xmm15    #81.91
        vaddss    %xmm11, %xmm15, %xmm15                        #81.91
        vdivss    %xmm10, %xmm15, %xmm11                        #81.103
        vsubss    %xmm2, %xmm11, %xmm15                         #89.70
        vmovss    12168(%rsp), %xmm2                            #82.91
        vmulss    %xmm1, %xmm2, %xmm11                          #82.91
        vxorps    .L_2il0floatpacket.7(%rip), %xmm11, %xmm11    #82.91
        vaddss    %xmm0, %xmm11, %xmm0                          #82.91
        vdivss    %xmm10, %xmm0, %xmm0                          #82.103
        vsubss    %xmm2, %xmm0, %xmm11                          #90.70
        vmovss    32(%rsp), %xmm0                               #83.91
        vmulss    %xmm1, %xmm0, %xmm1                           #83.91
        vxorps    .L_2il0floatpacket.7(%rip), %xmm1, %xmm1      #83.91
        vmovss    (%rsp), %xmm2                                 #91.70
        vaddss    %xmm3, %xmm1, %xmm3                           #83.91
        vdivss    %xmm10, %xmm3, %xmm10                         #83.103
        vsubss    %xmm0, %xmm10, %xmm1                          #91.70
        jmp       ..B1.19       # Prob 100%                     #91.70
                                # LOE rax rdx rcx rbx rsi rdi r8 r9 r10 r11 r12 r13 r14 r15 xmm1 xmm2 xmm4 xmm5 xmm6 xmm7 xmm8 xmm9 xmm11 xmm12 xmm13 xmm15
..B1.18:                        # Preds ..B1.15 ..B1.16
        vxorps    %xmm15, %xmm15, %xmm15                        #91.30
        vxorps    %xmm0, %xmm0, %xmm0                           #91.30
        vmovss    %xmm0, 12952(%rsp)                            #91.30
        vmovaps   %xmm15, %xmm11                                #91.30
        vmovss    %xmm15, 12960(%rsp)                           #91.30
        vmovaps   %xmm15, %xmm1                                 #91.30
        vmovss    %xmm15, 12968(%rsp)                           #91.30
                                # LOE rax rdx rcx rbx rsi rdi r8 r9 r10 r11 r12 r13 r14 r15 xmm1 xmm2 xmm4 xmm5 xmm6 xmm7 xmm8 xmm9 xmm11 xmm12 xmm13 xmm15
..B1.19:                        # Preds ..B1.18 ..B1.17
        vxorps    %xmm0, %xmm0, %xmm0                           #93.42
        vaddss    %xmm8, %xmm0, %xmm3                           #93.42
        vaddss    %xmm7, %xmm0, %xmm14                          #94.42
        vsubss    %xmm0, %xmm2, %xmm2                           #99.36
        vaddss    12952(%rsp), %xmm3, %xmm8                     #85.17
        vaddss    12960(%rsp), %xmm14, %xmm3                    #86.17
        vsubss    %xmm1, %xmm2, %xmm1                           #99.36
        vaddss    4(%r10,%rbx,4), %xmm8, %xmm10                 #93.17
        vsubss    %xmm1, %xmm13, %xmm13                         #99.17
        vmovss    %xmm10, 4(%r10,%rbx,4)                        #93.17
        vaddss    4(%r9,%rbx,4), %xmm3, %xmm3                   #94.17
        vmovss    %xmm3, 4(%r9,%rbx,4)                          #94.17
        vaddss    %xmm6, %xmm0, %xmm3                           #95.42
        vaddss    12968(%rsp), %xmm3, %xmm3                     #87.17
        vaddss    4(%rcx,%rbx,4), %xmm3, %xmm3                  #95.17
        vmovss    %xmm3, 4(%rcx,%rbx,4)                         #95.17
        incq      %rbx                                          #46.13
        vsubss    %xmm0, %xmm5, %xmm3                           #97.36
        vsubss    %xmm15, %xmm3, %xmm3                          #97.36
        vsubss    %xmm3, %xmm9, %xmm9                           #97.17
        vsubss    %xmm0, %xmm4, %xmm3                           #98.36
        vsubss    %xmm11, %xmm3, %xmm3                          #98.36
        vsubss    %xmm3, %xmm12, %xmm12                         #98.17
        cmpq      %rsi, %rbx                                    #46.13
        jb        ..B1.12       # Prob 99%                      #46.13
                                # LOE rax rdx rcx rbx rsi rdi r8 r9 r10 r11 r12 r13 r14 r15 xmm9 xmm12 xmm13
..B1.20:                        # Preds ..B1.19
        movl      16(%rsp), %ecx                                #
                                # LOE rax rdx ecx xmm9 xmm12 xmm13
..B1.21:                        # Preds ..B1.5 ..B1.20 ..B1.10
        decq      %rax                                          #14.5
        vaddss    64(%rsp,%rdx,4), %xmm9, %xmm0                 #102.13
        vaddss    4096(%rsp,%rdx,4), %xmm12, %xmm1              #103.13
        vaddss    8128(%rsp,%rdx,4), %xmm13, %xmm2              #104.13
        vmovss    %xmm0, 64(%rsp,%rdx,4)                        #102.13
        vmovss    %xmm1, 4096(%rsp,%rdx,4)                      #103.13
        vmovss    %xmm2, 8128(%rsp,%rdx,4)                      #104.13
        incq      %rdx                                          #14.5
        cmpl      $1000, %ecx                                   #35.9
        jb        ..B1.5        # Prob 99%                      #35.9
                                # LOE rax rdx ecx
..B1.22:                        # Preds ..B1.21
        vmovups   .L_2il0floatpacket.0(%rip), %ymm0             #
        xorl      %eax, %eax                                    #109.14
        vxorps    %ymm1, %ymm1, %ymm1                           #
        movl      8(%rsp), %esi                                 #
        movq      24(%rsp), %rdi                                #
                                # LOE rax rdi esi ymm0 ymm1
..B1.23:                        # Preds ..B1.23 ..B1.22
        vmovups   12096(%rdi,%rax,4), %ymm2                     #110.13
        vmovups   16128(%rdi,%rax,4), %ymm3                     #111.13
        vmovups   20160(%rdi,%rax,4), %ymm4                     #112.13
        vaddps    64(%rsp,%rax,4), %ymm2, %ymm5                 #110.13
        vaddps    4096(%rsp,%rax,4), %ymm3, %ymm8               #111.13
        vaddps    8128(%rsp,%rax,4), %ymm4, %ymm11              #112.13
        vmulps    %ymm5, %ymm0, %ymm6                           #114.36

###             pos_y[i] += vel_y[i] * DT;

        vmulps    %ymm8, %ymm0, %ymm9                           #115.36

###             pos_z[i] += vel_z[i] * DT;

        vmulps    %ymm11, %ymm0, %ymm12                         #116.36
        vmovups   %ymm5, 12096(%rdi,%rax,4)                     #110.13
        vmovups   %ymm8, 16128(%rdi,%rax,4)                     #111.13
        vmovups   %ymm11, 20160(%rdi,%rax,4)                    #112.13
        vaddps    (%rdi,%rax,4), %ymm6, %ymm7                   #114.13
        vaddps    4032(%rdi,%rax,4), %ymm9, %ymm10              #115.13
        vaddps    8064(%rdi,%rax,4), %ymm12, %ymm13             #116.13
        vmovups   %ymm7, (%rdi,%rax,4)                          #114.13
        vmovups   %ymm10, 4032(%rdi,%rax,4)                     #115.13
        vmovups   %ymm13, 8064(%rdi,%rax,4)                     #116.13
        addq      $8, %rax                                      #109.14
        cmpq      $1000, %rax                                   #109.14
        jb        ..B1.23       # Prob 99%                      #109.14
                                # LOE rax rdi esi ymm0 ymm1
..B1.24:                        # Preds ..B1.23
        incl      %esi                                          #26.5
        cmpl      $1000, %esi                                   #26.5
        jb        ..B1.2        # Prob 99%                      #26.5
                                # LOE rdi esi ymm0 ymm1
..B1.25:                        # Preds ..B1.24

###         }
###     }
### }

        vzeroupper                                              #119.1
        addq      $13016, %rsp                                  #119.1
	.cfi_restore 3
        popq      %rbx                                          #119.1
	.cfi_restore 15
        popq      %r15                                          #119.1
	.cfi_restore 14
        popq      %r14                                          #119.1
	.cfi_restore 13
        popq      %r13                                          #119.1
	.cfi_restore 12
        popq      %r12                                          #119.1
        movq      %rbp, %rsp                                    #119.1
        popq      %rbp                                          #119.1
	.cfi_def_cfa 7, 8
	.cfi_restore 6
        ret                                                     #119.1
	.cfi_def_cfa 6, 16
	.cfi_escape 0x10, 0x03, 0x0e, 0x38, 0x1c, 0x0d, 0xc0, 0xff, 0xff, 0xff, 0x1a, 0x0d, 0xd8, 0xff, 0xff, 0xff, 0x22
	.cfi_offset 6, -16
	.cfi_escape 0x10, 0x0c, 0x0e, 0x38, 0x1c, 0x0d, 0xc0, 0xff, 0xff, 0xff, 0x1a, 0x0d, 0xf8, 0xff, 0xff, 0xff, 0x22
	.cfi_escape 0x10, 0x0d, 0x0e, 0x38, 0x1c, 0x0d, 0xc0, 0xff, 0xff, 0xff, 0x1a, 0x0d, 0xf0, 0xff, 0xff, 0xff, 0x22
	.cfi_escape 0x10, 0x0e, 0x0e, 0x38, 0x1c, 0x0d, 0xc0, 0xff, 0xff, 0xff, 0x1a, 0x0d, 0xe8, 0xff, 0xff, 0xff, 0x22
	.cfi_escape 0x10, 0x0f, 0x0e, 0x38, 0x1c, 0x0d, 0xc0, 0xff, 0xff, 0xff, 0x1a, 0x0d, 0xe0, 0xff, 0xff, 0xff, 0x22
                                # LOE
..B1.26:                        # Preds ..B1.6                  # Infreq
        xorl      %ebx, %ebx                                    #46.13
        jmp       ..B1.10       # Prob 100%                     #46.13
        .align    16,0x90
	.cfi_endproc
                                # LOE rax rdx rbx rsi ecx xmm9 xmm12 xmm13
# mark_end;
	.type	_Z18particles_simulateP11t_particles,@function
	.size	_Z18particles_simulateP11t_particles,.-_Z18particles_simulateP11t_particles
	.data
# -- End  _Z18particles_simulateP11t_particles
	.text
# -- Begin  _Z14particles_readP8_IO_FILEP11t_particles
	.text
# mark_begin;
       .align    16,0x90
	.globl _Z14particles_readP8_IO_FILEP11t_particles
# --- particles_read(FILE *, t_particles *)
_Z14particles_readP8_IO_FILEP11t_particles:
# parameter 1: %rdi
# parameter 2: %rsi
..B2.1:                         # Preds ..B2.0

### void particles_read(FILE *fp, t_particles* p) {

	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
..___tag_value__Z14particles_readP8_IO_FILEP11t_particles.26:
..L27:
                                                         #122.47
        pushq     %r12                                          #122.47
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
        pushq     %r13                                          #122.47
	.cfi_def_cfa_offset 24
	.cfi_offset 13, -24
        pushq     %r14                                          #122.47
	.cfi_def_cfa_offset 32
	.cfi_offset 14, -32

###     for (int i = 0; i < N; i++) {

        xorl      %eax, %eax                                    #123.16
        movq      %rax, %r12                                    #123.16
        movq      %rsi, %r13                                    #123.16
        movq      %rdi, %r14                                    #123.16
                                # LOE rbx rbp r12 r13 r14 r15
..B2.2:                         # Preds ..B2.3 ..B2.1

###         fscanf(fp, "%f %f %f %f %f %f %f \n",

        addq      $-32, %rsp                                    #124.9
	.cfi_def_cfa_offset 64

###                &p->pos_x[i], &p->pos_y[i], &p->pos_z[i],
###                &p->vel_x[i], &p->vel_y[i], &p->vel_z[i],
###                &p->weight[i]);

        lea       (%r13,%r12,4), %rdx                           #127.17
        movq      %r14, %rdi                                    #124.9
        lea       4032(%rdx), %rcx                              #124.9
        movl      $.L_2__STRING.0, %esi                         #124.9
        lea       8064(%rdx), %r8                               #124.9
        xorl      %eax, %eax                                    #124.9
        lea       12096(%rdx), %r9                              #124.9
        lea       16128(%rdx), %r10                             #124.9
        movq      %r10, (%rsp)                                  #124.9
        lea       20160(%rdx), %r11                             #124.9
        movq      %r11, 8(%rsp)                                 #124.9
        lea       24192(%rdx), %r10                             #124.9
        movq      %r10, 16(%rsp)                                #124.9
#       fscanf(FILE *, const char *, ...)
        call      fscanf                                        #124.9
                                # LOE rbx rbp r12 r13 r14 r15
..B2.7:                         # Preds ..B2.2
        addq      $32, %rsp                                     #124.9
	.cfi_def_cfa_offset 32
                                # LOE rbx rbp r12 r13 r14 r15
..B2.3:                         # Preds ..B2.7
        incq      %r12                                          #123.28
        cmpq      $1000, %r12                                   #123.25
        jl        ..B2.2        # Prob 99%                      #123.25
                                # LOE rbx rbp r12 r13 r14 r15
..B2.4:                         # Preds ..B2.3

###     }
### }

	.cfi_restore 14
        popq      %r14                                          #129.1
	.cfi_def_cfa_offset 24
	.cfi_restore 13
        popq      %r13                                          #129.1
	.cfi_def_cfa_offset 16
	.cfi_restore 12
        popq      %r12                                          #129.1
	.cfi_def_cfa_offset 8
        ret                                                     #129.1
        .align    16,0x90
	.cfi_endproc
                                # LOE
# mark_end;
	.type	_Z14particles_readP8_IO_FILEP11t_particles,@function
	.size	_Z14particles_readP8_IO_FILEP11t_particles,.-_Z14particles_readP8_IO_FILEP11t_particles
	.data
# -- End  _Z14particles_readP8_IO_FILEP11t_particles
	.text
# -- Begin  _Z15particles_writeP8_IO_FILEP11t_particles
	.text
# mark_begin;
       .align    16,0x90
	.globl _Z15particles_writeP8_IO_FILEP11t_particles
# --- particles_write(FILE *, t_particles *)
_Z15particles_writeP8_IO_FILEP11t_particles:
# parameter 1: %rdi
# parameter 2: %rsi
..B3.1:                         # Preds ..B3.0

### void particles_write(FILE *fp, t_particles* p) {

	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
..___tag_value__Z15particles_writeP8_IO_FILEP11t_particles.43:
..L44:
                                                         #131.48
        pushq     %r12                                          #131.48
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
        pushq     %r13                                          #131.48
	.cfi_def_cfa_offset 24
	.cfi_offset 13, -24
        pushq     %r14                                          #131.48
	.cfi_def_cfa_offset 32
	.cfi_offset 14, -32

###     for (int i = 0; i < N; i++) {

        xorl      %eax, %eax                                    #132.16
        movq      %rax, %r12                                    #132.16
        movq      %rsi, %r14                                    #132.16
        movq      %rdi, %r13                                    #132.16
                                # LOE rbx rbp r12 r13 r14 r15
..B3.2:                         # Preds ..B3.3 ..B3.1

###         fprintf(fp, "%10.10f %10.10f %10.10f %10.10f %10.10f %10.10f %10.10f \n",

        vxorpd    %xmm0, %xmm0, %xmm0                           #133.9
        vxorpd    %xmm1, %xmm1, %xmm1                           #133.9
        vxorpd    %xmm2, %xmm2, %xmm2                           #133.9
        vxorpd    %xmm3, %xmm3, %xmm3                           #133.9
        vxorpd    %xmm4, %xmm4, %xmm4                           #133.9
        vxorpd    %xmm5, %xmm5, %xmm5                           #133.9
        vxorpd    %xmm6, %xmm6, %xmm6                           #133.9
        movq      %r13, %rdi                                    #133.9
        vcvtss2sd (%r14,%r12,4), %xmm0, %xmm0                   #133.9
        vcvtss2sd 4032(%r14,%r12,4), %xmm1, %xmm1               #133.9
        vcvtss2sd 8064(%r14,%r12,4), %xmm2, %xmm2               #133.9
        vcvtss2sd 12096(%r14,%r12,4), %xmm3, %xmm3              #133.9
        vcvtss2sd 16128(%r14,%r12,4), %xmm4, %xmm4              #133.9
        vcvtss2sd 20160(%r14,%r12,4), %xmm5, %xmm5              #133.9
        vcvtss2sd 24192(%r14,%r12,4), %xmm6, %xmm6              #133.9
        movl      $.L_2__STRING.1, %esi                         #133.9
        movl      $7, %eax                                      #133.9
#       fprintf(FILE *, const char *, ...)
        call      fprintf                                       #133.9
                                # LOE rbx rbp r12 r13 r14 r15
..B3.3:                         # Preds ..B3.2
        incq      %r12                                          #132.28
        cmpq      $1000, %r12                                   #132.25
        jl        ..B3.2        # Prob 99%                      #132.25
                                # LOE rbx rbp r12 r13 r14 r15
..B3.4:                         # Preds ..B3.3

###                 p->pos_x[i], p->pos_y[i], p->pos_z[i],
###                 p->vel_x[i], p->vel_y[i], p->vel_z[i],
###                 p->weight[i]);
###     }
### }

	.cfi_restore 14
        popq      %r14                                          #138.1
	.cfi_def_cfa_offset 24
	.cfi_restore 13
        popq      %r13                                          #138.1
	.cfi_def_cfa_offset 16
	.cfi_restore 12
        popq      %r12                                          #138.1
	.cfi_def_cfa_offset 8
        ret                                                     #138.1
        .align    16,0x90
	.cfi_endproc
                                # LOE
# mark_end;
	.type	_Z15particles_writeP8_IO_FILEP11t_particles,@function
	.size	_Z15particles_writeP8_IO_FILEP11t_particles,.-_Z15particles_writeP8_IO_FILEP11t_particles
	.data
# -- End  _Z15particles_writeP8_IO_FILEP11t_particles
	.text
# -- Begin  __sti__$E
	.text
# mark_begin;
       .align    16,0x90
# --- __sti__$E()
__sti__$E:
..B4.1:                         # Preds ..B4.0
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
..___tag_value___sti__$E.58:
..L59:
                                                         #
        pushq     %rsi                                          #
	.cfi_def_cfa_offset 16

###   static ios_base::Init __ioinit;

        movl      $_ZN30_INTERNAL_9_nbody_cpp_ce2e8df9St8__ioinitE, %edi #74.25
..___tag_value___sti__$E.61:
#       std::ios_base::Init::Init(std::ios_base::Init *)
        call      _ZNSt8ios_base4InitC1Ev                       #74.25
..___tag_value___sti__$E.62:
                                # LOE rbx rbp r12 r13 r14 r15
..B4.2:                         # Preds ..B4.1
        movl      $_ZNSt8ios_base4InitD1Ev, %edi                #74.25
        movl      $_ZN30_INTERNAL_9_nbody_cpp_ce2e8df9St8__ioinitE, %esi #74.25
        movl      $__dso_handle, %edx                           #74.25
        addq      $8, %rsp                                      #74.25
	.cfi_def_cfa_offset 8
#       __cxa_atexit()
        jmp       __cxa_atexit                                  #74.25
        .align    16,0x90
	.cfi_endproc
                                # LOE
# mark_end;
	.type	__sti__$E,@function
	.size	__sti__$E,.-__sti__$E
	.data
# -- End  __sti__$E
	.bss
	.align 4
	.align 1
_ZN30_INTERNAL_9_nbody_cpp_ce2e8df9St8__ioinitE:
	.type	_ZN30_INTERNAL_9_nbody_cpp_ce2e8df9St8__ioinitE,@object
	.size	_ZN30_INTERNAL_9_nbody_cpp_ce2e8df9St8__ioinitE,1
	.space 1	# pad
	.section .rodata, "a"
	.align 32
	.align 32
.L_2il0floatpacket.0:
	.long	0x3a83126f,0x3a83126f,0x3a83126f,0x3a83126f,0x3a83126f,0x3a83126f,0x3a83126f,0x3a83126f
	.type	.L_2il0floatpacket.0,@object
	.size	.L_2il0floatpacket.0,32
	.align 32
.L_2il0floatpacket.2:
	.long	0x3c23d70a,0x3c23d70a,0x3c23d70a,0x3c23d70a,0x3c23d70a,0x3c23d70a,0x3c23d70a,0x3c23d70a
	.type	.L_2il0floatpacket.2,@object
	.size	.L_2il0floatpacket.2,32
	.align 32
.L_2il0floatpacket.3:
	.long	0x29964812,0x29964812,0x29964812,0x29964812,0x29964812,0x29964812,0x29964812,0x29964812
	.type	.L_2il0floatpacket.3,@object
	.size	.L_2il0floatpacket.3,32
	.align 32
.L_2il0floatpacket.5:
	.long	0x40000000,0x40000000,0x40000000,0x40000000,0x40000000,0x40000000,0x40000000,0x40000000
	.type	.L_2il0floatpacket.5,@object
	.size	.L_2il0floatpacket.5,32
	.align 16
.L_2il0floatpacket.7:
	.long	0x80000000,0x00000000,0x00000000,0x00000000
	.type	.L_2il0floatpacket.7,@object
	.size	.L_2il0floatpacket.7,16
	.align 4
.L_2il0floatpacket.1:
	.long	0x29964812
	.type	.L_2il0floatpacket.1,@object
	.size	.L_2il0floatpacket.1,4
	.align 4
.L_2il0floatpacket.4:
	.long	0x40000000
	.type	.L_2il0floatpacket.4,@object
	.size	.L_2il0floatpacket.4,4
	.align 4
.L_2il0floatpacket.6:
	.long	0x3c23d70a
	.type	.L_2il0floatpacket.6,@object
	.size	.L_2il0floatpacket.6,4
	.section .rodata.str1.4, "aMS",@progbits,1
	.align 4
	.align 4
.L_2__STRING.0:
	.long	622880293
	.long	1713709158
	.long	543565088
	.long	622880293
	.long	1713709158
	.word	2592
	.byte	0
	.type	.L_2__STRING.0,@object
	.size	.L_2__STRING.0,23
	.space 1, 0x00 	# pad
	.align 4
.L_2__STRING.1:
	.long	774910245
	.long	543567921
	.long	774910245
	.long	543567921
	.long	774910245
	.long	543567921
	.long	774910245
	.long	543567921
	.long	774910245
	.long	543567921
	.long	774910245
	.long	543567921
	.long	774910245
	.long	543567921
	.word	10
	.type	.L_2__STRING.1,@object
	.size	.L_2__STRING.1,58
	.section .ctors, "wa"
	.align 8
__init_0:
	.type	__init_0,@object
	.size	__init_0,8
	.quad	__sti__$E
	.data
	.hidden __dso_handle
# mark_proc_addr_taken __sti__$E;
	.section .note.GNU-stack, ""
// -- Begin DWARF2 SEGMENT .eh_frame
	.section .eh_frame,"a",@progbits
.eh_frame_seg:
	.align 8
# End

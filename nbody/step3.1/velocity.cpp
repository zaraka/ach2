/*
 * Architektura procesoru (ACH 2016)
 * Projekt c. 1 (nbody)
 * Login: xvanku00
 */

#include <cmath>
#include <cfloat>
#include "velocity.h"

/**
 * @breef   Funkce vypocte rychlost kterou teleso p1 ziska vlivem gravitace p2.
 * @details Viz zadani.pdf
 */
void calculate_gravitation_velocity(
        const t_particle &p1,
        const t_particle &p2,
        t_velocity &vel
) {
    float r, dx, dy, dz;
    float vx, vy, vz;
    float vxc, vyc, vzc;

    dx = p2.pos_x - p1.pos_x;
    dy = p2.pos_y - p1.pos_y;
    dz = p2.pos_z - p1.pos_z;

    r = sqrt(dx * dx + dy * dy + dz * dz);

    float r3 = r * r * r;
    float help = p2.weight * G * DT;

    vx = (r > COLLISION_DISTANCE) ? (help * dx) / r3 : 0.0f;
    vy = (r > COLLISION_DISTANCE) ? (help * dy) / r3 : 0.0f;
    vz = (r > COLLISION_DISTANCE) ? (help * dz) / r3 : 0.0f;

    float m2 = p1.weight + p2.weight;
    float wx = (p1.weight * p1.vel_x - p2.weight * p1.vel_x + 2.0f * p2.weight * p2.vel_x) / m2;
    float wy = (p1.weight * p1.vel_y - p2.weight * p1.vel_y + 2.0f * p2.weight * p2.vel_y) / m2;
    float wz = (p1.weight * p1.vel_z - p2.weight * p1.vel_z + 2.0f * p2.weight * p2.vel_z) / m2;

    vxc += (r > 0.0f && r < COLLISION_DISTANCE) ? wx - p1.vel_x : 0.0f;
    vyc += (r > 0.0f && r < COLLISION_DISTANCE) ? wy - p1.vel_y : 0.0f;
    vzc += (r > 0.0f && r < COLLISION_DISTANCE) ? wz - p1.vel_z : 0.0f;

    vel.x += vx + vxc;
    vel.y += vy + vyc;
    vel.z += vz + vzc;
}
# mark_description "Intel(R) C++ Intel(R) 64 Compiler for applications running on Intel(R) 64, Version 16.0.1.150 Build 20151021";
# mark_description "";
# mark_description "-std=c++11 -lpapi -ansi-alias -O2 -Wall -xavx -qopenmp-simd -DN=1000 -DDT=0.001f -DSTEPS=1000 -S -fsource-as";
# mark_description "m -c";
	.file "nbody.cpp"
	.text
..TXTST0:
# -- Begin  _Z18particles_simulateP11t_particles
	.text
# mark_begin;
       .align    16,0x90
	.globl _Z18particles_simulateP11t_particles
# --- particles_simulate(t_particles *)
_Z18particles_simulateP11t_particles:
# parameter 1: %rdi
..B1.1:                         # Preds ..B1.0

### void particles_simulate(t_particles *p) {

	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
..___tag_value__Z18particles_simulateP11t_particles.1:
..L2:
                                                          #12.41
        pushq     %rbp                                          #12.41
	.cfi_def_cfa_offset 16
        movq      %rsp, %rbp                                    #12.41
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
        andq      $-64, %rsp                                    #12.41
        subq      $12288, %rsp                                  #12.41

###     int i, j, k;
### 
###     float *pos_x = p->pos_x;
###     float *pos_y = p->pos_y;
###     float *pos_z = p->pos_z;
###     float *vel_x = p->vel_x;
###     float *vel_y = p->vel_y;
###     float *vel_z = p->vel_z;
###     float *weight = p->weight;
### 
###     t_velocities velocities;
### 
###     for (k = 0; k < STEPS; k++) {

        xorl      %eax, %eax                                    #25.5

###         //vynulovani mezisouctu
###         #pragma omp simd
###         for (i = 0; i < N; i++) {
###             velocities.x[i] = 0.0f;

        vxorps    %ymm1, %ymm1, %ymm1                           #29.31

###             velocities.y[i] = 0.0f;
###             velocities.z[i] = 0.0f;
###         }
###         
###         //vypocet nove rychlosti
###         for (i = 0; i < N; i++) {
###             #pragma omp simd aligned(pos_x, pos_y, pos_z, vel_x, vel_y, vel_z, weight: 64)
###             for (j = 0; j < N; j++) {
###                 float r, dx, dy, dz;
###                 float vx, vy, vz;
###                 float vxc, vyc, vzc;
### 
###                 dx = pos_x[i] - pos_x[j];
###                 dy = pos_y[i] - pos_y[j];
###                 dz = pos_z[i] - pos_z[j];
### 
###                 r = sqrt(dx * dx + dy * dy + dz * dz);
### 
###                 float r3 = r * r * r;
###                 float help = weight[i] * G * DT;

        vmovss    .L_2il0floatpacket.0(%rip), %xmm7             #49.42

### 
###                 vx = (r > COLLISION_DISTANCE) ? (help * dx) / r3 : 0.0f;

        vmovups   .L_2il0floatpacket.1(%rip), %ymm13            #51.27

###                 vy = (r > COLLISION_DISTANCE) ? (help * dy) / r3 : 0.0f;
###                 vz = (r > COLLISION_DISTANCE) ? (help * dz) / r3 : 0.0f;
### 
###                 float m2 = weight[j] + weight[i];
###                 float j_weight_2 = 2.0f * weight[i];

        vmovss    .L_2il0floatpacket.2(%rip), %xmm6             #56.36

###                 float wx = (weight[j] * vel_x[j] - weight[i] * vel_x[j] + j_weight_2 * vel_x[i]) / m2;
###                 float wy = (weight[j] * vel_y[j] - weight[i] * vel_y[j] + j_weight_2 * vel_y[i]) / m2;
###                 float wz = (weight[j] * vel_z[j] - weight[i] * vel_z[j] + j_weight_2 * vel_z[i]) / m2;
### 
###                 vxc += (r > 0.0f && r < COLLISION_DISTANCE) ? wx - vel_x[j] : 0.0f;
###                 vyc += (r > 0.0f && r < COLLISION_DISTANCE) ? wy - vel_y[j] : 0.0f;
###                 vzc += (r > 0.0f && r < COLLISION_DISTANCE) ? wz - vel_z[j] : 0.0f;
### 
###                 velocities.x[j] += vx + vxc;
###                 velocities.y[j] += vy + vyc;
###                 velocities.z[j] += vz + vzc;
###             }
###         }
###         //ulozeni rychlosti a posun castic
###         #pragma omp simd aligned(pos_x, pos_y, pos_z, vel_x, vel_y, vel_z, weight: 64)
###         for (i = 0; i < N; i++) {
###             vel_x[i] += velocities.x[i];
###             vel_y[i] += velocities.y[i];
###             vel_z[i] += velocities.z[i];
### 
###             pos_x[i] += vel_x[i] * DT;

        vmovups   .L_2il0floatpacket.3(%rip), %ymm0             #77.36
                                # LOE rbx rdi r12 r13 r14 r15 eax xmm6 xmm7 ymm0 ymm1 ymm13
..B1.2:                         # Preds ..B1.10 ..B1.1
        xorl      %edx, %edx                                    #28.14
        .align    16,0x90
                                # LOE rdx rbx rdi r12 r13 r14 r15 eax xmm6 xmm7 ymm0 ymm1 ymm13
..B1.3:                         # Preds ..B1.3 ..B1.2
        vmovups   %ymm1, (%rsp,%rdx,4)                          #29.13
        vmovups   %ymm1, 4032(%rsp,%rdx,4)                      #30.13
        vmovups   %ymm1, 8064(%rsp,%rdx,4)                      #31.13
        vmovups   %ymm1, 32(%rsp,%rdx,4)                        #29.13
        vmovups   %ymm1, 4064(%rsp,%rdx,4)                      #30.13
        vmovups   %ymm1, 8096(%rsp,%rdx,4)                      #31.13
        vmovups   %ymm1, 64(%rsp,%rdx,4)                        #29.13
        vmovups   %ymm1, 4096(%rsp,%rdx,4)                      #30.13
        vmovups   %ymm1, 8128(%rsp,%rdx,4)                      #31.13
        vmovups   %ymm1, 96(%rsp,%rdx,4)                        #29.13
        vmovups   %ymm1, 4128(%rsp,%rdx,4)                      #30.13
        vmovups   %ymm1, 8160(%rsp,%rdx,4)                      #31.13
        addq      $32, %rdx                                     #28.14
        cmpq      $992, %rdx                                    #28.14
        jb        ..B1.3        # Prob 99%                      #28.14
                                # LOE rdx rbx rdi r12 r13 r14 r15 eax xmm6 xmm7 ymm0 ymm1 ymm13
..B1.4:                         # Preds ..B1.3
        vmovups   %ymm1, 3968(%rsp)                             #29.13
        xorl      %edx, %edx                                    #35.9
        vmovups   %ymm1, 8000(%rsp)                             #30.13
        vmovups   %ymm1, 12032(%rsp)                            #31.13
                                # LOE rdx rbx rdi r12 r13 r14 r15 eax xmm6 xmm7 ymm13
..B1.5:                         # Preds ..B1.7 ..B1.4
        vmovss    24192(%rdi,%rdx,4), %xmm1                     #49.30
        xorl      %ecx, %ecx                                    #37.18
        vbroadcastss 12096(%rdi,%rdx,4), %xmm10                 #57.88
        vshufps   $0, %xmm1, %xmm1, %xmm8                       #55.40
        vmulss    %xmm1, %xmm7, %xmm14                          #49.46
        vmulss    %xmm6, %xmm1, %xmm1                           #56.43
        vbroadcastss 16128(%rdi,%rdx,4), %xmm12                 #58.88
        vbroadcastss 20160(%rdi,%rdx,4), %xmm9                  #59.88
        vbroadcastss (%rdi,%rdx,4), %xmm5                       #42.22
        vbroadcastss 8064(%rdi,%rdx,4), %xmm4                   #44.22
        vbroadcastss 4032(%rdi,%rdx,4), %xmm11                  #43.22
        vshufps   $0, %xmm14, %xmm14, %xmm15                    #49.46
        vinsertf128 $1, %xmm10, %ymm10, %ymm3                   #57.88
        vinsertf128 $1, %xmm8, %ymm8, %ymm10                    #55.40
        vshufps   $0, %xmm1, %xmm1, %xmm8                       #56.43
        vinsertf128 $1, %xmm12, %ymm12, %ymm2                   #58.88
        vinsertf128 $1, %xmm9, %ymm9, %ymm0                     #59.88
        vinsertf128 $1, %xmm8, %ymm8, %ymm9                     #56.43
        vmulps    %ymm9, %ymm3, %ymm3                           #57.88
        vmulps    %ymm2, %ymm9, %ymm1                           #58.88
        vmulps    %ymm0, %ymm9, %ymm0                           #59.88
        vmovups   %ymm3, 12160(%rsp)                            #59.88
        vmovups   %ymm1, 12128(%rsp)                            #59.88
        vmovups   %ymm0, 12096(%rsp)                            #59.88
        vinsertf128 $1, %xmm5, %ymm5, %ymm5                     #42.22
        vinsertf128 $1, %xmm4, %ymm4, %ymm4                     #44.22
        vmovups   %ymm4, 12192(%rsp)                            #59.88
        vmovups   %ymm5, 12224(%rsp)                            #59.88
        vinsertf128 $1, %xmm11, %ymm11, %ymm11                  #43.22
        vinsertf128 $1, %xmm15, %ymm15, %ymm12                  #49.46
                                # LOE rdx rcx rbx rdi r12 r13 r14 r15 eax ymm10 ymm11 ymm12 ymm13
..B1.6:                         # Preds ..B1.6 ..B1.5
        vmovups   12224(%rsp), %ymm5                            #42.33
        vmovups   12192(%rsp), %ymm2                            #44.33
        vsubps    4032(%rdi,%rcx,4), %ymm11, %ymm3              #43.33
        vsubps    (%rdi,%rcx,4), %ymm5, %ymm7                   #42.33
        vsubps    8064(%rdi,%rcx,4), %ymm2, %ymm8               #44.33
        vmulps    %ymm3, %ymm3, %ymm1                           #46.41
        vmulps    %ymm7, %ymm7, %ymm0                           #46.31
        vmulps    %ymm8, %ymm8, %ymm5                           #46.51
        vmulps    %ymm12, %ymm3, %ymm3                          #52.57
        vmulps    %ymm12, %ymm8, %ymm8                          #53.57
        vaddps    %ymm1, %ymm0, %ymm9                           #46.41
        vaddps    %ymm5, %ymm9, %ymm6                           #46.51
        vrsqrtps  %ymm6, %ymm4                                  #46.21
        vandps    .L_2il0floatpacket.7(%rip), %ymm6, %ymm14     #46.21
        vcmpgeps  .L_2il0floatpacket.6(%rip), %ymm14, %ymm15    #46.21
        vandps    %ymm4, %ymm15, %ymm2                          #46.21
        vmulps    %ymm2, %ymm6, %ymm1                           #46.21
        vmulps    %ymm12, %ymm7, %ymm4                          #51.57
        vmulps    %ymm2, %ymm1, %ymm0                           #46.21
        vmovups   12096(%rdi,%rcx,4), %ymm2                     #57.41
        vsubps    .L_2il0floatpacket.4(%rip), %ymm0, %ymm9      #46.21
        vmulps    %ymm9, %ymm1, %ymm5                           #46.21
        vmulps    .L_2il0floatpacket.5(%rip), %ymm5, %ymm1      #46.21
        vmulps    %ymm1, %ymm1, %ymm14                          #48.32
        vcmpgtps  %ymm13, %ymm1, %ymm9                          #51.27
        vmulps    %ymm14, %ymm1, %ymm15                         #48.36
        vrcpps    %ymm15, %ymm5                                 #51.63
        vmulps    %ymm15, %ymm5, %ymm7                          #51.63
        vmulps    %ymm5, %ymm7, %ymm14                          #51.63
        vaddps    %ymm5, %ymm5, %ymm7                           #51.63
        vsubps    %ymm14, %ymm7, %ymm6                          #51.63
        vmulps    %ymm6, %ymm4, %ymm15                          #51.63
        vmulps    %ymm6, %ymm3, %ymm7                           #52.63
        vmulps    %ymm6, %ymm8, %ymm14                          #53.63
        vmovups   24192(%rdi,%rcx,4), %ymm6                     #55.28
        vmulps    %ymm2, %ymm10, %ymm4                          #57.64
        vaddps    %ymm6, %ymm10, %ymm8                          #55.40
        vandps    %ymm9, %ymm15, %ymm5                          #51.63
        vandps    %ymm9, %ymm7, %ymm7                           #52.63
        vandps    %ymm9, %ymm14, %ymm9                          #53.63
        vrcpps    %ymm8, %ymm14                                 #57.100
        vmulps    %ymm2, %ymm6, %ymm15                          #57.41
        vmulps    %ymm8, %ymm14, %ymm0                          #57.100
        vsubps    %ymm4, %ymm15, %ymm3                          #57.64
        vaddps    %ymm14, %ymm14, %ymm15                        #57.100
        vaddps    12160(%rsp), %ymm3, %ymm4                     #57.88
        vmulps    %ymm14, %ymm0, %ymm3                          #57.100
        vsubps    %ymm3, %ymm15, %ymm15                         #57.100
        vmovups   16128(%rdi,%rcx,4), %ymm3                     #58.41
        vmulps    %ymm15, %ymm4, %ymm0                          #57.100
        vmulps    %ymm3, %ymm6, %ymm4                           #58.41
        vmulps    %ymm3, %ymm10, %ymm8                          #58.64
        vsubps    %ymm2, %ymm0, %ymm2                           #61.68
        vsubps    %ymm8, %ymm4, %ymm14                          #58.64
        vaddps    12128(%rsp), %ymm14, %ymm4                    #58.88
        vmulps    %ymm15, %ymm4, %ymm8                          #58.100
        vmovups   20160(%rdi,%rcx,4), %ymm4                     #59.41
        vsubps    %ymm3, %ymm8, %ymm0                           #62.68
        vmulps    %ymm4, %ymm6, %ymm6                           #59.41
        vmulps    %ymm4, %ymm10, %ymm14                         #59.64
        vsubps    %ymm14, %ymm6, %ymm6                          #59.64
        vaddps    12096(%rsp), %ymm6, %ymm14                    #59.88
        vmulps    %ymm15, %ymm14, %ymm6                         #59.100
        vxorps    %ymm15, %ymm15, %ymm15                        #61.29
        vcmpgtps  %ymm15, %ymm1, %ymm14                         #61.29
        vcmpltps  %ymm13, %ymm1, %ymm1                          #61.41
        vsubps    %ymm4, %ymm6, %ymm4                           #63.68
        vandps    %ymm1, %ymm14, %ymm15                         #61.41
        vandps    %ymm15, %ymm2, %ymm14                         #61.68
        vandps    %ymm15, %ymm0, %ymm1                          #62.68
        vandps    %ymm15, %ymm4, %ymm2                          #63.68
        vaddps    %ymm5, %ymm14, %ymm5                          #61.17
        vaddps    %ymm7, %ymm1, %ymm7                           #62.17
        vaddps    %ymm9, %ymm2, %ymm9                           #63.17
        vaddps    (%rsp,%rcx,4), %ymm5, %ymm5                   #65.17
        vaddps    4032(%rsp,%rcx,4), %ymm7, %ymm1               #66.17
        vaddps    8064(%rsp,%rcx,4), %ymm9, %ymm0               #67.17
        vmovups   %ymm5, (%rsp,%rcx,4)                          #65.17
        vmovups   %ymm1, 4032(%rsp,%rcx,4)                      #66.17
        vmovups   %ymm0, 8064(%rsp,%rcx,4)                      #67.17
        addq      $8, %rcx                                      #37.18
        cmpq      $1000, %rcx                                   #37.18
        jb        ..B1.6        # Prob 99%                      #37.18
                                # LOE rdx rcx rbx rdi r12 r13 r14 r15 eax ymm10 ymm11 ymm12 ymm13
..B1.7:                         # Preds ..B1.6
        incq      %rdx                                          #35.9
        vmovss    .L_2il0floatpacket.2(%rip), %xmm6             #
        vmovss    .L_2il0floatpacket.0(%rip), %xmm7             #
        cmpq      $1000, %rdx                                   #35.9
        jb        ..B1.5        # Prob 99%                      #35.9
                                # LOE rdx rbx rdi r12 r13 r14 r15 eax xmm6 xmm7 ymm13
..B1.8:                         # Preds ..B1.7
        vmovups   .L_2il0floatpacket.3(%rip), %ymm0             #
        xorl      %edx, %edx                                    #72.14
        vxorps    %ymm1, %ymm1, %ymm1                           #
                                # LOE rdx rbx rdi r12 r13 r14 r15 eax xmm6 xmm7 ymm0 ymm1 ymm13
..B1.9:                         # Preds ..B1.9 ..B1.8
        vmovups   12096(%rdi,%rdx,4), %ymm2                     #73.13
        vmovups   16128(%rdi,%rdx,4), %ymm3                     #74.13
        vmovups   20160(%rdi,%rdx,4), %ymm4                     #75.13
        vaddps    (%rsp,%rdx,4), %ymm2, %ymm5                   #73.13
        vaddps    4032(%rsp,%rdx,4), %ymm3, %ymm10              #74.13
        vaddps    8064(%rsp,%rdx,4), %ymm4, %ymm14              #75.13
        vmulps    %ymm5, %ymm0, %ymm8                           #77.36

###             pos_y[i] += vel_y[i] * DT;

        vmulps    %ymm10, %ymm0, %ymm11                         #78.36

###             pos_z[i] += vel_z[i] * DT;

        vmulps    %ymm14, %ymm0, %ymm15                         #79.36
        vmovups   %ymm5, 12096(%rdi,%rdx,4)                     #73.13
        vmovups   %ymm10, 16128(%rdi,%rdx,4)                    #74.13
        vmovups   %ymm14, 20160(%rdi,%rdx,4)                    #75.13
        vaddps    (%rdi,%rdx,4), %ymm8, %ymm9                   #77.13
        vaddps    4032(%rdi,%rdx,4), %ymm11, %ymm12             #78.13
        vaddps    8064(%rdi,%rdx,4), %ymm15, %ymm2              #79.13
        vmovups   %ymm9, (%rdi,%rdx,4)                          #77.13
        vmovups   %ymm12, 4032(%rdi,%rdx,4)                     #78.13
        vmovups   %ymm2, 8064(%rdi,%rdx,4)                      #79.13
        addq      $8, %rdx                                      #72.14
        cmpq      $1000, %rdx                                   #72.14
        jb        ..B1.9        # Prob 99%                      #72.14
                                # LOE rdx rbx rdi r12 r13 r14 r15 eax xmm6 xmm7 ymm0 ymm1 ymm13
..B1.10:                        # Preds ..B1.9
        incl      %eax                                          #25.5
        cmpl      $1000, %eax                                   #25.5
        jb        ..B1.2        # Prob 99%                      #25.5
                                # LOE rbx rdi r12 r13 r14 r15 eax xmm6 xmm7 ymm0 ymm1 ymm13
..B1.11:                        # Preds ..B1.10

###         }
###     }
### }

        vzeroupper                                              #82.1
        movq      %rbp, %rsp                                    #82.1
        popq      %rbp                                          #82.1
	.cfi_def_cfa 7, 8
	.cfi_restore 6
        ret                                                     #82.1
        .align    16,0x90
	.cfi_endproc
                                # LOE
# mark_end;
	.type	_Z18particles_simulateP11t_particles,@function
	.size	_Z18particles_simulateP11t_particles,.-_Z18particles_simulateP11t_particles
	.data
# -- End  _Z18particles_simulateP11t_particles
	.text
# -- Begin  _Z14particles_readP8_IO_FILEP11t_particles
	.text
# mark_begin;
       .align    16,0x90
	.globl _Z14particles_readP8_IO_FILEP11t_particles
# --- particles_read(FILE *, t_particles *)
_Z14particles_readP8_IO_FILEP11t_particles:
# parameter 1: %rdi
# parameter 2: %rsi
..B2.1:                         # Preds ..B2.0

### void particles_read(FILE *fp, t_particles* p) {

	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
..___tag_value__Z14particles_readP8_IO_FILEP11t_particles.9:
..L10:
                                                         #85.47
        pushq     %r12                                          #85.47
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
        pushq     %r13                                          #85.47
	.cfi_def_cfa_offset 24
	.cfi_offset 13, -24
        pushq     %r14                                          #85.47
	.cfi_def_cfa_offset 32
	.cfi_offset 14, -32

###     for (int i = 0; i < N; i++) {

        xorl      %eax, %eax                                    #86.16
        movq      %rax, %r12                                    #86.16
        movq      %rsi, %r13                                    #86.16
        movq      %rdi, %r14                                    #86.16
                                # LOE rbx rbp r12 r13 r14 r15
..B2.2:                         # Preds ..B2.3 ..B2.1

###         fscanf(fp, "%f %f %f %f %f %f %f \n",

        addq      $-32, %rsp                                    #87.9
	.cfi_def_cfa_offset 64

###                &p->pos_x[i], &p->pos_y[i], &p->pos_z[i],
###                &p->vel_x[i], &p->vel_y[i], &p->vel_z[i],
###                &p->weight[i]);

        lea       (%r13,%r12,4), %rdx                           #90.17
        movq      %r14, %rdi                                    #87.9
        lea       4032(%rdx), %rcx                              #87.9
        movl      $.L_2__STRING.0, %esi                         #87.9
        lea       8064(%rdx), %r8                               #87.9
        xorl      %eax, %eax                                    #87.9
        lea       12096(%rdx), %r9                              #87.9
        lea       16128(%rdx), %r10                             #87.9
        movq      %r10, (%rsp)                                  #87.9
        lea       20160(%rdx), %r11                             #87.9
        movq      %r11, 8(%rsp)                                 #87.9
        lea       24192(%rdx), %r10                             #87.9
        movq      %r10, 16(%rsp)                                #87.9
#       fscanf(FILE *, const char *, ...)
        call      fscanf                                        #87.9
                                # LOE rbx rbp r12 r13 r14 r15
..B2.7:                         # Preds ..B2.2
        addq      $32, %rsp                                     #87.9
	.cfi_def_cfa_offset 32
                                # LOE rbx rbp r12 r13 r14 r15
..B2.3:                         # Preds ..B2.7
        incq      %r12                                          #86.28
        cmpq      $1000, %r12                                   #86.25
        jl        ..B2.2        # Prob 99%                      #86.25
                                # LOE rbx rbp r12 r13 r14 r15
..B2.4:                         # Preds ..B2.3

###     }
### }

	.cfi_restore 14
        popq      %r14                                          #92.1
	.cfi_def_cfa_offset 24
	.cfi_restore 13
        popq      %r13                                          #92.1
	.cfi_def_cfa_offset 16
	.cfi_restore 12
        popq      %r12                                          #92.1
	.cfi_def_cfa_offset 8
        ret                                                     #92.1
        .align    16,0x90
	.cfi_endproc
                                # LOE
# mark_end;
	.type	_Z14particles_readP8_IO_FILEP11t_particles,@function
	.size	_Z14particles_readP8_IO_FILEP11t_particles,.-_Z14particles_readP8_IO_FILEP11t_particles
	.data
# -- End  _Z14particles_readP8_IO_FILEP11t_particles
	.text
# -- Begin  _Z15particles_writeP8_IO_FILEP11t_particles
	.text
# mark_begin;
       .align    16,0x90
	.globl _Z15particles_writeP8_IO_FILEP11t_particles
# --- particles_write(FILE *, t_particles *)
_Z15particles_writeP8_IO_FILEP11t_particles:
# parameter 1: %rdi
# parameter 2: %rsi
..B3.1:                         # Preds ..B3.0

### void particles_write(FILE *fp, t_particles* p) {

	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
..___tag_value__Z15particles_writeP8_IO_FILEP11t_particles.26:
..L27:
                                                         #94.48
        pushq     %r12                                          #94.48
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
        pushq     %r13                                          #94.48
	.cfi_def_cfa_offset 24
	.cfi_offset 13, -24
        pushq     %r14                                          #94.48
	.cfi_def_cfa_offset 32
	.cfi_offset 14, -32

###     for (int i = 0; i < N; i++) {

        xorl      %eax, %eax                                    #95.16
        movq      %rax, %r12                                    #95.16
        movq      %rsi, %r14                                    #95.16
        movq      %rdi, %r13                                    #95.16
                                # LOE rbx rbp r12 r13 r14 r15
..B3.2:                         # Preds ..B3.3 ..B3.1

###         fprintf(fp, "%10.10f %10.10f %10.10f %10.10f %10.10f %10.10f %10.10f \n",

        vxorpd    %xmm0, %xmm0, %xmm0                           #96.9
        vxorpd    %xmm1, %xmm1, %xmm1                           #96.9
        vxorpd    %xmm2, %xmm2, %xmm2                           #96.9
        vxorpd    %xmm3, %xmm3, %xmm3                           #96.9
        vxorpd    %xmm4, %xmm4, %xmm4                           #96.9
        vxorpd    %xmm5, %xmm5, %xmm5                           #96.9
        vxorpd    %xmm6, %xmm6, %xmm6                           #96.9
        movq      %r13, %rdi                                    #96.9
        vcvtss2sd (%r14,%r12,4), %xmm0, %xmm0                   #96.9
        vcvtss2sd 4032(%r14,%r12,4), %xmm1, %xmm1               #96.9
        vcvtss2sd 8064(%r14,%r12,4), %xmm2, %xmm2               #96.9
        vcvtss2sd 12096(%r14,%r12,4), %xmm3, %xmm3              #96.9
        vcvtss2sd 16128(%r14,%r12,4), %xmm4, %xmm4              #96.9
        vcvtss2sd 20160(%r14,%r12,4), %xmm5, %xmm5              #96.9
        vcvtss2sd 24192(%r14,%r12,4), %xmm6, %xmm6              #96.9
        movl      $.L_2__STRING.1, %esi                         #96.9
        movl      $7, %eax                                      #96.9
#       fprintf(FILE *, const char *, ...)
        call      fprintf                                       #96.9
                                # LOE rbx rbp r12 r13 r14 r15
..B3.3:                         # Preds ..B3.2
        incq      %r12                                          #95.28
        cmpq      $1000, %r12                                   #95.25
        jl        ..B3.2        # Prob 99%                      #95.25
                                # LOE rbx rbp r12 r13 r14 r15
..B3.4:                         # Preds ..B3.3

###                 p->pos_x[i], p->pos_y[i], p->pos_z[i],
###                 p->vel_x[i], p->vel_y[i], p->vel_z[i],
###                 p->weight[i]);
###     }
### }

	.cfi_restore 14
        popq      %r14                                          #101.1
	.cfi_def_cfa_offset 24
	.cfi_restore 13
        popq      %r13                                          #101.1
	.cfi_def_cfa_offset 16
	.cfi_restore 12
        popq      %r12                                          #101.1
	.cfi_def_cfa_offset 8
        ret                                                     #101.1
        .align    16,0x90
	.cfi_endproc
                                # LOE
# mark_end;
	.type	_Z15particles_writeP8_IO_FILEP11t_particles,@function
	.size	_Z15particles_writeP8_IO_FILEP11t_particles,.-_Z15particles_writeP8_IO_FILEP11t_particles
	.data
# -- End  _Z15particles_writeP8_IO_FILEP11t_particles
	.section .rodata, "a"
	.align 32
	.align 32
.L_2il0floatpacket.1:
	.long	0x3c23d70a,0x3c23d70a,0x3c23d70a,0x3c23d70a,0x3c23d70a,0x3c23d70a,0x3c23d70a,0x3c23d70a
	.type	.L_2il0floatpacket.1,@object
	.size	.L_2il0floatpacket.1,32
	.align 32
.L_2il0floatpacket.3:
	.long	0x3a83126f,0x3a83126f,0x3a83126f,0x3a83126f,0x3a83126f,0x3a83126f,0x3a83126f,0x3a83126f
	.type	.L_2il0floatpacket.3,@object
	.size	.L_2il0floatpacket.3,32
	.align 32
.L_2il0floatpacket.4:
	.long	0x40400000,0x40400000,0x40400000,0x40400000,0x40400000,0x40400000,0x40400000,0x40400000
	.type	.L_2il0floatpacket.4,@object
	.size	.L_2il0floatpacket.4,32
	.align 32
.L_2il0floatpacket.5:
	.long	0xbf000000,0xbf000000,0xbf000000,0xbf000000,0xbf000000,0xbf000000,0xbf000000,0xbf000000
	.type	.L_2il0floatpacket.5,@object
	.size	.L_2il0floatpacket.5,32
	.align 32
.L_2il0floatpacket.6:
	.long	0x00800000,0x00800000,0x00800000,0x00800000,0x00800000,0x00800000,0x00800000,0x00800000
	.type	.L_2il0floatpacket.6,@object
	.size	.L_2il0floatpacket.6,32
	.align 32
.L_2il0floatpacket.7:
	.long	0x7fffffff,0x7fffffff,0x7fffffff,0x7fffffff,0x7fffffff,0x7fffffff,0x7fffffff,0x7fffffff
	.type	.L_2il0floatpacket.7,@object
	.size	.L_2il0floatpacket.7,32
	.align 4
.L_2il0floatpacket.0:
	.long	0x29964812
	.type	.L_2il0floatpacket.0,@object
	.size	.L_2il0floatpacket.0,4
	.align 4
.L_2il0floatpacket.2:
	.long	0x40000000
	.type	.L_2il0floatpacket.2,@object
	.size	.L_2il0floatpacket.2,4
	.section .rodata.str1.4, "aMS",@progbits,1
	.align 4
	.align 4
.L_2__STRING.0:
	.long	622880293
	.long	1713709158
	.long	543565088
	.long	622880293
	.long	1713709158
	.word	2592
	.byte	0
	.type	.L_2__STRING.0,@object
	.size	.L_2__STRING.0,23
	.space 1, 0x00 	# pad
	.align 4
.L_2__STRING.1:
	.long	774910245
	.long	543567921
	.long	774910245
	.long	543567921
	.long	774910245
	.long	543567921
	.long	774910245
	.long	543567921
	.long	774910245
	.long	543567921
	.long	774910245
	.long	543567921
	.long	774910245
	.long	543567921
	.word	10
	.type	.L_2__STRING.1,@object
	.size	.L_2__STRING.1,58
	.data
	.section .note.GNU-stack, ""
// -- Begin DWARF2 SEGMENT .eh_frame
	.section .eh_frame,"a",@progbits
.eh_frame_seg:
	.align 8
# End

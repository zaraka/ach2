/*
 * Architektura procesoru (ACH 2016)
 * Projekt c. 1 (nbody)
 * Login: xvanku00
 */

#include <cmath>
#include <cfloat>
#include "velocity.h"

/**
 * @breef   Funkce vypocte rychlost kterou teleso p1 ziska vlivem gravitace p2.
 * @details Viz zadani.pdf
 */
/*
#pragma omp declare simd uniform(vel_x, vel_y, vel_z)
void calculate_gravitation_velocity(
        const float p1_pos_x,
        const float p1_pos_y,
        const float p1_pos_z,
        const float p1_vel_x,
        const float p1_vel_y,
        const float p1_vel_z,
        const float p1_weight,

        const float p2_pos_x,
        const float p2_pos_y,
        const float p2_pos_z,
        const float p2_vel_x,
        const float p2_vel_y,
        const float p2_vel_z,
        const float p2_weight,

        float * vel_x,
        float * vel_y,
        float * vel_z
) {

    float r, dx, dy, dz;
    float vx, vy, vz;
    float vxc, vyc, vzc;

    dx = p2_pos_x - p1_pos_x;
    dy = p2_pos_y - p1_pos_y;
    dz = p2_pos_z - p1_pos_z;

    r = sqrt(dx * dx + dy * dy + dz * dz);

    float r3 = r * r * r;
    float help = p2_weight * G * DT;

    vx = (help * dx) / r3;
    vy = (help * dy) / r3;
    vz = (help * dz) / r3;

    float m2 = p1_weight * p2_weight;
    float j_weight_2 = 2.0f * p2_weight;
    float wx = (p1_weight * p1_vel_x - p2_weight * p1_vel_x + j_weight_2 * p2_vel_x) / m2;
    float wy = (p1_weight * p1_vel_y - p2_weight * p1_vel_y + j_weight_2 * p2_vel_y) / m2;
    float wz = (p1_weight * p1_vel_z - p2_weight * p1_vel_z + j_weight_2 * p2_vel_z) / m2;

    vxc += wx - p1_vel_x;
    vyc += wy - p1_vel_y;
    vzc += wz - p1_vel_z;

    *vel_x += (r > COLLISION_DISTANCE) ? vx : 0.0f;
    *vel_y += (r > COLLISION_DISTANCE) ? vy : 0.0f;
    *vel_z += (r > COLLISION_DISTANCE) ? vz : 0.0f;


    *vel_x += (r > 0.0f && r < COLLISION_DISTANCE) ? vxc : 0.0f;
    *vel_y += (r > 0.0f && r < COLLISION_DISTANCE) ? vyc : 0.0f;
    *vel_z += (r > 0.0f && r < COLLISION_DISTANCE) ? vzc : 0.0f;
}*/
/*
 * Architektura procesoru (ACH 2016)
 * Projekt c. 1 (nbody)
 * Login: xvanku00
 */

#include "nbody.h"

#include <cmath>
#include <cfloat>

void particles_simulate(t_particles *p) {
    int i, j, k;

    float *pos_x = p->pos_x;
    float *pos_y = p->pos_y;
    float *pos_z = p->pos_z;
    float *vel_x = p->vel_x;
    float *vel_y = p->vel_y;
    float *vel_z = p->vel_z;
    float *weight = p->weight;

    t_velocities velocities;

    for (k = 0; k < STEPS; k++) {
        //vynulovani mezisouctu
        #pragma omp simd
        for (i = 0; i < N; i++) {
            velocities.x[i] = 0.0f;
            velocities.y[i] = 0.0f;
            velocities.z[i] = 0.0f;
        }

        //vypocet nove rychlosti
        for (i = 0; i < N; i++) {
            #pragma omp simd aligned(pos_x, pos_y, pos_z, vel_x, vel_y, vel_z, weight: 64)
            for (j = 0; j < N; j++) {
                float r, dx, dy, dz;
                float vx, vy, vz;
                float vxc, vyc, vzc;

                dx = pos_x[i] - pos_x[j];
                dy = pos_y[i] - pos_y[j];
                dz = pos_z[i] - pos_z[j];

                r = sqrt(dx * dx + dy * dy + dz * dz);

                float r3 = r * r * r;
                float help = weight[i] * G * DT;

                vx = (r > COLLISION_DISTANCE) ? (help * dx) / r3 : 0.0f;
                vy = (r > COLLISION_DISTANCE) ? (help * dy) / r3 : 0.0f;
                vz = (r > COLLISION_DISTANCE) ? (help * dz) / r3 : 0.0f;

                float m2 = weight[j] + weight[i];
                float j_weight_2 = 2.0f * weight[i];
                float wx = (weight[j] * vel_x[j] - weight[i] * vel_x[j] + j_weight_2 * vel_x[i]) / m2;
                float wy = (weight[j] * vel_y[j] - weight[i] * vel_y[j] + j_weight_2 * vel_y[i]) / m2;
                float wz = (weight[j] * vel_z[j] - weight[i] * vel_z[j] + j_weight_2 * vel_z[i]) / m2;

                vxc += (r > 0.0f && r < COLLISION_DISTANCE) ? wx - vel_x[j] : 0.0f;
                vyc += (r > 0.0f && r < COLLISION_DISTANCE) ? wy - vel_y[j] : 0.0f;
                vzc += (r > 0.0f && r < COLLISION_DISTANCE) ? wz - vel_z[j] : 0.0f;

                velocities.x[j] += vx + vxc;
                velocities.y[j] += vy + vyc;
                velocities.z[j] += vz + vzc;
            }
        }
        //ulozeni rychlosti a posun castic
        #pragma omp simd aligned(pos_x, pos_y, pos_z, vel_x, vel_y, vel_z, weight: 64)
        for (i = 0; i < N; i++) {
            vel_x[i] += velocities.x[i];
            vel_y[i] += velocities.y[i];
            vel_z[i] += velocities.z[i];

            pos_x[i] += vel_x[i] * DT;
            pos_y[i] += vel_y[i] * DT;
            pos_z[i] += vel_z[i] * DT;
        }
    }
}


void particles_read(FILE *fp, t_particles* p) {
    for (int i = 0; i < N; i++) {
        fscanf(fp, "%f %f %f %f %f %f %f \n",
               &p->pos_x[i], &p->pos_y[i], &p->pos_z[i],
               &p->vel_x[i], &p->vel_y[i], &p->vel_z[i],
               &p->weight[i]);
    }
}

void particles_write(FILE *fp, t_particles* p) {
    for (int i = 0; i < N; i++) {
        fprintf(fp, "%10.10f %10.10f %10.10f %10.10f %10.10f %10.10f %10.10f \n",
                p->pos_x[i], p->pos_y[i], p->pos_z[i],
                p->vel_x[i], p->vel_y[i], p->vel_z[i],
                p->weight[i]);
    }
}

/*
 * Architektura procesoru (ACH 2016)
 * Projekt c. 1 (nbody)
 * Login: xvanku00
 */

#include "nbody.h"

void particles_simulate(t_particles *p) {
    int i, j, k;

    t_velocities velocities;

    float *pos_x = p->pos_x;
    float *pos_y = p->pos_y;
    float *pos_z = p->pos_z;
    float *vel_x = p->vel_x;
    float *vel_y = p->vel_y;
    float *vel_z = p->vel_z;
    float *weight = p->weight;

    for (k = 0; k < STEPS; k++) {
        //vynulovani mezisouctu
        #pragma omp simd
        for (i = 0; i < N; i++) {
            velocities.x[i] = 0.0f;
            velocities.y[i] = 0.0f;
            velocities.z[i] = 0.0f;
        }
        //vypocet nove rychlosti

        for (i = 0; i < N; i++) {
            #pragma omp simd aligned(pos_x, pos_y, pos_z, vel_x, vel_y, vel_z, weight: 64)
            for (j = 0; j < N; j++) {
                calculate_gravitation_velocity(
                        pos_x[j],
                        pos_y[j],
                        pos_z[j],
                        vel_x[j],
                        vel_y[j],
                        vel_z[j],
                        weight[j],

                        pos_x[i],
                        pos_y[i],
                        pos_z[i],
                        vel_x[i],
                        vel_y[i],
                        vel_z[i],
                        weight[i],

                        &(velocities.x[j]),
                        &(velocities.y[j]),
                        &(velocities.z[j])
                );
            }
        }
        //ulozeni rychlosti a posun castic
        #pragma omp simd aligned(pos_x, pos_y, pos_z, vel_x, vel_y, vel_z, weight: 64)
        for (i = 0; i < N; i++) {
            vel_x[i] += velocities.x[i];
            vel_y[i] += velocities.y[i];
            vel_z[i] += velocities.z[i];

            pos_x[i] += vel_x[i] * DT;
            pos_y[i] += vel_y[i] * DT;
            pos_z[i] += vel_z[i] * DT;
        }
    }
}


void particles_read(FILE *fp, t_particles* p) {
    for (int i = 0; i < N; i++) {
        fscanf(fp, "%f %f %f %f %f %f %f \n",
               &p->pos_x[i], &p->pos_y[i], &p->pos_z[i],
               &p->vel_x[i], &p->vel_y[i], &p->vel_z[i],
               &p->weight[i]);
    }
}

void particles_write(FILE *fp, t_particles* p) {
    for (int i = 0; i < N; i++) {
        fprintf(fp, "%10.10f %10.10f %10.10f %10.10f %10.10f %10.10f %10.10f \n",
                p->pos_x[i], p->pos_y[i], p->pos_z[i],
                p->vel_x[i], p->vel_y[i], p->vel_z[i],
                p->weight[i]);
    }
}

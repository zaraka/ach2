# mark_description "Intel(R) C++ Intel(R) 64 Compiler for applications running on Intel(R) 64, Version 16.0.1.150 Build 20151021";
# mark_description "";
# mark_description "-std=c++11 -lpapi -ansi-alias -O2 -Wall -xavx -qopenmp-simd -DN=1000 -DDT=0.001f -DSTEPS=1000 -S -fsource-as";
# mark_description "m -c";
	.file "nbody.cpp"
	.text
..TXTST0:
# -- Begin  _Z18particles_simulateP11t_particles
	.text
# mark_begin;
       .align    16,0x90
	.globl _Z18particles_simulateP11t_particles
# --- particles_simulate(t_particles *)
_Z18particles_simulateP11t_particles:
# parameter 1: %rdi
..B1.1:                         # Preds ..B1.0

### void particles_simulate(t_particles *p) {

	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
..___tag_value__Z18particles_simulateP11t_particles.1:
..L2:
                                                          #9.41
        pushq     %rbp                                          #9.41
	.cfi_def_cfa_offset 16
        movq      %rsp, %rbp                                    #9.41
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
        andq      $-32, %rsp                                    #9.41
        pushq     %r12                                          #9.41
        pushq     %r13                                          #9.41
        pushq     %r14                                          #9.41
        pushq     %r15                                          #9.41
        pushq     %rbx                                          #9.41
        subq      $13176, %rsp                                  #9.41

###     int i, j, k;
### 
###     t_velocities velocities;
### 
###     float *pos_x = p->pos_x;
###     float *pos_y = p->pos_y;
###     float *pos_z = p->pos_z;
###     float *vel_x = p->vel_x;
###     float *vel_y = p->vel_y;
###     float *vel_z = p->vel_z;
###     float *weight = p->weight;
### 
###     for (k = 0; k < STEPS; k++) {

        xorl      %eax, %eax                                    #22.10

###         //vynulovani mezisouctu
###         #pragma omp simd
###         for (i = 0; i < N; i++) {
###             velocities.x[i] = 0.0f;

        vxorps    %ymm1, %ymm1, %ymm1                           #26.31
	.cfi_escape 0x10, 0x03, 0x0e, 0x38, 0x1c, 0x0d, 0xe0, 0xff, 0xff, 0xff, 0x1a, 0x0d, 0xd8, 0xff, 0xff, 0xff, 0x22
	.cfi_escape 0x10, 0x0c, 0x0e, 0x38, 0x1c, 0x0d, 0xe0, 0xff, 0xff, 0xff, 0x1a, 0x0d, 0xf8, 0xff, 0xff, 0xff, 0x22
	.cfi_escape 0x10, 0x0d, 0x0e, 0x38, 0x1c, 0x0d, 0xe0, 0xff, 0xff, 0xff, 0x1a, 0x0d, 0xf0, 0xff, 0xff, 0xff, 0x22
	.cfi_escape 0x10, 0x0e, 0x0e, 0x38, 0x1c, 0x0d, 0xe0, 0xff, 0xff, 0xff, 0x1a, 0x0d, 0xe8, 0xff, 0xff, 0xff, 0x22
	.cfi_escape 0x10, 0x0f, 0x0e, 0x38, 0x1c, 0x0d, 0xe0, 0xff, 0xff, 0xff, 0x1a, 0x0d, 0xe0, 0xff, 0xff, 0xff, 0x22

###             velocities.y[i] = 0.0f;
###             velocities.z[i] = 0.0f;
###         }
###         //vypocet nove rychlosti
### 
###         for (i = 0; i < N; i++) {
###             #pragma omp simd aligned(pos_x, pos_y, pos_z, vel_x, vel_y, vel_z, weight: 64)
###             for (j = 0; j < N; j++) {
###                 calculate_gravitation_velocity(
###                         pos_x[j],
###                         pos_y[j],
###                         pos_z[j],
###                         vel_x[j],
###                         vel_y[j],
###                         vel_z[j],
###                         weight[j],
### 
###                         pos_x[i],
###                         pos_y[i],
###                         pos_z[i],
###                         vel_x[i],
###                         vel_y[i],
###                         vel_z[i],
###                         weight[i],
### 
###                         &(velocities.x[j]),
###                         &(velocities.y[j]),
###                         &(velocities.z[j])
###                 );
###             }
###         }
###         //ulozeni rychlosti a posun castic
###         #pragma omp simd aligned(pos_x, pos_y, pos_z, vel_x, vel_y, vel_z, weight: 64)
###         for (i = 0; i < N; i++) {
###             vel_x[i] += velocities.x[i];
###             vel_y[i] += velocities.y[i];
###             vel_z[i] += velocities.z[i];
### 
###             pos_x[i] += vel_x[i] * DT;

        movq      %rdi, %r15                                    #65.36
        vmovdqu   .L_2il0floatpacket.0(%rip), %xmm2             #52.27
        vmovups   .L_2il0floatpacket.1(%rip), %ymm0             #65.36
                                # LOE r15 eax ymm0 ymm1
..B1.2:                         # Preds ..B1.18 ..B1.1
        xorl      %edx, %edx                                    #25.14
        .align    16,0x90
                                # LOE rdx r15 eax ymm0 ymm1
..B1.3:                         # Preds ..B1.3 ..B1.2
        vmovups   %ymm1, 32(%rsp,%rdx,4)                        #26.13
        vmovups   %ymm1, 4032(%rsp,%rdx,4)                      #27.13
        vmovups   %ymm1, 8032(%rsp,%rdx,4)                      #28.13
        vmovups   %ymm1, 64(%rsp,%rdx,4)                        #26.13
        vmovups   %ymm1, 4064(%rsp,%rdx,4)                      #27.13
        vmovups   %ymm1, 8064(%rsp,%rdx,4)                      #28.13
        vmovups   %ymm1, 96(%rsp,%rdx,4)                        #26.13
        vmovups   %ymm1, 4096(%rsp,%rdx,4)                      #27.13
        vmovups   %ymm1, 8096(%rsp,%rdx,4)                      #28.13
        vmovups   %ymm1, 128(%rsp,%rdx,4)                       #26.13
        vmovups   %ymm1, 4128(%rsp,%rdx,4)                      #27.13
        vmovups   %ymm1, 8128(%rsp,%rdx,4)                      #28.13
        addq      $32, %rdx                                     #25.14
        cmpq      $992, %rdx                                    #25.14
        jb        ..B1.3        # Prob 99%                      #25.14
                                # LOE rdx r15 eax ymm0 ymm1
..B1.4:                         # Preds ..B1.3
        vmovups   %ymm1, 4000(%rsp)                             #26.13
        xorl      %edx, %edx                                    #32.14
        vmovups   %ymm1, 8000(%rsp)                             #27.13
        vmovups   %ymm1, 12000(%rsp)                            #28.13
        movl      %eax, (%rsp)                                  #32.14
                                # LOE rdx r15
..B1.5:                         # Preds ..B1.15 ..B1.4
        movq      %rdx, 8(%rsp)                                 #34.18
        xorl      %r14d, %r14d                                  #34.18
                                # LOE r14 r15
..B1.6:                         # Preds ..B1.14 ..B1.5
        movq      8(%rsp), %rbx                                 #44.25
        lea       4032(%rsp,%r14,4), %r12                       #53.27
        vmovdqu   .L_2il0floatpacket.0(%rip), %xmm4             #52.27
        lea       8032(%rsp,%r14,4), %r13                       #54.27
        vmovsd    24000(%r15,%r14,4), %xmm0                     #42.25
        vbroadcastss (%r15,%rbx,4), %xmm1                       #44.25
        vbroadcastss 4000(%r15,%rbx,4), %xmm8                   #45.25
        vmovd     %r12, %xmm2                                   #53.27
        vbroadcastss 8000(%r15,%rbx,4), %xmm6                   #46.25
        vbroadcastss 12000(%r15,%rbx,4), %xmm12                 #47.25
        vbroadcastss 16000(%r15,%rbx,4), %xmm13                 #48.25
        vbroadcastss 20000(%r15,%rbx,4), %xmm15                 #49.25
        vbroadcastss 24000(%r15,%rbx,4), %xmm14                 #50.25
        vmovupd   %xmm0, 12656(%rsp)                            #42.25
        vmovups   %xmm1, 12528(%rsp)                            #44.25
        vmovss    %xmm1, 13104(%rsp)                            #35.17
        vmovss    %xmm0, 13112(%rsp)                            #35.17
        lea       32(%rsp,%r14,4), %rbx                         #52.27
        vmovsd    20000(%r15,%r14,4), %xmm5                     #41.25
        vmovsd    4000(%r15,%r14,4), %xmm1                      #35.17
        vmovsd    (%r15,%r14,4), %xmm0                          #35.17
        vmovd     %rbx, %xmm7                                   #52.27
        vpunpcklqdq %xmm7, %xmm7, %xmm3                         #52.27
        vpunpcklqdq %xmm2, %xmm2, %xmm7                         #53.27
        vpaddq    %xmm4, %xmm7, %xmm2                           #53.27
        vpaddq    %xmm4, %xmm3, %xmm3                           #52.27
        vmovd     %xmm2, %rsi                                   #35.17
        vmovd     %r13, %xmm7                                   #54.27
        vpunpcklqdq %xmm7, %xmm7, %xmm7                         #54.27
        vpaddq    %xmm4, %xmm7, %xmm4                           #54.27
        vmovd     %xmm3, %rdi                                   #35.17
        vmovupd   %xmm5, 12624(%rsp)                            #41.25
        vmovups   %xmm6, 12512(%rsp)                            #46.25
        vmovups   %xmm12, 12448(%rsp)                           #47.25
        vmovups   %xmm13, 12352(%rsp)                           #48.25
        vmovups   %xmm15, 12272(%rsp)                           #49.25
        vmovups   %xmm14, 12224(%rsp)                           #50.25
        vmovdqu   %xmm3, 12144(%rsp)                            #52.27
        vmovdqu   %xmm2, 12080(%rsp)                            #53.27
        vmovdqu   %xmm4, 12032(%rsp)                            #54.27
        vmovupd   %xmm1, 12736(%rsp)                            #35.17
        vmovupd   %xmm0, 12800(%rsp)                            #35.17
        addq      $-48, %rsp                                    #35.17
        vmovd     %xmm4, %rdx                                   #35.17
        vmovsd    8000(%r15,%r14,4), %xmm9                      #38.25
        vmovsd    12000(%r15,%r14,4), %xmm10                    #39.25
        vmovss    %xmm8, (%rsp)                                 #35.17
        vmovss    %xmm6, 8(%rsp)                                #35.17
        vmovss    %xmm12, 16(%rsp)                              #35.17
        vmovsd    16000(%r15,%r14,4), %xmm11                    #40.25
        vmovss    %xmm13, 24(%rsp)                              #35.17
        vmovss    %xmm15, 32(%rsp)                              #35.17
        vmovss    %xmm14, 40(%rsp)                              #35.17
        vzeroupper                                              #35.17
        vmovss    13160(%rsp), %xmm6                            #35.17
        vmovaps   %xmm9, %xmm2                                  #35.17
        vmovss    13152(%rsp), %xmm7                            #35.17
        vmovaps   %xmm10, %xmm3                                 #35.17
        vmovaps   %xmm11, %xmm4                                 #35.17
        vmovups   %xmm8, 12896(%rsp)                            #35.17
        vmovupd   %xmm11, 12912(%rsp)                           #35.17
        vmovupd   %xmm10, 12928(%rsp)                           #35.17
        vmovupd   %xmm9, 12944(%rsp)                            #35.17
..___tag_value__Z18particles_simulateP11t_particles.11:
#       calculate_gravitation_velocity(float, float, float, float, float, float, float, float, float, float, float, float, float, float, float *, float *, float *)
        call      _Z30calculate_gravitation_velocityffffffffffffffPfS_S_ #35.17
..___tag_value__Z18particles_simulateP11t_particles.12:
                                # LOE rbx r12 r13 r14 r15
..B1.22:                        # Preds ..B1.6
        vmovupd   12944(%rsp), %xmm9                            #
        vmovupd   12928(%rsp), %xmm10                           #
        vmovupd   12912(%rsp), %xmm11                           #
        vmovups   12896(%rsp), %xmm8                            #
        addq      $48, %rsp                                     #35.17
                                # LOE rbx r12 r13 r14 r15 xmm8 xmm9 xmm10 xmm11 ymm8 ymm9 ymm10 ymm11 zmm8 zmm9 zmm10 zmm11
..B1.7:                         # Preds ..B1.22
        vpshufd   $14, 12032(%rsp), %xmm14                      #35.17
        vpshufd   $57, 12800(%rsp), %xmm0                       #35.17
        vpshufd   $57, 12736(%rsp), %xmm1                       #35.17
        vpshufd   $57, 12624(%rsp), %xmm5                       #35.17
        vpshufd   $57, 12656(%rsp), %xmm6                       #35.17
        vpshufd   $57, 12528(%rsp), %xmm7                       #35.17
        vpshufd   $57, 12512(%rsp), %xmm12                      #35.17
        vpshufd   $14, 12080(%rsp), %xmm15                      #35.17
        vpshufd   $57, %xmm9, %xmm2                             #35.17
        vpshufd   $57, 12272(%rsp), %xmm9                       #35.17
        vpshufd   $57, %xmm10, %xmm3                            #35.17
        vpshufd   $57, 12352(%rsp), %xmm10                      #35.17
        vpshufd   $57, %xmm11, %xmm4                            #35.17
        vpshufd   $57, 12448(%rsp), %xmm11                      #35.17
        vpshufd   $57, %xmm8, %xmm13                            #35.17
        vpshufd   $57, 12224(%rsp), %xmm8                       #35.17
        vmovd     %xmm14, %rdx                                  #35.17
        vpshufd   $14, 12144(%rsp), %xmm14                      #35.17
        vmovd     %xmm15, %rsi                                  #35.17
        vmovd     %xmm14, %rdi                                  #35.17
        addq      $-48, %rsp                                    #35.17
        vmovss    %xmm13, (%rsp)                                #35.17
        vmovss    %xmm12, 8(%rsp)                               #35.17
        vmovss    %xmm11, 16(%rsp)                              #35.17
        vmovss    %xmm10, 24(%rsp)                              #35.17
        vmovss    %xmm9, 32(%rsp)                               #35.17
        vmovss    %xmm8, 40(%rsp)                               #35.17
..___tag_value__Z18particles_simulateP11t_particles.13:
#       calculate_gravitation_velocity(float, float, float, float, float, float, float, float, float, float, float, float, float, float, float *, float *, float *)
        call      _Z30calculate_gravitation_velocityffffffffffffffPfS_S_ #35.17
..___tag_value__Z18particles_simulateP11t_particles.14:
                                # LOE rbx r12 r13 r14 r15
..B1.23:                        # Preds ..B1.7
        addq      $48, %rsp                                     #35.17
                                # LOE rbx r12 r13 r14 r15
..B1.8:                         # Preds ..B1.23
        movq      8(%rsp), %rax                                 #44.25
        lea       8(%rbx), %rcx                                 #52.27
        vmovdqu   .L_2il0floatpacket.0(%rip), %xmm6             #52.27
        lea       8(%r12), %r8                                  #53.27
        vmovsd    24008(%r15,%r14,4), %xmm2                     #42.25
        lea       8(%r13), %r9                                  #54.27
        vbroadcastss (%r15,%rax,4), %xmm1                       #44.25
        vmovd     %rcx, %xmm7                                   #52.27
        vmovd     %r8, %xmm3                                    #53.27
        vpunpcklqdq %xmm7, %xmm7, %xmm4                         #52.27
        vpunpcklqdq %xmm3, %xmm3, %xmm7                         #53.27
        vpaddq    %xmm6, %xmm7, %xmm3                           #53.27
        vpaddq    %xmm6, %xmm4, %xmm4                           #52.27
        vbroadcastss 4000(%r15,%rax,4), %xmm15                  #45.25
        vmovd     %r9, %xmm7                                    #54.27
        vpunpcklqdq %xmm7, %xmm7, %xmm7                         #54.27
        vmovupd   %xmm2, 12544(%rsp)                            #42.25
        vbroadcastss 12000(%r15,%rax,4), %xmm13                 #47.25
        vbroadcastss 16000(%r15,%rax,4), %xmm12                 #48.25
        vbroadcastss 20000(%r15,%rax,4), %xmm11                 #49.25
        vmovups   %xmm1, 12464(%rsp)                            #44.25
        vmovss    %xmm1, 13120(%rsp)                            #35.17
        vmovss    %xmm2, 13128(%rsp)                            #35.17
        vmovsd    8(%r15,%r14,4), %xmm0                         #36.25
        vmovsd    20008(%r15,%r14,4), %xmm5                     #41.25
        vpaddq    %xmm6, %xmm7, %xmm6                           #54.27
        vbroadcastss 8000(%r15,%rax,4), %xmm8                   #46.25
        vmovsd    8008(%r15,%r14,4), %xmm2                      #35.17
        vmovsd    4008(%r15,%r14,4), %xmm1                      #35.17
        vmovupd   %xmm0, 12704(%rsp)                            #36.25
        vmovupd   %xmm5, 12608(%rsp)                            #41.25
        vmovups   %xmm15, 12400(%rsp)                           #45.25
        vmovups   %xmm13, 12384(%rsp)                           #47.25
        vmovups   %xmm12, 12208(%rsp)                           #48.25
        vmovups   %xmm11, 12320(%rsp)                           #49.25
        vmovdqu   %xmm4, 12096(%rsp)                            #52.27
        vmovdqu   %xmm3, 12160(%rsp)                            #53.27
        vmovdqu   %xmm6, 16(%rsp)                               #54.27
        vmovupd   %xmm2, 12720(%rsp)                            #35.17
        vmovupd   %xmm1, 12784(%rsp)                            #35.17
        addq      $-48, %rsp                                    #35.17
        vbroadcastss 24000(%r15,%rax,4), %xmm14                 #50.25
        vmovsd    12008(%r15,%r14,4), %xmm9                     #39.25
        vmovd     %xmm6, %rdx                                   #35.17
        vmovss    %xmm15, (%rsp)                                #35.17
        vmovss    %xmm8, 8(%rsp)                                #35.17
        vmovd     %xmm3, %rsi                                   #35.17
        vmovss    %xmm13, 16(%rsp)                              #35.17
        vmovsd    16008(%r15,%r14,4), %xmm10                    #40.25
        vmovd     %xmm4, %rdi                                   #35.17
        vmovss    %xmm12, 24(%rsp)                              #35.17
        vmovss    %xmm11, 32(%rsp)                              #35.17
        vmovss    %xmm14, 40(%rsp)                              #35.17
        vmovss    13176(%rsp), %xmm6                            #35.17
        vmovaps   %xmm9, %xmm3                                  #35.17
        vmovss    13168(%rsp), %xmm7                            #35.17
        vmovaps   %xmm10, %xmm4                                 #35.17
        vmovups   %xmm14, 12960(%rsp)                           #35.17
        vmovups   %xmm8, 12976(%rsp)                            #35.17
        vmovupd   %xmm10, 12992(%rsp)                           #35.17
        vmovupd   %xmm9, 13008(%rsp)                            #35.17
..___tag_value__Z18particles_simulateP11t_particles.15:
#       calculate_gravitation_velocity(float, float, float, float, float, float, float, float, float, float, float, float, float, float, float *, float *, float *)
        call      _Z30calculate_gravitation_velocityffffffffffffffPfS_S_ #35.17
..___tag_value__Z18particles_simulateP11t_particles.16:
                                # LOE rbx r12 r13 r14 r15
..B1.24:                        # Preds ..B1.8
        vmovupd   13008(%rsp), %xmm9                            #
        vmovupd   12992(%rsp), %xmm10                           #
        vmovups   12976(%rsp), %xmm8                            #
        vmovups   12960(%rsp), %xmm14                           #
        addq      $48, %rsp                                     #35.17
                                # LOE rbx r12 r13 r14 r15 xmm8 xmm9 xmm10 xmm14 ymm8 ymm9 ymm10 ymm14 zmm8 zmm9 zmm10 zmm14
..B1.9:                         # Preds ..B1.24
        vpshufd   $57, %xmm8, %xmm12                            #35.17
        vpshufd   $57, %xmm14, %xmm8                            #35.17
        vpshufd   $14, 16(%rsp), %xmm14                         #35.17
        vpshufd   $57, 12704(%rsp), %xmm0                       #35.17
        vpshufd   $57, 12784(%rsp), %xmm1                       #35.17
        vpshufd   $57, 12720(%rsp), %xmm2                       #35.17
        vpshufd   $57, 12608(%rsp), %xmm5                       #35.17
        vpshufd   $57, 12544(%rsp), %xmm6                       #35.17
        vpshufd   $57, 12464(%rsp), %xmm7                       #35.17
        vpshufd   $57, 12400(%rsp), %xmm13                      #35.17
        vpshufd   $57, 12384(%rsp), %xmm11                      #35.17
        vpshufd   $14, 12160(%rsp), %xmm15                      #35.17
        vpshufd   $57, %xmm9, %xmm3                             #35.17
        vpshufd   $57, 12320(%rsp), %xmm9                       #35.17
        vpshufd   $57, %xmm10, %xmm4                            #35.17
        vpshufd   $57, 12208(%rsp), %xmm10                      #35.17
        vmovd     %xmm14, %rdx                                  #35.17
        vpshufd   $14, 12096(%rsp), %xmm14                      #35.17
        vmovd     %xmm15, %rsi                                  #35.17
        vmovd     %xmm14, %rdi                                  #35.17
        addq      $-48, %rsp                                    #35.17
        vmovss    %xmm13, (%rsp)                                #35.17
        vmovss    %xmm12, 8(%rsp)                               #35.17
        vmovss    %xmm11, 16(%rsp)                              #35.17
        vmovss    %xmm10, 24(%rsp)                              #35.17
        vmovss    %xmm9, 32(%rsp)                               #35.17
        vmovss    %xmm8, 40(%rsp)                               #35.17
..___tag_value__Z18particles_simulateP11t_particles.17:
#       calculate_gravitation_velocity(float, float, float, float, float, float, float, float, float, float, float, float, float, float, float *, float *, float *)
        call      _Z30calculate_gravitation_velocityffffffffffffffPfS_S_ #35.17
..___tag_value__Z18particles_simulateP11t_particles.18:
                                # LOE rbx r12 r13 r14 r15
..B1.25:                        # Preds ..B1.9
        addq      $48, %rsp                                     #35.17
                                # LOE rbx r12 r13 r14 r15
..B1.10:                        # Preds ..B1.25
        movq      8(%rsp), %rax                                 #44.25
        lea       16(%rbx), %rcx                                #52.27
        vmovdqu   .L_2il0floatpacket.0(%rip), %xmm4             #52.27
        lea       16(%r12), %r8                                 #53.27
        vmovsd    24016(%r15,%r14,4), %xmm1                     #42.25
        lea       16(%r13), %r9                                 #54.27
        vbroadcastss (%r15,%rax,4), %xmm0                       #44.25
        vmovd     %rcx, %xmm7                                   #52.27
        vmovd     %r8, %xmm2                                    #53.27
        vpunpcklqdq %xmm7, %xmm7, %xmm3                         #52.27
        vpunpcklqdq %xmm2, %xmm2, %xmm7                         #53.27
        vpaddq    %xmm4, %xmm7, %xmm2                           #53.27
        vpaddq    %xmm4, %xmm3, %xmm3                           #52.27
        vbroadcastss 4000(%r15,%rax,4), %xmm6                   #45.25
        vmovd     %r9, %xmm7                                    #54.27
        vpunpcklqdq %xmm7, %xmm7, %xmm7                         #54.27
        vmovupd   %xmm1, 12672(%rsp)                            #42.25
        vbroadcastss 8000(%r15,%rax,4), %xmm12                  #46.25
        vbroadcastss 16000(%r15,%rax,4), %xmm13                 #48.25
        vbroadcastss 20000(%r15,%rax,4), %xmm15                 #49.25
        vbroadcastss 24000(%r15,%rax,4), %xmm14                 #50.25
        vmovups   %xmm0, 12496(%rsp)                            #44.25
        vmovss    %xmm0, 13136(%rsp)                            #35.17
        vmovss    %xmm1, 13144(%rsp)                            #35.17
        vmovsd    20016(%r15,%r14,4), %xmm5                     #41.25
        vpaddq    %xmm4, %xmm7, %xmm4                           #54.27
        vbroadcastss 12000(%r15,%rax,4), %xmm8                  #47.25
        vmovsd    4016(%r15,%r14,4), %xmm1                      #35.17
        vmovsd    16(%r15,%r14,4), %xmm0                        #35.17
        vmovupd   %xmm5, 12592(%rsp)                            #41.25
        vmovups   %xmm6, 12432(%rsp)                            #45.25
        vmovups   %xmm12, 12368(%rsp)                           #46.25
        vmovups   %xmm13, 12304(%rsp)                           #48.25
        vmovups   %xmm15, 12560(%rsp)                           #49.25
        vmovups   %xmm14, 12256(%rsp)                           #50.25
        vmovdqu   %xmm3, 12176(%rsp)                            #52.27
        vmovdqu   %xmm2, 12128(%rsp)                            #53.27
        vmovdqu   %xmm4, 12064(%rsp)                            #54.27
        vmovupd   %xmm1, 12752(%rsp)                            #35.17
        vmovupd   %xmm0, 12816(%rsp)                            #35.17
        addq      $-48, %rsp                                    #35.17
        vmovd     %xmm4, %rdx                                   #35.17
        vmovd     %xmm2, %rsi                                   #35.17
        vmovd     %xmm3, %rdi                                   #35.17
        vmovss    %xmm6, (%rsp)                                 #35.17
        vmovss    %xmm12, 8(%rsp)                               #35.17
        vmovss    %xmm8, 16(%rsp)                               #35.17
        vmovsd    8016(%r15,%r14,4), %xmm9                      #38.25
        vmovsd    12016(%r15,%r14,4), %xmm10                    #39.25
        vmovsd    16016(%r15,%r14,4), %xmm11                    #40.25
        vmovss    %xmm13, 24(%rsp)                              #35.17
        vmovss    %xmm15, 32(%rsp)                              #35.17
        vmovss    %xmm14, 40(%rsp)                              #35.17
        vmovss    13192(%rsp), %xmm6                            #35.17
        vmovaps   %xmm9, %xmm2                                  #35.17
        vmovss    13184(%rsp), %xmm7                            #35.17
        vmovaps   %xmm10, %xmm3                                 #35.17
        vmovaps   %xmm11, %xmm4                                 #35.17
        vmovups   %xmm8, 13024(%rsp)                            #35.17
        vmovupd   %xmm11, 13040(%rsp)                           #35.17
        vmovupd   %xmm10, 13056(%rsp)                           #35.17
        vmovupd   %xmm9, 13072(%rsp)                            #35.17
..___tag_value__Z18particles_simulateP11t_particles.19:
#       calculate_gravitation_velocity(float, float, float, float, float, float, float, float, float, float, float, float, float, float, float *, float *, float *)
        call      _Z30calculate_gravitation_velocityffffffffffffffPfS_S_ #35.17
..___tag_value__Z18particles_simulateP11t_particles.20:
                                # LOE rbx r12 r13 r14 r15
..B1.26:                        # Preds ..B1.10
        vmovupd   13072(%rsp), %xmm9                            #
        vmovupd   13056(%rsp), %xmm10                           #
        vmovupd   13040(%rsp), %xmm11                           #
        vmovups   13024(%rsp), %xmm8                            #
        addq      $48, %rsp                                     #35.17
                                # LOE rbx r12 r13 r14 r15 xmm8 xmm9 xmm10 xmm11 ymm8 ymm9 ymm10 ymm11 zmm8 zmm9 zmm10 zmm11
..B1.11:                        # Preds ..B1.26
        vpshufd   $14, 12064(%rsp), %xmm14                      #35.17
        vpshufd   $57, 12816(%rsp), %xmm0                       #35.17
        vpshufd   $57, 12752(%rsp), %xmm1                       #35.17
        vpshufd   $57, 12592(%rsp), %xmm5                       #35.17
        vpshufd   $57, 12672(%rsp), %xmm6                       #35.17
        vpshufd   $57, 12496(%rsp), %xmm7                       #35.17
        vpshufd   $57, 12432(%rsp), %xmm13                      #35.17
        vpshufd   $57, 12368(%rsp), %xmm12                      #35.17
        vpshufd   $14, 12128(%rsp), %xmm15                      #35.17
        vpshufd   $57, %xmm9, %xmm2                             #35.17
        vpshufd   $57, 12560(%rsp), %xmm9                       #35.17
        vpshufd   $57, %xmm10, %xmm3                            #35.17
        vpshufd   $57, 12304(%rsp), %xmm10                      #35.17
        vpshufd   $57, %xmm11, %xmm4                            #35.17
        vpshufd   $57, %xmm8, %xmm11                            #35.17
        vpshufd   $57, 12256(%rsp), %xmm8                       #35.17
        vmovd     %xmm14, %rdx                                  #35.17
        vpshufd   $14, 12176(%rsp), %xmm14                      #35.17
        vmovd     %xmm15, %rsi                                  #35.17
        vmovd     %xmm14, %rdi                                  #35.17
        addq      $-48, %rsp                                    #35.17
        vmovss    %xmm13, (%rsp)                                #35.17
        vmovss    %xmm12, 8(%rsp)                               #35.17
        vmovss    %xmm11, 16(%rsp)                              #35.17
        vmovss    %xmm10, 24(%rsp)                              #35.17
        vmovss    %xmm9, 32(%rsp)                               #35.17
        vmovss    %xmm8, 40(%rsp)                               #35.17
..___tag_value__Z18particles_simulateP11t_particles.21:
#       calculate_gravitation_velocity(float, float, float, float, float, float, float, float, float, float, float, float, float, float, float *, float *, float *)
        call      _Z30calculate_gravitation_velocityffffffffffffffPfS_S_ #35.17
..___tag_value__Z18particles_simulateP11t_particles.22:
                                # LOE rbx r12 r13 r14 r15
..B1.27:                        # Preds ..B1.11
        addq      $48, %rsp                                     #35.17
                                # LOE rbx r12 r13 r14 r15
..B1.12:                        # Preds ..B1.27
        addq      $24, %rbx                                     #52.27
        addq      $24, %r12                                     #53.27
        addq      $24, %r13                                     #54.27
        movq      8(%rsp), %rax                                 #44.25
        vmovdqu   .L_2il0floatpacket.0(%rip), %xmm4             #52.27
        vmovd     %rbx, %xmm1                                   #52.27
        vmovd     %r12, %xmm2                                   #53.27
        vpunpcklqdq %xmm1, %xmm1, %xmm3                         #52.27
        vpunpcklqdq %xmm2, %xmm2, %xmm1                         #53.27
        vbroadcastss (%r15,%rax,4), %xmm15                      #44.25
        vpaddq    %xmm4, %xmm1, %xmm2                           #53.27
        vpaddq    %xmm4, %xmm3, %xmm3                           #52.27
        vbroadcastss 4000(%r15,%rax,4), %xmm0                   #45.25
        vmovd     %r13, %xmm1                                   #54.27
        vmovsd    24024(%r15,%r14,4), %xmm13                    #42.25
        vpunpcklqdq %xmm1, %xmm1, %xmm1                         #54.27
        vbroadcastss 8000(%r15,%rax,4), %xmm12                  #46.25
        vbroadcastss 12000(%r15,%rax,4), %xmm14                 #47.25
        vbroadcastss 16000(%r15,%rax,4), %xmm6                  #48.25
        vbroadcastss 24000(%r15,%rax,4), %xmm7                  #50.25
        vmovupd   %xmm13, 12640(%rsp)                           #42.25
        vmovss    %xmm13, 13152(%rsp)                           #35.17
        vmovsd    20024(%r15,%r14,4), %xmm5                     #41.25
        vpaddq    %xmm4, %xmm1, %xmm4                           #54.27
        vbroadcastss 20000(%r15,%rax,4), %xmm8                  #49.25
        vmovsd    4024(%r15,%r14,4), %xmm1                      #35.17
        vmovsd    24(%r15,%r14,4), %xmm13                       #35.17
        vmovups   %xmm15, 12480(%rsp)                           #44.25
        vmovupd   %xmm5, 12688(%rsp)                            #41.25
        vmovups   %xmm0, 12416(%rsp)                            #45.25
        vmovups   %xmm12, 12336(%rsp)                           #46.25
        vmovups   %xmm14, 12288(%rsp)                           #47.25
        vmovups   %xmm6, 12240(%rsp)                            #48.25
        vmovups   %xmm7, 12576(%rsp)                            #50.25
        vmovdqu   %xmm3, 12192(%rsp)                            #52.27
        vmovdqu   %xmm2, 12112(%rsp)                            #53.27
        vmovdqu   %xmm4, 12048(%rsp)                            #54.27
        vmovupd   %xmm1, 12832(%rsp)                            #35.17
        vmovupd   %xmm13, 12768(%rsp)                           #35.17
        addq      $-48, %rsp                                    #35.17
        vmovd     %xmm4, %rdx                                   #35.17
        vmovd     %xmm2, %rsi                                   #35.17
        vmovd     %xmm3, %rdi                                   #35.17
        vmovss    %xmm0, (%rsp)                                 #35.17
        vmovss    %xmm12, 8(%rsp)                               #35.17
        vmovss    %xmm14, 16(%rsp)                              #35.17
        vmovsd    8024(%r15,%r14,4), %xmm9                      #38.25
        vmovsd    12024(%r15,%r14,4), %xmm10                    #39.25
        vmovsd    16024(%r15,%r14,4), %xmm11                    #40.25
        vmovss    %xmm6, 24(%rsp)                               #35.17
        vmovss    %xmm8, 32(%rsp)                               #35.17
        vmovss    %xmm7, 40(%rsp)                               #35.17
        vmovss    13200(%rsp), %xmm6                            #35.17
        vmovaps   %xmm13, %xmm0                                 #35.17
        vmovaps   %xmm15, %xmm7                                 #35.17
        vmovaps   %xmm9, %xmm2                                  #35.17
        vmovaps   %xmm10, %xmm3                                 #35.17
        vmovaps   %xmm11, %xmm4                                 #35.17
        vmovups   %xmm8, 13088(%rsp)                            #35.17
        vmovupd   %xmm11, 13104(%rsp)                           #35.17
        vmovupd   %xmm10, 13120(%rsp)                           #35.17
        vmovupd   %xmm9, 13136(%rsp)                            #35.17
..___tag_value__Z18particles_simulateP11t_particles.23:
#       calculate_gravitation_velocity(float, float, float, float, float, float, float, float, float, float, float, float, float, float, float *, float *, float *)
        call      _Z30calculate_gravitation_velocityffffffffffffffPfS_S_ #35.17
..___tag_value__Z18particles_simulateP11t_particles.24:
                                # LOE r14 r15
..B1.28:                        # Preds ..B1.12
        vmovupd   13136(%rsp), %xmm9                            #
        vmovupd   13120(%rsp), %xmm10                           #
        vmovupd   13104(%rsp), %xmm11                           #
        vmovups   13088(%rsp), %xmm8                            #
        addq      $48, %rsp                                     #35.17
                                # LOE r14 r15 xmm8 xmm9 xmm10 xmm11 ymm8 ymm9 ymm10 ymm11 zmm8 zmm9 zmm10 zmm11
..B1.13:                        # Preds ..B1.28
        vpshufd   $14, 12048(%rsp), %xmm14                      #35.17
        vpshufd   $57, 12768(%rsp), %xmm0                       #35.17
        vpshufd   $57, 12832(%rsp), %xmm1                       #35.17
        vpshufd   $57, 12688(%rsp), %xmm5                       #35.17
        vpshufd   $57, 12640(%rsp), %xmm6                       #35.17
        vpshufd   $57, 12480(%rsp), %xmm7                       #35.17
        vpshufd   $57, 12416(%rsp), %xmm13                      #35.17
        vpshufd   $57, 12336(%rsp), %xmm12                      #35.17
        vpshufd   $14, 12112(%rsp), %xmm15                      #35.17
        vpshufd   $57, %xmm9, %xmm2                             #35.17
        vpshufd   $57, %xmm10, %xmm3                            #35.17
        vpshufd   $57, 12240(%rsp), %xmm10                      #35.17
        vpshufd   $57, %xmm11, %xmm4                            #35.17
        vpshufd   $57, 12288(%rsp), %xmm11                      #35.17
        vpshufd   $57, %xmm8, %xmm9                             #35.17
        vpshufd   $57, 12576(%rsp), %xmm8                       #35.17
        vmovd     %xmm14, %rdx                                  #35.17
        vpshufd   $14, 12192(%rsp), %xmm14                      #35.17
        vmovd     %xmm15, %rsi                                  #35.17
        vmovd     %xmm14, %rdi                                  #35.17
        addq      $-48, %rsp                                    #35.17
        vmovss    %xmm13, (%rsp)                                #35.17
        vmovss    %xmm12, 8(%rsp)                               #35.17
        vmovss    %xmm11, 16(%rsp)                              #35.17
        vmovss    %xmm10, 24(%rsp)                              #35.17
        vmovss    %xmm9, 32(%rsp)                               #35.17
        vmovss    %xmm8, 40(%rsp)                               #35.17
..___tag_value__Z18particles_simulateP11t_particles.25:
#       calculate_gravitation_velocity(float, float, float, float, float, float, float, float, float, float, float, float, float, float, float *, float *, float *)
        call      _Z30calculate_gravitation_velocityffffffffffffffPfS_S_ #35.17
..___tag_value__Z18particles_simulateP11t_particles.26:
                                # LOE r14 r15
..B1.29:                        # Preds ..B1.13
        addq      $48, %rsp                                     #35.17
                                # LOE r14 r15
..B1.14:                        # Preds ..B1.29
        addq      $8, %r14                                      #34.18
        cmpq      $1000, %r14                                   #34.18
        jb        ..B1.6        # Prob 82%                      #34.18
                                # LOE r14 r15
..B1.15:                        # Preds ..B1.14
        movq      8(%rsp), %rdx                                 #
        incq      %rdx                                          #32.28
        cmpq      $1000, %rdx                                   #32.25
        jl        ..B1.5        # Prob 99%                      #32.25
                                # LOE rdx r15
..B1.16:                        # Preds ..B1.15
        vmovups   .L_2il0floatpacket.1(%rip), %ymm0             #
        xorl      %edx, %edx                                    #60.14
        vxorps    %ymm1, %ymm1, %ymm1                           #
        movl      (%rsp), %eax                                  #
                                # LOE rdx r15 eax ymm0 ymm1
..B1.17:                        # Preds ..B1.17 ..B1.16
        vmovups   12000(%r15,%rdx,4), %ymm2                     #61.13
        vmovups   16000(%r15,%rdx,4), %ymm3                     #62.13
        vmovups   20000(%r15,%rdx,4), %ymm4                     #63.13
        vaddps    32(%rsp,%rdx,4), %ymm2, %ymm5                 #61.13
        vaddps    4032(%rsp,%rdx,4), %ymm3, %ymm8               #62.13
        vaddps    8032(%rsp,%rdx,4), %ymm4, %ymm11              #63.13
        vmulps    %ymm5, %ymm0, %ymm6                           #65.36

###             pos_y[i] += vel_y[i] * DT;

        vmulps    %ymm8, %ymm0, %ymm9                           #66.36

###             pos_z[i] += vel_z[i] * DT;

        vmulps    %ymm11, %ymm0, %ymm12                         #67.36
        vmovups   %ymm5, 12000(%r15,%rdx,4)                     #61.13
        vmovups   %ymm8, 16000(%r15,%rdx,4)                     #62.13
        vmovups   %ymm11, 20000(%r15,%rdx,4)                    #63.13
        vaddps    (%r15,%rdx,4), %ymm6, %ymm7                   #65.13
        vaddps    4000(%r15,%rdx,4), %ymm9, %ymm10              #66.13
        vaddps    8000(%r15,%rdx,4), %ymm12, %ymm13             #67.13
        vmovups   %ymm7, (%r15,%rdx,4)                          #65.13
        vmovups   %ymm10, 4000(%r15,%rdx,4)                     #66.13
        vmovups   %ymm13, 8000(%r15,%rdx,4)                     #67.13
        addq      $8, %rdx                                      #60.14
        cmpq      $1000, %rdx                                   #60.14
        jb        ..B1.17       # Prob 99%                      #60.14
                                # LOE rdx r15 eax ymm0 ymm1
..B1.18:                        # Preds ..B1.17
        incl      %eax                                          #22.28
        cmpl      $1000, %eax                                   #22.21
        jl        ..B1.2        # Prob 99%                      #22.21
                                # LOE r15 eax ymm0 ymm1
..B1.19:                        # Preds ..B1.18

###         }
###     }
### }

        vzeroupper                                              #70.1
        addq      $13176, %rsp                                  #70.1
	.cfi_restore 3
        popq      %rbx                                          #70.1
	.cfi_restore 15
        popq      %r15                                          #70.1
	.cfi_restore 14
        popq      %r14                                          #70.1
	.cfi_restore 13
        popq      %r13                                          #70.1
	.cfi_restore 12
        popq      %r12                                          #70.1
        movq      %rbp, %rsp                                    #70.1
        popq      %rbp                                          #70.1
	.cfi_def_cfa 7, 8
	.cfi_restore 6
        ret                                                     #70.1
        .align    16,0x90
	.cfi_endproc
                                # LOE
# mark_end;
	.type	_Z18particles_simulateP11t_particles,@function
	.size	_Z18particles_simulateP11t_particles,.-_Z18particles_simulateP11t_particles
	.data
# -- End  _Z18particles_simulateP11t_particles
	.text
# -- Begin  _Z14particles_readP8_IO_FILEP11t_particles
	.text
# mark_begin;
       .align    16,0x90
	.globl _Z14particles_readP8_IO_FILEP11t_particles
# --- particles_read(FILE *, t_particles *)
_Z14particles_readP8_IO_FILEP11t_particles:
# parameter 1: %rdi
# parameter 2: %rsi
..B2.1:                         # Preds ..B2.0

### void particles_read(FILE *fp, t_particles* p) {

	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
..___tag_value__Z14particles_readP8_IO_FILEP11t_particles.35:
..L36:
                                                         #73.47
        pushq     %r12                                          #73.47
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
        pushq     %r13                                          #73.47
	.cfi_def_cfa_offset 24
	.cfi_offset 13, -24
        pushq     %r14                                          #73.47
	.cfi_def_cfa_offset 32
	.cfi_offset 14, -32

###     for (int i = 0; i < N; i++) {

        xorl      %eax, %eax                                    #74.16
        movq      %rax, %r12                                    #74.16
        movq      %rsi, %r13                                    #74.16
        movq      %rdi, %r14                                    #74.16
                                # LOE rbx rbp r12 r13 r14 r15
..B2.2:                         # Preds ..B2.3 ..B2.1

###         fscanf(fp, "%f %f %f %f %f %f %f \n",

        addq      $-32, %rsp                                    #75.9
	.cfi_def_cfa_offset 64

###                &p->pos_x[i], &p->pos_y[i], &p->pos_z[i],
###                &p->vel_x[i], &p->vel_y[i], &p->vel_z[i],
###                &p->weight[i]);

        lea       (%r13,%r12,4), %rdx                           #78.17
        movq      %r14, %rdi                                    #75.9
        lea       4000(%rdx), %rcx                              #75.9
        movl      $.L_2__STRING.0, %esi                         #75.9
        lea       8000(%rdx), %r8                               #75.9
        xorl      %eax, %eax                                    #75.9
        lea       12000(%rdx), %r9                              #75.9
        lea       16000(%rdx), %r10                             #75.9
        movq      %r10, (%rsp)                                  #75.9
        lea       20000(%rdx), %r11                             #75.9
        movq      %r11, 8(%rsp)                                 #75.9
        lea       24000(%rdx), %r10                             #75.9
        movq      %r10, 16(%rsp)                                #75.9
#       fscanf(FILE *, const char *, ...)
        call      fscanf                                        #75.9
                                # LOE rbx rbp r12 r13 r14 r15
..B2.7:                         # Preds ..B2.2
        addq      $32, %rsp                                     #75.9
	.cfi_def_cfa_offset 32
                                # LOE rbx rbp r12 r13 r14 r15
..B2.3:                         # Preds ..B2.7
        incq      %r12                                          #74.28
        cmpq      $1000, %r12                                   #74.25
        jl        ..B2.2        # Prob 99%                      #74.25
                                # LOE rbx rbp r12 r13 r14 r15
..B2.4:                         # Preds ..B2.3

###     }
### }

	.cfi_restore 14
        popq      %r14                                          #80.1
	.cfi_def_cfa_offset 24
	.cfi_restore 13
        popq      %r13                                          #80.1
	.cfi_def_cfa_offset 16
	.cfi_restore 12
        popq      %r12                                          #80.1
	.cfi_def_cfa_offset 8
        ret                                                     #80.1
        .align    16,0x90
	.cfi_endproc
                                # LOE
# mark_end;
	.type	_Z14particles_readP8_IO_FILEP11t_particles,@function
	.size	_Z14particles_readP8_IO_FILEP11t_particles,.-_Z14particles_readP8_IO_FILEP11t_particles
	.data
# -- End  _Z14particles_readP8_IO_FILEP11t_particles
	.text
# -- Begin  _Z15particles_writeP8_IO_FILEP11t_particles
	.text
# mark_begin;
       .align    16,0x90
	.globl _Z15particles_writeP8_IO_FILEP11t_particles
# --- particles_write(FILE *, t_particles *)
_Z15particles_writeP8_IO_FILEP11t_particles:
# parameter 1: %rdi
# parameter 2: %rsi
..B3.1:                         # Preds ..B3.0

### void particles_write(FILE *fp, t_particles* p) {

	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
..___tag_value__Z15particles_writeP8_IO_FILEP11t_particles.52:
..L53:
                                                         #82.48
        pushq     %r12                                          #82.48
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
        pushq     %r13                                          #82.48
	.cfi_def_cfa_offset 24
	.cfi_offset 13, -24
        pushq     %r14                                          #82.48
	.cfi_def_cfa_offset 32
	.cfi_offset 14, -32

###     for (int i = 0; i < N; i++) {

        xorl      %eax, %eax                                    #83.16
        movq      %rax, %r12                                    #83.16
        movq      %rsi, %r14                                    #83.16
        movq      %rdi, %r13                                    #83.16
                                # LOE rbx rbp r12 r13 r14 r15
..B3.2:                         # Preds ..B3.3 ..B3.1

###         fprintf(fp, "%10.10f %10.10f %10.10f %10.10f %10.10f %10.10f %10.10f \n",

        vxorpd    %xmm0, %xmm0, %xmm0                           #84.9
        vxorpd    %xmm1, %xmm1, %xmm1                           #84.9
        vxorpd    %xmm2, %xmm2, %xmm2                           #84.9
        vxorpd    %xmm3, %xmm3, %xmm3                           #84.9
        vxorpd    %xmm4, %xmm4, %xmm4                           #84.9
        vxorpd    %xmm5, %xmm5, %xmm5                           #84.9
        vxorpd    %xmm6, %xmm6, %xmm6                           #84.9
        movq      %r13, %rdi                                    #84.9
        vcvtss2sd (%r14,%r12,4), %xmm0, %xmm0                   #84.9
        vcvtss2sd 4000(%r14,%r12,4), %xmm1, %xmm1               #84.9
        vcvtss2sd 8000(%r14,%r12,4), %xmm2, %xmm2               #84.9
        vcvtss2sd 12000(%r14,%r12,4), %xmm3, %xmm3              #84.9
        vcvtss2sd 16000(%r14,%r12,4), %xmm4, %xmm4              #84.9
        vcvtss2sd 20000(%r14,%r12,4), %xmm5, %xmm5              #84.9
        vcvtss2sd 24000(%r14,%r12,4), %xmm6, %xmm6              #84.9
        movl      $.L_2__STRING.1, %esi                         #84.9
        movl      $7, %eax                                      #84.9
#       fprintf(FILE *, const char *, ...)
        call      fprintf                                       #84.9
                                # LOE rbx rbp r12 r13 r14 r15
..B3.3:                         # Preds ..B3.2
        incq      %r12                                          #83.28
        cmpq      $1000, %r12                                   #83.25
        jl        ..B3.2        # Prob 99%                      #83.25
                                # LOE rbx rbp r12 r13 r14 r15
..B3.4:                         # Preds ..B3.3

###                 p->pos_x[i], p->pos_y[i], p->pos_z[i],
###                 p->vel_x[i], p->vel_y[i], p->vel_z[i],
###                 p->weight[i]);
###     }
### }

	.cfi_restore 14
        popq      %r14                                          #89.1
	.cfi_def_cfa_offset 24
	.cfi_restore 13
        popq      %r13                                          #89.1
	.cfi_def_cfa_offset 16
	.cfi_restore 12
        popq      %r12                                          #89.1
	.cfi_def_cfa_offset 8
        ret                                                     #89.1
        .align    16,0x90
	.cfi_endproc
                                # LOE
# mark_end;
	.type	_Z15particles_writeP8_IO_FILEP11t_particles,@function
	.size	_Z15particles_writeP8_IO_FILEP11t_particles,.-_Z15particles_writeP8_IO_FILEP11t_particles
	.data
# -- End  _Z15particles_writeP8_IO_FILEP11t_particles
	.section .rodata, "a"
	.align 32
	.align 32
.L_2il0floatpacket.1:
	.long	0x3a83126f,0x3a83126f,0x3a83126f,0x3a83126f,0x3a83126f,0x3a83126f,0x3a83126f,0x3a83126f
	.type	.L_2il0floatpacket.1,@object
	.size	.L_2il0floatpacket.1,32
	.align 16
.L_2il0floatpacket.0:
	.long	0x00000000,0x00000000,0x00000004,0x00000000
	.type	.L_2il0floatpacket.0,@object
	.size	.L_2il0floatpacket.0,16
	.section .rodata.str1.4, "aMS",@progbits,1
	.align 4
	.align 4
.L_2__STRING.0:
	.long	622880293
	.long	1713709158
	.long	543565088
	.long	622880293
	.long	1713709158
	.word	2592
	.byte	0
	.type	.L_2__STRING.0,@object
	.size	.L_2__STRING.0,23
	.space 1, 0x00 	# pad
	.align 4
.L_2__STRING.1:
	.long	774910245
	.long	543567921
	.long	774910245
	.long	543567921
	.long	774910245
	.long	543567921
	.long	774910245
	.long	543567921
	.long	774910245
	.long	543567921
	.long	774910245
	.long	543567921
	.long	774910245
	.long	543567921
	.word	10
	.type	.L_2__STRING.1,@object
	.size	.L_2__STRING.1,58
	.data
	.section .note.GNU-stack, ""
// -- Begin DWARF2 SEGMENT .eh_frame
	.section .eh_frame,"a",@progbits
.eh_frame_seg:
	.align 8
# End
